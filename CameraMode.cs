﻿using UnityEngine;
using System.Collections;
using ToivonRandomUtils;
using ToivoDebug;
public class CameraMode : MonoBehaviour {
    public float handDistanceTolerance;
    [Header("Head will be this transform if null")]
    public Transform head;
    public Transform leftHand;
    public Transform rightHand;
    public Transform camera;
	// Update is called once per frame
	void Update () {
        if (head == null)
            head = transform;
        Vector3 handAveragePosition = HandAveragePosition(leftHand.position, rightHand.position);
        Vector3? cameraVector = CalculateCameraDirectionVector(head.position, handAveragePosition, 100f);
        if (cameraVector !=null)
        {
            CurrentCustomLog.LogDynamic("kamera vektori: ", cameraVector);

            Vector3 cast = ((Vector3)cameraVector);
            camera.position= (transform.position+  (cast * 2));
            var cross = Vector3.Cross((leftHand.position - rightHand.position).normalized, Vector3.up) * -1;
            ( camera.position + cross).DrawDebugPoint(null, 1f);
            camera.LookAt(camera.position + cross);
            Vector3.Angle(camera.position + cross, Vector3.up);
        }
        else
        {
            CurrentCustomLog.LogDynamic("kamera vektori: ", null);
        }

	}


    /// <summary>
    /// returns null if hands are too far from head else camera direction vector
    /// </summary>
    /// <param name="headPos"></param>
    /// <param name="leftHandPos"></param>
    /// <param name="rightHandPos"></param>
    /// <param name="handDistanceTolerance"></param>
    /// <returns></returns>
    public static Vector3? CalculateCameraDirectionVector(Vector3 headPos, Vector3 cameraPosition, float handDistanceTolerance){
        float cameraDistance = (headPos - cameraPosition).sqrMagnitude;

        if (cameraDistance> handDistanceTolerance) {
            return null;
        }
        
        return -(headPos - cameraPosition).normalized;
    }

    public static Vector3 HandAveragePosition( Vector3 leftHandPos, Vector3 rightHandPos)
    {
        return new Vector3(leftHandPos.x + rightHandPos.x, leftHandPos.y + rightHandPos.y, leftHandPos.z + rightHandPos.z) / 2;
    } 
}


