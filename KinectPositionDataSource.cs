﻿using UnityEngine;
using System.Collections;
using System.Linq;
using Kinect = Windows.Kinect;
using System.Collections.Generic;
using ToivoDebug;
using ToivonRandomUtils;
public class KinectPositionDataSource : MonoBehaviour {
    public List<TrackedBody > trackedBodies=new List<TrackedBody>();
    public TrackedBody selectedBody;
	public BodySourceManager bodySource;

	private Kinect.Body[] bodiesLast;
	public bool somethingTracked{ private set; get;}
    // Update is called once per frame
    //Kinect.Body selected = null;

    public static KinectPositionDataSource instance;


    private void Awake()
    {
        instance = this;
    }

    public int registeredBodies	 {get;private set;}
	public System.Action<TrackedBody> onBodyRegistered;

	void Update () {
		somethingTracked = false;
		bodiesLast= bodySource.GetData ();
		if (bodiesLast == null)
			return;

		foreach (var item in bodiesLast) {
            if (item.IsTracked)
            {
                var trackedBody = trackedBodies.Find(x => x.body == item);
                if (trackedBody==null )
                {
                    trackedBody = new TrackedBody { body = item };
                    trackedBodies.Add(trackedBody);
                    registeredBodies++;
                    somethingTracked = true;
					if(onBodyRegistered!=null) onBodyRegistered(trackedBody);
                }
                UpdateTrackedBodyJointPositions(trackedBody);
                if (selectedBody == null || !selectedBody.body.IsTracked)
                {
                    selectedBody = trackedBody;
                }
                
                
            }
		}
        CurrentCustomLog.LogDynamic("registered bodies", registeredBodies);

        if (!somethingTracked)
            return;

	}

    public static void LoadTestScene()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("TestControlLevel", UnityEngine.SceneManagement.LoadSceneMode.Additive);
    }


    void UpdateTrackedBodyJointPositions(TrackedBody trackedBody)
    {
        for (int i = 0; i < 25; i++)
        {
            trackedBody.joints[(Kinect.JointType)i] = CameraCoordToVector3( trackedBody.body.Joints[(Kinect.JointType)i].Position);
            //Debug.Log("tracking " + item.Key.ToString());
        }

    }

	Vector3 CameraCoordToVector3 (Kinect.CameraSpacePoint position)
	{
		return new Vector3 (position.X, position.Y, position.Z);
	}
}

public class TrackedBody
{
    public Kinect.Body body;
    public Dictionary<Kinect.JointType, Vector3> joints = new Dictionary<Kinect.JointType, Vector3>();
    public Dictionary<Windows.Kinect.JointType, DeltaTrackedPosition> deltas = new Dictionary<Windows.Kinect.JointType, DeltaTrackedPosition>();

}