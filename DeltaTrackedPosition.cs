﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Kinect = Windows.Kinect;
using ToivoDebug;	
public class DeltaTrackedPosition{
	public Vector3 deltaPosition{ get; private set; }
	public Vector3 position{ get; private set; }
	
	public float logTime{ get; private set; }
	public float speed{ get; private set; }

//	public 

	public void Update(Vector3 pos, float logTime){
		deltaPosition = position - pos;
		position = pos;

		speed = (logTime-this.logTime) * deltaPosition.sqrMagnitude;
        this.logTime = logTime;
	}
}