﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ToivoDebug;
using ToivonRandomUtils;

public class JointTracker : MonoBehaviour {
	public KinectPositionDataSource kinectDataSource;
    
    public bool visualize;
    public float refreshRate=0.01f;
    public Transform cubeHolder;

	List<GameObject> existingCubes=new List<GameObject>();
	float movement;
    float time;
    void Update()
    {
        time += Time.deltaTime;
        if (time > refreshRate)
        {
            time = 0f;
            UpdatePositions();
        }
    }
    void CreateCube()
    {
        existingCubes.Add(GameObject.CreatePrimitive(PrimitiveType.Cube));
        
        existingCubes[existingCubes.Count-1].transform.SetParent( (cubeHolder==null)?transform:cubeHolder , false);
        existingCubes[existingCubes.Count - 1].transform.localScale = Vector3.one * 0.1f;
    }

	void UpdatePositions () {
        if (!visualize)
        {
            foreach (var item in existingCubes)
            {
                Destroy(item.gameObject);
            }
            existingCubes.Clear();
        }
        
        int cubeIndex = 0;
        foreach (var item in kinectDataSource.trackedBodies)
        {
            if (!item.body.IsTracked)
            {
                continue;
            }
            
            for (int i = 0; i < 25; i++)
            {
                Vector3 pos = item.joints[(Windows.Kinect.JointType)i];
                if (visualize)
                {
                    if (existingCubes.Count <= cubeIndex)
                    {
                        CreateCube();
                    }
                    existingCubes[cubeIndex].transform.localPosition = pos;
                }
                //if (item == kinectDataSource.selectedBody) {
                //    existingCubes[cubeIndex].GetComponent<Renderer>().material.color = Color.green;
                //} else{
                //    existingCubes[cubeIndex].GetComponent<Renderer>().material.color = Color.gray;
                //}

                if (!item.deltas.ContainsKey((Windows.Kinect.JointType)i))
                    item.deltas.Add((Windows.Kinect.JointType)i, new DeltaTrackedPosition());

                item.deltas[(Windows.Kinect.JointType)i].Update(pos, Time.time);

                cubeIndex++;
            }

        }


//        CurrentCustomLog.LogDynamic ("something tracked", kinectDataSource.somethingTracked);
        int person = 0;
        

        foreach (var item in kinectDataSource.trackedBodies)
        {
            if (!item.body.IsTracked)
                continue;
            person++;
            float totalSpeedSum = 0;
            foreach (var delta in item.deltas)
            {
                if (delta.Key != Windows.Kinect.JointType.FootLeft || delta.Key != Windows.Kinect.JointType.FootRight)
                    totalSpeedSum += delta.Value.speed;
            }
        }
        
//        CurrentCustomLog.LogDynamic ("head speed: " , System.Math.Round( deltas [Windows.Kinect.JointType.Head].speed,3) );
	}
}
