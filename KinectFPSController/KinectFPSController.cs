﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using ToivoDebug;
using System.Linq;
using Windows.Kinect;
using ToivonRandomUtils;

public class KinectFPSController : MonoBehaviour
{
    public bool showDetailedDebug;
    public Canvas gestureProgressionCanvas;

    public float turningZTolerance;

    public float walkingKneeYAxisMinDist;

    public float turningTolerance = 0.8f;


    public bool selfContainedMode;

    private void Start()
    {
        KinectPositionDataSource.LoadTestScene();
    }

    void LogTrackingState(string state)
    {
        CurrentCustomLog.LogDynamic("tracking state: ", state);
    }

    float lastMoveForwardStamp;


    public float currentZOutput;


    public float moveForwardForTime;

    public float currentXRoationOutput;

    private void Update()
    {
        if (Time.time - lastMoveForwardStamp < moveForwardForTime)
        {
            if(selfContainedMode) transform.Translate(Vector3.forward * Time.deltaTime);
            currentZOutput = 1;
        }else {
            currentZOutput = 0;
        }


        if (KinectPositionDataSource.instance == null)
        {
            LogTrackingState("not initialized");
            return;
        }

        TrackedBody trackedBody = KinectPositionDataSource.instance.selectedBody;
        if (trackedBody == null)
        {
            LogTrackingState("no bodies");
            return;
        }


        //trackedBody.joints[JointType.SpineMid]
        Vector3 spineDir = JointOrientationToQuaternion(trackedBody.body.JointOrientations[JointType.SpineMid]) * Vector3.forward;
        if (showDetailedDebug) CurrentCustomLog.LogDynamic("spinemid dir:", spineDir);


        if (spineDir.x > turningTolerance)
        {
            CurrentCustomLog.LogDynamic("turning: ", "right");
            currentXRoationOutput = 1;
        }
        else if (spineDir.x < turningTolerance * -1)
        {
            CurrentCustomLog.LogDynamic("turning: ", "left");
            currentXRoationOutput = -1f;
        }
        else
        {
            CurrentCustomLog.LogDynamic("turning: ", "not turning");
            currentXRoationOutput = 0;
        }

        if(selfContainedMode) transform.Rotate(Vector3.up, turnSpeed * currentXRoationOutput * Time.deltaTime);

        if (showDetailedDebug) CurrentCustomLog.LogDynamic("spinemid dir:", JointOrientationToQuaternion(trackedBody.body.JointOrientations[JointType.SpineMid]) * Vector3.forward);

        if (currentWalkingTracking == null || !currentWalkingTracking.MoveNext()) currentWalkingTracking = WalkingTracker(trackedBody);
      //  if (leftHandTracking == null || !leftHandTracking.MoveNext()) leftHandTracking = HandTracker(trackedBody, JointType.HandLeft); 
        if (rightHandTracking == null || !rightHandTracking.MoveNext()) rightHandTracking = HandTracker(trackedBody, JointType.HandRight);
        if (rightHandTracking.Current)
        {
            if (onInteract != null) onInteract();
        }

            Quaternion lookRotation = Quaternion.LookRotation(trackedBody.joints[JointType.SpineMid] - trackedBody.joints[JointType.HandLeft].MultiplyAxises(-1, -1, 1));
        if(selfContainedMode)
        {
            GetComponentInChildren<Camera>().transform.localRotation = lookRotation;
        }
        currentLookingRotation = lookRotation;
    }

    public Quaternion currentLookingRotation;
    public System.Action onInteract;

    IEnumerator currentWalkingTracking;
    IEnumerator<bool> leftHandTracking;
    IEnumerator<bool> rightHandTracking;

    public float handUsageCooldown = 0.5f;
    public float turnSpeed;

    public Quaternion JointOrientationToQuaternion(JointOrientation jointOrientation)
    {
        Quaternion quaternion = new Quaternion(jointOrientation.Orientation.W, jointOrientation.Orientation.X, jointOrientation.Orientation.Y, jointOrientation.Orientation.Z);
        if (showDetailedDebug) CurrentCustomLog.LogDynamic("JointOrientationToQuaternion "+jointOrientation.JointType, quaternion.eulerAngles);
        return quaternion;
    }

    IEnumerator<bool> HandTracker(TrackedBody trackedBody, JointType hand)
    {
        while(trackedBody.body.Joints[hand].Position.Y < trackedBody.body.Joints[JointType.Head].Position.Y)
        {
            CurrentCustomLog.LogDynamic(hand.ToString() + " interaction state: ", "put your hand above your head");
            yield return false;
        }

        float cooldownStartStamp = Time.time;
        CurrentCustomLog.Log("SUCCESFUL " + hand + " USAGE");
        yield return true;

        while (true)
        {
            CurrentCustomLog.LogDynamic(hand.ToString() + " interaction state: ", "on cool down");
            if(Time.time - cooldownStartStamp > handUsageCooldown)
            {
                break;
            }
            yield return false;
        }

    }

    IEnumerator WalkingTracker(TrackedBody trackedBody)
    {
        JointType usedLeg;
        while (true)
        {
            CurrentCustomLog.LogDynamic("walking state", "not walking");
            if(DistanceToJointOnYAxis(trackedBody, JointType.KneeLeft, JointType.SpineMid) < walkingKneeYAxisMinDist)
            {
                usedLeg = JointType.KneeLeft;
                CurrentCustomLog.Log("left step");
                break;
            }
            if (DistanceToJointOnYAxis(trackedBody, JointType.KneeRight, JointType.SpineMid) < walkingKneeYAxisMinDist)
            {
                usedLeg = JointType.KneeRight;
                CurrentCustomLog.Log("right step");
                break;
            }
            yield return null;

        }

        CurrentCustomLog.LogDynamic("walking state", "please return leg");
        while (true)
        {
            if (DistanceToJointOnYAxis(trackedBody, usedLeg, JointType.SpineMid) > walkingKneeYAxisMinDist)
            {
                break;
            }
            yield return null;
        }
        CurrentCustomLog.Log("succesful walk cycle");
        lastMoveForwardStamp = Time.time;
    }

    float DistanceToJointOnYAxis(TrackedBody trackedBody, JointType jointA, JointType jointB)
    {
        float dist = Mathf.Abs(trackedBody.joints[jointA].y - trackedBody.joints[jointB].y);
        if (showDetailedDebug) CurrentCustomLog.LogDynamic(jointA + " ydist to "+ jointB, dist);
        return dist;
    }


}
