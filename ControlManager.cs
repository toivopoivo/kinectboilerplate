﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
public class ControlManager : MonoBehaviour {

	public static ControlManager instance;
    public bool loadLevelOnStart;
    public bool setSceneDebugStuffOffOnStart;
	public string defaultLevelName;
	public Transform playerObject;
	public UnityEvent debugStuffToSetNotActiveOnRealGame;

    public static System.Action onControlManagerStart;

    public JointTracker jointTracker;
    public KinectPositionDataSource kinectPositionDataSource;

	void Awake(){
		instance = this;
        if (setSceneDebugStuffOffOnStart)
            ((GameObject)debugStuffToSetNotActiveOnRealGame.GetPersistentTarget(0)).SetActive(false);

	}
    void Start()
    {
        if (onControlManagerStart != null)
        {
            onControlManagerStart();
        }
    }

}
