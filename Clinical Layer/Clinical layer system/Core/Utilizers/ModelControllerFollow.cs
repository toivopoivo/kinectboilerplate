using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Kinect = Windows.Kinect;
using System.IO;

namespace ClinicalLayer
{
    public enum FollowMode { None, Live, LiveRemote, Recording, Override, ForceTPose };
    public enum PartialVisibilityMode { Transparent, Clip };

    public class ModelControllerFollow : BodyOrientationUtilizer
    {

        public bool driving = true;
        public bool moveRoot = false;
        public bool dontEvenDefaultIt = false; //TODO: rename
        public bool onlyVisibleWhenTracked = false;
        public bool visDebug = false;
        public bool autoLive = false;

        public BodyOrientationProvider overrideSource;

        public KinectBodyOrientationSensor refToFollow; //the reference body that this body will imitate (local)
        public MoveRepeater repeaterToFollow; //get saved frames from here
        public BodyFrameReceiver receiverToFollow; //get network feed of frames from here

        public BodyOrientationProvider currentBodySource
        {
            get
            {
                switch (currentFollowMode)
                {
                    case FollowMode.Live:
                        return refToFollow;
                    case FollowMode.LiveRemote:
                        return receiverToFollow;
                    case FollowMode.Recording:
                        return repeaterToFollow;
                    case FollowMode.Override:
                        return overrideSource;
                    default:
                        return null;
                }
            }
        }

        public FollowMode currentFollowMode
        {
            set
            {
                UnSubscribeFrom(currentBodySource);
                m_currentFollowmode = value;
                switch (value)
                {
                    case FollowMode.Live:
                        SubscribeTo(refToFollow);
                        break;
                    case FollowMode.LiveRemote:
                        SubscribeTo(receiverToFollow);
                        break;
                    case FollowMode.Recording:
                        SubscribeTo(repeaterToFollow);
                        break;
                    case FollowMode.Override:
                        SubscribeTo(overrideSource);
                        break;
                    default:
                        break;
                }
            }
            get
            {
                return m_currentFollowmode;
            }
        }
        private FollowMode m_currentFollowmode;

        public bool createBoxSkeletonInStart = false;


        private TrackedJoint[] joints; //all the TrackedJoint scripts acting as bones that this rig has.

        public BodyOrientationFrame currentBodyOrientationFrame; //not sure if this being public is ok
        //private BodyOrientationFrame previousBodyOrientationFrame;

        //used at runtime
        public Dictionary<Kinect.JointType, TrackedJoint> jointsByChild = new Dictionary<Kinect.JointType, TrackedJoint>();

        private JointFrame currJointFrame; //put here to avoid GC
        //private JointFrame prevJointFrame;

        private bool haveSomeData = false; //avoid errors when there's not even old data

        private float smoothingExtraMod = 1f; //to avoid smoothing when getting first actual data

        public bool isTracking
        {
            get
            {
                return m_isTracking;
            }
            set
            {
                if (m_isTracking == value && trackingSet) return;
                m_isTracking = value;
                trackingSet = true;

                if (onlyVisibleWhenTracked)
                {
                    if (value)
                    {
                        visibilityController.Appear();
                    }
                    else visibilityController.Disappear();
                }
            }
        }
        private bool m_isTracking = false;
        private bool trackingSet = false; //for calling Appear() and Disappear() properly

        public bool disAppearWhenNoFrames = false;
        private float disappearTimer = 0;
        [HideInInspector]
        public VisibilityController visibilityController;

        private Quaternion[] tRots;

		//public Animator handsStateAnimator; //Very unity-specific
		public Animation leftHandAnim;
		public Animation rightHandAnim;

		public AnimationClip leftHandOpenClip;
		public AnimationClip leftHandClosedClip;


		[ContextMenu("HandAnimTest1")]
		void HandAnimTest1() {
			//handsStateAnimator.SetInteger("leftHandState",1);
			leftHandAnim.CrossFade("leftHandClosedClip",1f);
		}

		[ContextMenu("HandAnimTest2")]
		void HandAnimTest2() {
			//handsStateAnimator.SetInteger("leftHandState",2);
			//leftHandAnim.clip = leftHandOpenClip;
			leftHandAnim.CrossFade("leftHandOpenClip",1f);
		}
		
		void GetJointsForDictionary()
        {
            foreach (TrackedJoint joint in joints)
            {
                if (!joint.childJoint) continue;
                if (jointsByChild.ContainsKey(joint.childJoint.thisType))
                {
                    Debug.LogWarning("Double joint, skipping! Type:" + joint.childJoint.thisType + " GameObject name:" + joint.childJoint.name + " Existing GameObject name:" + jointsByChild[joint.childJoint.thisType]);
                    continue;
                }
                jointsByChild.Add(joint.childJoint.thisType, joint);
            }
        }

        // Use this for initialization
        void Start()
        {                       
			//Run hierarchy re-adjusting before getting list of joints, if re-adjusting script is attached
            if (GetComponent<RigAdjusterForKinect>() != null) {
				if (!refToFollow) refToFollow = FindObjectOfType<KinectBodyOrientationSensor>();
                GetComponent<RigAdjusterForKinect>().MakeParentsWithRightOrientation();
            }
            joints = GetComponentsInChildren<TrackedJoint>();
            GetJointsForDictionary();

			if (createBoxSkeletonInStart) {
				BoxSkeleton skelly = GetComponent<BoxSkeleton>();
				if(skelly == null)skelly = gameObject.AddComponent<BoxSkeleton>();
				skelly.CreateBoxSkeleton();
			}

            if (autoLive)
            {
                //Debug.LogWarning(name+": auto live on but follow mode already set.");
                //currentFollowMode = FollowMode.LiveRemote;
                currentFollowMode = FollowMode.Live;
            }

            if (overrideSource != null) currentFollowMode = FollowMode.Override;

            visibilityController = GetComponent<VisibilityController>();
            if (!visibilityController) visibilityController = gameObject.AddComponent<VisibilityController>();

            //Attempt to get t-pose rots
            /*tRots = new Quaternion[joints.Length];
            for (int i = 0; i < joints.Length; i++)
            {
                //tRots[i] = joints[i].transform.localRotation;
                joints[i].tRot = joints[i].transform.rotation;
            }*/

			Invoke("HandAnimTest2",15f);

        }

        [ContextMenu("Debug visibility fading")]
        void DebugVisibility()
        {
            isTracking = !isTracking;
        }

        public override void NewFrameAvailable()
        {
            currentBodySource.GetLatestFrame(out currentBodyOrientationFrame);
            haveSomeData = true;
            disappearTimer = 0.3f;
            //print (this+": new frame");
        }

        void Update()
        {
            //TODO: visibility controller breaks if disAppearWhenNoFrames is not used
            disappearTimer -= Time.deltaTime;
            if (disAppearWhenNoFrames)
            {
                if (disappearTimer < 0) visibilityController.Disappear();
                else if (driving) visibilityController.Appear();
            }

            if (currentFollowMode == FollowMode.None || !haveSomeData || !driving)
            {
                if (!visDebug) isTracking = false;
                return;
            }

            if (currentBodyOrientationFrame.jointFrames == null) return;

            //Spoof the tracked state if trying to debug visibility
            if (!visDebug) isTracking = currentBodyOrientationFrame.isTracking;

            float smoothMod = Mathf.Clamp(Time.deltaTime * 30, 0.016f, 1f);
            //print ("smoothmod:"+smoothMod);


            //Set rotations for joints
            int count = 0;
            foreach (TrackedJoint joint in joints)
            {
                if (!joint.childJoint) continue;
				//if(joint.childJoint.thisType == Kinect.JointType.ShoulderLeft || joint.childJoint.thisType == Kinect.JointType.ShoulderRight)continue;

                currJointFrame = currentBodyOrientationFrame.jointFrames[(int)joint.childJoint.thisType];

                Quaternion toBeSetLocalRotation;

                //Forced T-pose mode, set all rotations to zero
                if (currentFollowMode == FollowMode.ForceTPose)
                {
                    toBeSetLocalRotation = Quaternion.Lerp(joint.transform.localRotation, Quaternion.identity, 0.2f);
                }

                //Normal case, use given data
                else
                {
                    //Ignore hand middle joint, and combine the rotations of wrist and hand middle joint to wrist
                    /*if(joint.childJoint.thisType == Kinect.JointType.HandTipRight || joint.childJoint.thisType == Kinect.JointType.HandTipLeft) continue;
                    if(joint.childJoint.thisType == Kinect.JointType.HandLeft) {
                        toBeSetLocalRotation = currentBodyOrientationFrame.jointFrames[(int)Kinect.JointType.HandTipLeft].jointRotLocal * currentBodyOrientationFrame.jointFrames[(int)Kinect.JointType.HandLeft].jointRotLocal;
                    }
                    else if(joint.childJoint.thisType == Kinect.JointType.HandRight) {
                        toBeSetLocalRotation = currentBodyOrientationFrame.jointFrames[(int)Kinect.JointType.HandTipRight].jointRotLocal * currentBodyOrientationFrame.jointFrames[(int)Kinect.JointType.HandRight].jointRotLocal;
                    }*/

                    //else {
                    toBeSetLocalRotation = currJointFrame.jointRotLocal;
                    //}
                }

                //Lerp towards the target rotation
                joint.transform.localRotation = Quaternion.Lerp(joint.transform.localRotation, toBeSetLocalRotation /* * Quaternion.Inverse(joint.tRot) */, 0.5f * smoothMod * smoothingExtraMod * Time.timeScale);
				//joint.transform.localRotation = toBeSetLocalRotation;
                count++;
            }

            bool isLive = (currentFollowMode == FollowMode.Live || currentFollowMode == FollowMode.LiveRemote);

            //Set root position
            if (currentFollowMode != FollowMode.ForceTPose)
            {
                if (moveRoot && (!isLive || (isLive && currentBodyOrientationFrame.isTracking)))
                {
                    transform.localPosition = Vector3.Lerp(transform.localPosition, currentBodyOrientationFrame.rootPosition, smoothMod * smoothingExtraMod * Time.timeScale);
                    //transform.localPosition = Vector3.Lerp(previousBodyOrientationFrame.rootPosition,currentBodyOrientationFrame.rootPosition,progressToNextFrame);
                }
                else if (!dontEvenDefaultIt) transform.localPosition = new Vector3(0, 0, -3f);
            }
            //else transform.localPosition = new Vector3(0,0,0f); //Don't change position if adjusting joints

            //if(isTracking)smoothingExtraMod = 1;
            //else smoothingExtraMod = 5000;

            //print(name+" tracking:"+isTracking);
        }



        public bool[] GetIgnoredJoints()
        {
            bool[] ignoredJoints = new bool[25];
            for (Kinect.JointType t = 0; t < (Kinect.JointType)25; t++)
            {
                if (!jointsByChild.ContainsKey(t))
                {
                    ignoredJoints[(int)t] = true;
                }
                else ignoredJoints[(int)t] = jointsByChild[t].ignored;
            }
            return ignoredJoints;
        }

        /*public void SetIgnoredJoints(bool[] ignoredJoints) {
            bool[] activeJoints = new bool[25];
            for (Kinect.JointType t = 0; t < (Kinect.JointType)25; t++) {
                if(!jointsByChild.ContainsKey(t)) {
                    jointsByChild[t].ignored = true;
                }
                else activeJoints[(int)t] = jointsByChild[t].ignored;
                jointsByChild[t].ignored = activeJoints[(int)t];
            }
        }*/

        public void SetIgnoredJoints(bool[] ignoreds, bool visualizeMeshes)
        {
            TrackedJoint temp;
            for (Kinect.JointType t = 0; t < (Kinect.JointType)25; t++)
            {
                if (jointsByChild.ContainsKey(t))
                {
                    //if(jointSource.ignoredJoints.Length == 0)return; //TODO:fix. Dunno what's going wrong
                    //print("JOINT ACCCESS:"+(int)t+" ignoredjoints lenght:"+jointSource.ignoredJoints.Length);
                    temp = jointsByChild[t];
                    //temp.ignored = jointSource.ignoredJoints[(int)t];
                    temp.ignored = ignoreds[(int)t];
                }
            }

			JointHighlightMeshManager jointMeshHighligter = GetComponent<JointHighlightMeshManager>();
			if(jointMeshHighligter != null && visualizeMeshes) {
				float[] highLightVals = new float[25];
				for (int i = 0; i < 25; i++) {
					if(ignoreds[i]) highLightVals[i] = -501f;
					else highLightVals[i] = 1f;
				}
				jointMeshHighligter.DisplayJointCompliance(highLightVals);
			}
        }

        /*[ContextMenu("Init")]
        void Init()
        {
            joints = GetComponentsInChildren<TrackedJoint>(); //get all the joints in the hierarchy
            CreateBoxSkeleton();
        }*/
    }
}