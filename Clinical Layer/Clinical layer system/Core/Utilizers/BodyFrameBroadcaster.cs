using UnityEngine;
using System.Collections;
//using Serialization; //UnitySerializer

namespace ClinicalLayer
{
    public enum BroadcastMode { Live, Rebroadcast };

    public class BodyFrameBroadcaster : BodyOrientationUtilizer
    {
        public BodyOrientationProvider liveBodySource; //TODO change back to sensorsource
        public MoveRepeater reBroadcastSource;

        public BroadcastMode currentBroadcastMode
        {
            set
            {
                //Unsubscribe from the old source
                UnSubscribeFrom(currentBodySource);
                //Subscribe to the new source
                m_currentBroadcastMode = value;
                switch (value)
                {
                    case BroadcastMode.Live:
                        SubscribeTo(liveBodySource);
                        break;
                    case BroadcastMode.Rebroadcast:
                        SubscribeTo(reBroadcastSource);
                        break;
                }
            }
            get
            {
                return m_currentBroadcastMode;
            }
        }
        private BroadcastMode m_currentBroadcastMode;

        public BodyOrientationProvider currentBodySource
        {
            get
            {
                if (currentBroadcastMode == BroadcastMode.Live) return liveBodySource;
                else return reBroadcastSource;
            }
        }

        public BodyOrientationFrameQuality broadcastQuality = BodyOrientationFrameQuality.High;

        public bool alterFPS = false; //if true, broadcaster will calculate it's own frame times, otherwise it will just send data when it's available (preferred)
        public float frameRate = 30;               

  //      public NetworkView target;

        public bool autoStart = false;
        public bool autoServerStart = false;

		public bool broadcasting = true;

		private float frameTime { get { return 1f / frameRate; } }
		private float timer = 0;
        private bool trackedLastFrame = false;
		private BodyOrientationFrame jointFramesBuffer;
		private byte[] dataBuffer;

        void Start()
        {
            if (autoStart && Utils.KinectPossiblyAvailable())
            {
                broadcasting = true;                
                currentBroadcastMode = BroadcastMode.Live;
            }
        }

        void Update()
        {
      //      if (alterFPS && broadcasting && Network.peerType == NetworkPeerType.Server)
      //      {
      //          timer -= Time.deltaTime;
      //          if (timer < 0)
      //          {
      //              SendJointData();
      //              timer += frameTime;
      //          }
      //      }
        }

        public override void NewFrameAvailable()
        {
      //     if (!alterFPS && broadcasting && Network.peerType == NetworkPeerType.Server)
      //     {
		//		//print(currentBodySource);
      //         SendJointData();
      //     }
        }

        void SendJointData()
        {
      //     currentBodySource.GetLatestFrame(out jointFramesBuffer);
      //     if (!trackedLastFrame)
      //     {
      //         if (!jointFramesBuffer.isTracking) return; //simple optimization: if body isn't tracked, don't send any data (except for one frame with untracked status, so the "not tracked" state gets sent)
      //     }
      //     //if(trackedLastFrame != jointFramesBuffer.isTracking) print("Broadcaster "+name+" state changed, active:"+jointFramesBuffer.isTracking);
      //     trackedLastFrame = jointFramesBuffer.isTracking;
      //
      //     //dataBuffer = UnitySerializer.Serialize(jointFramesBuffer);//binary
      //     //dataBuffer = UnitySerializer.SerializeForDeserializeInto (jointFramesBuffer); //better binary?
      //     //dataBuffer = System.Text.Encoding.Default.GetBytes (UnitySerializer.JSONSerialize (jointFramesBuffer)); //JSON
      //
      //     dataBuffer = jointFramesBuffer.ToBytes(broadcastQuality); //custom binary, efficient
      //     //print (dataBuffer.Length);
      //
      //     //target.RPC("SetJointData", RPCMode.All, dataBuffer); //ORIGINAL
		//	//CHANGE 28.2.2015 to string because crashbug
		//	//print("SENDING");
		//	target.RPC("SetJointData", RPCMode.All, /*MoveSender.GetString(dataBuffer)*/dataBuffer);
      //
      //
      //     //print("Sent frame! Tracking"+jointFramesBuffer.isTracking+" Lenght:"+dataBuffer.Length);
      }
    }
}