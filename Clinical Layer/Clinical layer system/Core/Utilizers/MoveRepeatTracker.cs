using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Kinect = Windows.Kinect;
using System.Linq;

namespace ClinicalLayer
{
	public struct CandidateComparisonFrame {
		public int comparedToFrameNum;

		public float rawAngleError; //error in degrees in comparison to the reference pose, summed from every joint
		public float distance; //distance in frames from reference
		public float distancePenalty; //distance in frames from expected frame
		public float distancePenaltyMultiplier; //unused currently

		public float finalCalculatedError;
	}

	public struct RepeatScore {
		public float[] frameSpecificDeviationAngles;
		public float avgDeviationAngle;
	}

    public class MoveRepeatTracker : BodyOrientationUtilizer
    {
		//The source for input frames for the tracking. Tracker will subscribe to this in Start
        public BodyOrientationProvider bodySource;

		//The current reference move for tracking.
		public ClinicalMove currentlyTrackingMove;

		//The live and reference body visualizations, both optional. If set, joint compliance may be visualized.
        public ModelControllerFollow liveBody;
        public ModelControllerFollow referenceBody;

		//The latest pose gotten from the body source.
        private BodyOrientationFrame bodyOrientationFrameBuffer;

		//If set, its position will be set to the currently tracked position in the reference move
        public MoveRepeater attachedRepeater;

		//Is the tracking running or not.
		public bool trackingActive { get; private set; }

        private int cyclesCompleted = 0;

        //temp modifiers
        private bool[] currentFrameIgnoredStates;

        private float[] frameScores; //TODO figure out if to keep or not

		//Is the user currently in a position to be tracked with the move?
		public bool trackingAcquired = false;

		public float currentSpeed { get; private set; }
        /*public float currentSpeed
        {
            get { return m_currentSpeed; }
            private set { m_currentSpeed = value; }
        }
        private float m_currentSpeed = 0;*/

		public float currentFrame {
			get { return m_currentFrame; }
			set {
				m_currentFrame = value;
				if (attachedRepeater) attachedRepeater.SetFrame(Mathf.RoundToInt(value)); //TODO: check that this works, untested //TODO:maybe move out of here (delete)
			}
		}
		private float m_currentFrame;

		//Get normalized current progress of tracked move.
		public float moveProgressNormalized{get{return currentFrame / (float)currentClipLenght;}}

		public float maxError = 30;


		#region TODOREMOVE
        public GameObject trackingIndicator;
		//TODO: remove from here, wrong place
        public Text lastCyclePointsText;
        public Text currentCycleNumberText;
		public HeatGraphNewUI heatGraph;
		public HeatGraphNewUI lastRepeatScoringHeatMap;

		//TODO: move from here, wrong place
		//[HideInInspector]
		//public ExerciseProgramTracker exerciseProgramTracker;
		//TODO: do better, depedency is not ok (use delegates?)
		[HideInInspector]
		public MoveRecognizer parentRecognizer;
		#endregion

        private int currentClipLenght {get{return currentlyTrackingMove.lenghtInFrames;}}
		private bool justGotNewFrame = false;

		//For angular velocity tracking
        /*private Vector3[] lastRotations;
        private Vector3[,] refAngularVelocities;
        private Vector3[] currentAngularVelocities;*/

		//frames from the period that the last repeat took, for scoring a single repeat
		private List<BodyOrientationFrame> lastRepeatUserFrames = new List<BodyOrientationFrame>();

		private float lastFrame = 0f;
		private float lastProgressNormalized = 0f;

		//Read these to get the change in move progress
		public float frameProgressDelta {
			get { return currentFrame - lastFrame; }
		}
		public float normalizedProgressDelta {
			get { return moveProgressNormalized - lastProgressNormalized; }
		}

		//Was a cycle completed this frame?
		public bool completedCycleThisFrame;
		public bool usePurkkaCooldown; 

		public System.Action onPurkkaCoolDown;
		void Awake() {
			trackingActive = false; //doing this here because this needs to be false and can't do in initialization as setter and getter are defined
		}

        // Use this for initialization
        void Start()
        {
            if (bodySource == null) {
				Debug.Log(this+": no body source set, attempting to find and set to a KinectBodyOrientationSensor.");
				bodySource = FindObjectOfType<KinectBodyOrientationSensor>();
			}
            SubscribeTo(bodySource);

            if (referenceBody && !attachedRepeater) attachedRepeater = referenceBody.GetComponent<MoveRepeater>();

            if (lastCyclePointsText) lastCyclePointsText.gameObject.SetActive(false);
            //if(currentCycleNumberText)currentCycleNumberText.gameObject.SetActive (false);
        }

        //Debug
        [ContextMenu("Resub")]
        void Resub()
        {
            SubscribeTo(bodySource);
        }

        public override void NewFrameAvailable()
        {
            //print (name + " fetching new frame from " + bodySource);
            bodySource.GetLatestFrame(out bodyOrientationFrameBuffer);
			justGotNewFrame = true;
        }

        // Update is called once per frame
        void Update()
        {
            if (!trackingActive) return;
            if (!bodyOrientationFrameBuffer.isTracking) {
				//TODO:test
                SetTrackingAcquired(false);
                return;
            }       

            currentFrameIgnoredStates = currentlyTrackingMove.GetFrame((int)currentFrame).ignoredJoints;

            //Visualize ignored / active joints on both live and reference skeletons
            //print (Mathf.RoundToInt (currentFrame));
            //print (currentlyTrackingMove.orientationFrames.Length);
            //##########################################################################################TODO:decide what visualization to use for what (boxes, vertex color)
            if (referenceBody) referenceBody.SetIgnoredJoints(currentlyTrackingMove.GetFrame(Mathf.RoundToInt(currentFrame)).ignoredJoints,true);
            if (liveBody) liveBody.SetIgnoredJoints(currentlyTrackingMove.GetFrame(Mathf.RoundToInt(currentFrame)).ignoredJoints,true);

			//Save the previous progress to calculate delta
			lastFrame = currentFrame;
			lastProgressNormalized = moveProgressNormalized;

			//TODO: maybe move these to a new struct (CandidateComparisonFrame)
            //int bestFrameYet = 0;
			CandidateComparisonFrame bestCandidateYet = new CandidateComparisonFrame();
			bestCandidateYet.finalCalculatedError = 10000f;


            float expectedFrame = (currentFrame + currentSpeed + 1f); //frame expected now, taking speed in to account (and some extra encouragement)
            //print ("curr frame:" + currentFrame + " expected frame:" + expectedFrame);
            if (expectedFrame > currentClipLenght-1) {
                if (currentClipLenght < currentSpeed * 3) expectedFrame = 0; //avoid going to overdrive (cycles done every frame) with short moves
                else expectedFrame -= currentClipLenght;
			}

            float[] heatValues = new float[currentClipLenght];


			//Main part of algorithm. Here we go through all frames in the current animation and compare them to the one that is currently playing
            for (int i = 0; i < currentClipLenght; i++)
            {
				CandidateComparisonFrame currentCandidate = new CandidateComparisonFrame();
				currentCandidate.comparedToFrameNum = i;
				currentCandidate.distance = i - currentFrame;//Get the frame number difference between current frame and frame comparing to. Positive if candidate frame number is larger

                currentCandidate.rawAngleError = GetTotalPoseError(bodyOrientationFrameBuffer, i, true); //Last part: only update visual for current frame comparison (not multiple times one frame)

				currentCandidate.distancePenaltyMultiplier = 1f;
				bool candidateGoesBackwards = (i < currentFrame && currentCandidate.distance < 0.7f * currentClipLenght); //TODO change the weird conditions to something that is true if a rep was just completed last frame
				if (candidateGoesBackwards) currentCandidate.distancePenaltyMultiplier = 5f;

				float diffFromExpected = i - expectedFrame;
				currentCandidate.distancePenalty = Mathf.Abs(/*currentCandidate.distance*/diffFromExpected);
				currentCandidate.distancePenalty *= currentCandidate.distancePenaltyMultiplier;

				currentCandidate.finalCalculatedError = currentCandidate.rawAngleError + currentCandidate.distancePenalty;

				heatValues[i] = 1f - currentCandidate.finalCalculatedError / (maxError * 3.33f);

				if (currentCandidate.finalCalculatedError < bestCandidateYet.finalCalculatedError) {
					bestCandidateYet = currentCandidate;
                }
            }
            //print("Chose frame "+bestFrameYet+", error:"+bestError+" diff:"+bestDiff+" distance penalty"+bestDistancePenalty+"("+bestDistancePenaltyMultip+"x)" );
            //if(lastCyclePointsText)lastCyclePointsText.text = "Best frame: " + bestFrameYet + " error:" + System.Math.Round(bestError,2).ToString("F2");
            //currentCycleNumberText.text = "Best frame: " + bestFrameYet + " error:" + System.Math.Round(bestError,2).ToString("F2");

            //lastCyclePointsText.text = "frame:"+bestFrameYet;
            //TODO: re-enable!
            if (heatGraph) {
                heatGraph.UpdateHeatMap(heatValues);
                heatGraph.UpdateMarkers((float)currentFrame / (float)currentClipLenght, expectedFrame / (float)currentClipLenght);
            }

			if (bestCandidateYet.rawAngleError < maxError) {
				currentFrame = bestCandidateYet.comparedToFrameNum;
                SetTrackingAcquired(true);
				frameScores[bestCandidateYet.comparedToFrameNum] = bestCandidateYet.finalCalculatedError; //TODO maybe move
                //print("uuaa");

				float accMultip = 5f;
				//if (bestDiff < currentSpeed) accMultip = 1f; //TODO FIGURE OUT WHAT THIS WAS
				currentSpeed = Mathf.Lerp(currentSpeed, bestCandidateYet.distance, Time.deltaTime * accMultip); //TODO maybe do this even when not tracking, lerp speed down
            }
            else {
                //Debug.LogWarning("Pose too far off. Ignoring and using last good pose.");
                SetTrackingAcquired(false);
				currentSpeed = 0;
            }


            //print (currentSpeed);
            //check if cycle was completed
			if (currentFrame > currentClipLenght - (currentSpeed * 5f)) CycleDone(); //TODO: do this in a better way.

			if(justGotNewFrame)lastRepeatUserFrames.Add(bodyOrientationFrameBuffer);
			justGotNewFrame = false;


        }

		void LateUpdate() {
			completedCycleThisFrame = false;
		}

        void SetTrackingAcquired(bool val)
        {            
            if (val == trackingAcquired) return;
            trackingAcquired = val;

			//TODO: move out of class
			if (!trackingIndicator) return;
            Color c = Color.red;
            if (val) c = Color.green;
            iTween.ColorTo(trackingIndicator, c, 0.1f);
            //trackingIndicator.SetActive(val);
        }

		//<summary>asd</summmary>
		public RepeatScore CalculateLastRepeatError() {
			RepeatScore score = new RepeatScore();

			score.frameSpecificDeviationAngles = new float[currentlyTrackingMove.lenghtInFrames];	
			float totalErr = 0f;

			float[] heatValues = new float[currentlyTrackingMove.lenghtInFrames];

			System.Diagnostics.Stopwatch timer = System.Diagnostics.Stopwatch.StartNew();
			//Go through all reference animation frames, find the closest poses from user frames, get average error from found best matches
			for (int referenceFrameIndex = 0; referenceFrameIndex < currentlyTrackingMove.lenghtInFrames; referenceFrameIndex++) {
				timer.Reset();
				timer.Start();
				float bestPoseError = 100000f;
				for (int userFrameIndex = 0; userFrameIndex < lastRepeatUserFrames.Count; userFrameIndex++) {
					float poseError = GetTotalPoseError(lastRepeatUserFrames[userFrameIndex],referenceFrameIndex,false);
					if(poseError < bestPoseError)bestPoseError = poseError;
				}
				//print("time searching best representation for one reference frame:"+timer.ElapsedMilliseconds);
				timer.Stop();
				score.frameSpecificDeviationAngles[referenceFrameIndex] = bestPoseError;
				totalErr += bestPoseError;

				heatValues[referenceFrameIndex] = 1f - bestPoseError / (5 * 3.33f);
			}
			score.avgDeviationAngle = totalErr / (float)lastRepeatUserFrames.Count;

			if(lastRepeatScoringHeatMap) {
				lastRepeatScoringHeatMap.CreateGraph((float)currentlyTrackingMove.lenghtInFrames);
				lastRepeatScoringHeatMap.UpdateHeatMap(heatValues);
			}
			print("REMEMBER TO FLUSH USER FRAMES");

			lastRepeatUserFrames.Clear();

			return score;
		}

        
        float GetTotalPoseError(BodyOrientationFrame comparePose, int referenceFrameNum, bool updateVisual)
        {
            float totalError = 0;
            int numActive = 0;
			
			float[] complyValues = null;
			if(updateVisual) {
            	complyValues = new float[25];//TODO:ungc
			}

            for (Kinect.JointType t = (Kinect.JointType)0; t < (Kinect.JointType)25; t++)
            {
				if (t == Kinect.JointType.SpineBase) continue; //spinebase is child to no one, so skip
				if (updateVisual && currentFrameIgnoredStates != null && currentFrameIgnoredStates[(int)t] == true)
                {
                    complyValues[(int)t] = -501f;
                    continue; //joint ignored, dont care
                }

                //print("accessing:"+(int)t+" real lenght:"+liveBody.jointSource.jointFrames.Length);
                //Vector3 liveJointVec = Utils.GetUpVector(liveBody.currentBodyOrientationFrame.jointFrames[(int)t].jointRotLocal);
                //Vector3 referenceJointVec = Utils.GetUpVector(currentlyTrackingMove.orientationFrames[frame].jointFrames[(int)t].jointRotLocal);
				
				Vector3 liveJointVec = GetJointDirectionVector(comparePose, t); //TODO: dont
				Vector3 referenceJointVec = GetJointDirectionVector(currentlyTrackingMove.GetFrame(referenceFrameNum) , t);

                float error = Vector3.Angle(liveJointVec, referenceJointVec);

				if(updateVisual)complyValues[(int)t] = 1f - (error / (maxError * 2));


				//DEBUG ##########: drawing the "right directions" for joints

				/*TrackedJoint j = liveBody.jointsByChild[t];

				Vector3 properRayDir = Utils.GetJointDirectionVector(j.transform.rotation,j.childJoint.thisType);
				Quaternion parentRot = j.transform.parent.rotation;
				//Quaternion childLocalRot = j.transform.localRotation;
				Quaternion childLocalRot = currentlyTrackingMove.GetFrame(referenceFrameNum).jointFrames[(int)t].jointRotLocal;
				//Quaternion childLocalRot = comparePose.jointFrames[(int)t].jointRotLocal;
				Quaternion worldRotTest = parentRot * childLocalRot;
				Vector3 referenceRay = Utils.GetJointDirectionVector(worldRotTest,j.childJoint.thisType);*/	

				/*Quaternion refLocal = currentlyTrackingMove.GetFrame(referenceFrameNum).jointFrames[(int)t].jointRotLocal;
				Vector3 unCorrectedLocalVec = Utils.GetForwardVector(refLocal);
				Vector3 unCorrectedWorldVec = j.transform.InverseTransformDirection(unCorrectedLocalVec);
				Vector3 correctedWorldVec*/

				/*if(referenceFrameNum == (int)currentFrame) {
					Debug.DrawRay(j.transform.position,properRayDir,Color.blue,0.2f);
					Debug.DrawRay(j.transform.position,referenceRay * 1.2f,Color.yellow,0.2f); //Draw the strongest reference ray specially

					/*print("A:"+properRayDir);
					print("B:"+properRayDirTest);

				}
				else {
					Color c = Color.Lerp(Color.red,Color.green,complyValues[(int)t]);
					Debug.DrawRay(j.transform.position,referenceRay,c);
				}*/
	

				/*Transform cube = j.transform.FindChild("WAY");
				if(!cube) {
					cube = GameObject.CreatePrimitive(PrimitiveType.Cube).transform;
					cube.localScale = new Vector3(0.03f,0.03f,0.2f);
					cube.parent = j.transform;
					cube.localPosition = Vector3.forward * 0.1f;
				}
				//cube.localRotation = Quaternion.LookRotation(referenceJointVec);*/




				/*Vector3 worldLive = j.transform.TransformDirection(liveJointVec);
				Vector3 worldRef = j.transform.TransformDirection(referenceJointVec);*/

				/*TrackedJoint j = liveBody.jointsByChild[t];
				if(j != null) {
					Debug.DrawRay(j.transform.position,liveJointVec,Color.yellow,0.2f);
					Debug.DrawRay(j.transform.position,referenceJointVec,Color.Lerp(Color.red,Color.green,complyValues[(int)t]),0.2f);
				}*/

				//#######DEBUG END


                totalError += error;
                numActive++;
            }
            //FindObjectOfType<JointHighlightController> ().DisplayJointCompliance (complyValues);

            //Second if: is this the frame that was best last time
			if (updateVisual && liveBody) {
				if ((referenceFrameNum == (int)currentFrame)) {
					if(liveBody.GetComponent<VertexColorJointHighlighter>())liveBody.GetComponent<VertexColorJointHighlighter>().DisplayJointCompliance(complyValues);
					
					if(liveBody.GetComponent<JointHighlightMeshManager>())liveBody.GetComponent<JointHighlightMeshManager>().DisplayJointCompliance(complyValues);

					FindObjectOfType<JointComplianceBars>().DisplayJointCompliance(complyValues);
				}
            }

            float avgError = totalError / numActive;

            return avgError;

			//Velocity tracking, inactive currently
            /*Vector3[] velDifferences = new Vector3[25];
            for (int j = 0; j < 25; j++) {
                Kinect.JointType t = (Kinect.JointType)j;
                if(t == Kinect.JointType.SpineBase)continue; //spinebase is child to no one, so skip
                if(currentFrameIgnoredStates != null && currentFrameIgnoredStates[(int)t] == true) {
                    complyValues[(int)t] = -501f;
                    continue; //joint ignored, dont care
                }
                numActive++;

                Vector3 now = GetJointDirectionVector(bodyOrientationFrameBuffer,(Kinect.JointType)j);
                now.z = 0f;
                currentAngularVelocities[j] = Vector3.Lerp(currentAngularVelocities[j],(now - lastRotations[j]),0.5f);
                if(frame == 1 && t == Kinect.JointType.ElbowLeft)print(currentAngularVelocities[j].magnitude); //debug
                velDifferences[j] = currentAngularVelocities[j] - refAngularVelocities[frame,j];
                if(frame == currentlyTrackingMove.frameLenght-1) lastRotations[j] = now;
            }
            float totalDiff = 0;
            for (int i = 0; i < 25; i++) {
                totalDiff += velDifferences[i].magnitude;
            }
            //print (totalDiff);
            return (totalDiff / numActive) * 300f;*/

        }

        //TODO: remove this from here, use the on in Utils
        //TODO:remember to think about local or world rotation, does it matter?
        Vector3 GetJointDirectionVector(BodyOrientationFrame orientationFrame, Kinect.JointType childType)
        {
            Quaternion rot = orientationFrame.jointFrames[(int)childType].jointRotLocal;

            if (childType == Kinect.JointType.SpineMid ||
               childType == Kinect.JointType.SpineShoulder ||
               childType == Kinect.JointType.Neck ||
               childType == Kinect.JointType.Head) return Utils.GetUpVector(rot);

            else if (childType == Kinect.JointType.KneeLeft ||
                    childType == Kinect.JointType.AnkleLeft ||
                    childType == Kinect.JointType.KneeRight ||
                    childType == Kinect.JointType.AnkleRight) return Utils.GetDownVector(rot);

            else if (childType == Kinect.JointType.HipLeft ||
                    childType == Kinect.JointType.ShoulderLeft ||
                    childType == Kinect.JointType.ElbowLeft ||
                    childType == Kinect.JointType.WristLeft) return Utils.GetLeftVector(rot);

            else if (childType == Kinect.JointType.HipRight ||
                    childType == Kinect.JointType.ShoulderRight ||
                    childType == Kinect.JointType.ElbowRight ||
                    childType == Kinect.JointType.WristRight) return Utils.GetRightVector(rot);

            //satisfy compiler
            else return Vector3.forward;
        }
		bool coolDown=false;
        private void CycleDone()
        {
			if (coolDown)
				return;
            print("Cycle done, cycles done before:" + cyclesCompleted);

            cyclesCompleted++;

            if (currentCycleNumberText) currentCycleNumberText.text = "Cycles competed: " + cyclesCompleted;

            //scoring
            float totalScore = 0;
            int scoredFramesCount = 0;
            float frameAverageScore = 0;
            for (int i = 0; i < currentClipLenght; i++)
            {
                if (frameScores[i] != null)
                {
                    totalScore += frameScores[i];
                    scoredFramesCount++;
                }
            }
            frameAverageScore = totalScore / scoredFramesCount;
            //lastCyclePointsText.text = "Cycle points:" + frameAverageScore;
            if (lastCyclePointsText) lastCyclePointsText.text = "p:" + System.Math.Round(frameAverageScore, 2) + " count:" + scoredFramesCount + "l:" + currentClipLenght;

            currentFrame = 0;

			lastFrame -= (float)currentClipLenght;
			lastProgressNormalized -= 1f;

			//TODO: remove all these, should be done with delegate or something
            if (parentRecognizer) parentRecognizer.Recognized(currentlyTrackingMove);
            //if (exerciseProgramTracker != null) exerciseProgramTracker.RepCompleted();
			//:DD
			//if(FindObjectOfType<RoundTrackingVisual>())FindObjectOfType<RoundTrackingVisual>().RepCompleteVisual();

			//print("LAST avg error:"+CalculateLastRepeatError().avgDeviationAngle);
			completedCycleThisFrame = true;
			if (usePurkkaCooldown) {
				currentSpeed =0;
//				StartCoroutine(CoolDown());
			}
        }
		IEnumerator CoolDown(){
			coolDown = true;
			yield return new WaitForSeconds (0.5f);
			coolDown = false;
		}
        public void StartTrackingMove(ClinicalMove moveToTrack)
        {
            currentlyTrackingMove = moveToTrack;
            StartTracking();
        }

        public void StartTracking()
        {
            print("orientationframes null CHECK REMOVED");
            if (attachedRepeater != null && (attachedRepeater.currentlyPlayingBack == null)) {
                print(this.GetType()+": no move loaded!");
				return;
            }

            currentFrame = 0f;
            maxError = currentlyTrackingMove.angleComparingStrictness;

            if (attachedRepeater) {
                currentlyTrackingMove = attachedRepeater.currentlyPlayingBack;
                attachedRepeater.currentPlayBackMode = PlayBackMode.Manual;
                attachedRepeater.SetFrame(0);
                attachedRepeater.Play();
            }

            frameScores = new float[currentlyTrackingMove.lenghtInFrames];
            cyclesCompleted = 0;
            trackingActive = true;

			//TODO: reverse depedency here
            if (lastCyclePointsText) lastCyclePointsText.gameObject.SetActive(true);
            if (currentCycleNumberText) currentCycleNumberText.gameObject.SetActive(true);            
            if (heatGraph) heatGraph.CreateGraph(currentlyTrackingMove.lenghtInFrames);
            if (currentCycleNumberText) currentCycleNumberText.text = "Cycles competed: 0";

            //For velocity tracking
            //calculate ref velocities
            /*refAngularVelocities = new Vector3[currentlyTrackingMove.lenghtInFrames, 25];

            for (int j = 0; j < 25; j++)
            {
                Vector3 last = currentlyTrackingMove.GetFrame(0).jointFrames[j].jointRotLocal.eulerAngles; //first frame is compared to itself
                last.z = 0f;
                for (int i = 0; i < currentlyTrackingMove.lenghtInFrames; i++)
                {
                    Vector3 now = GetJointDirectionVector(currentlyTrackingMove.GetFrame(i), (Kinect.JointType)j);
                    now.z = 0f;
                    Vector3 vel;
                    if (i == 0) vel = Vector3.zero;
                    else vel = now - last;
                    refAngularVelocities[i, j] = vel;
                    last = now;
                }
            }
            lastRotations = new Vector3[25];
            currentAngularVelocities = new Vector3[25];
            for (int i = 0; i < 25; i++)
            {
                lastRotations[i] = Vector3.zero;
                currentAngularVelocities[i] = Vector3.zero;
            }*/
        }

        public void StopTracking()
        {
            trackingActive = false;
            if(lastCyclePointsText) lastCyclePointsText.gameObject.SetActive(false);
            if(currentCycleNumberText)currentCycleNumberText.gameObject.SetActive (false);
            if(heatGraph) heatGraph.DestroyGraph();
			if(liveBody && liveBody.GetComponent<VertexColorJointHighlighter>()) liveBody.GetComponent<VertexColorJointHighlighter>().DisplayJointCompliance(Enumerable.Repeat(-501f, 25).ToArray());
        }
    }
}