﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace ClinicalLayer
{
    public abstract class BodyOrientationUtilizer : MonoBehaviour
    {
        public abstract void NewFrameAvailable(); //TODO: in all implementations of this, use currentProvider instead

		//public BodyOrientationProvider currentProvider { private set; get; } //NOTE: This doesn't seem to be used in many places, maybe it should be? //OR NOT?
		public BodyOrientationProvider currentProvider;
		private bool doDebug = true;


        public void SubscribeTo(BodyOrientationProvider provider) //TODO: maybe instead use currentProvider's setter / getter instead of these functions
        {
			if(doDebug)print(this + " subbing to:" + provider);
            if (!provider) return;
            if (currentProvider)
            {
				if(doDebug)print(this + " unsubbing from current first");
                UnSubscribeFrom(currentProvider);
            }
            provider.AddSubscriber(this);
            currentProvider = provider;
        }

        public void UnSubscribeFrom(BodyOrientationProvider provider)
        {
            if (!provider) return;
			if(doDebug)print(this + " unsubbing from " + provider);
            provider.RemoveSubscriber(this);
        }

		[ContextMenu("Resub")]
		void Resub() {
			SubscribeTo(currentProvider);
		}
    }
}