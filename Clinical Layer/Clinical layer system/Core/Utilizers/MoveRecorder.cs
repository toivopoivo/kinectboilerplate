using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
//using Serialization; //UnitySerializer
using ClinicalLayer;

namespace ClinicalLayer
{
	public enum RecordingSourceMode { FromLocal, FromNetwork, FromImport };
	
	public class MoveRecorder : BodyOrientationUtilizer
	{
		
		public KinectBodyOrientationSensor sensorSource;
		public BodyFrameReceiver networkBodySource;
		//public AnimationImportPlayer importSource;
		
		private BodyOrientationProvider bodySource
		{
			get
			{
				if (currentRecordingSourceMode == RecordingSourceMode.FromNetwork) return networkBodySource;
				else if (currentRecordingSourceMode == RecordingSourceMode.FromLocal) return sensorSource;
				//else return importSource;
				else return null;
			}
		}
		
		public RecordingSourceMode currentRecordingSourceMode
		{
			get
			{
				return m_currentRecordingSourceMode;
			}
			set
			{
				print(value);
				UnSubscribeFrom(bodySource);
				m_currentRecordingSourceMode = value;
				switch (value)
				{
				case RecordingSourceMode.FromLocal:
					SubscribeTo(sensorSource);
					break;
				case RecordingSourceMode.FromNetwork:
					SubscribeTo(networkBodySource);
					break;
				case RecordingSourceMode.FromImport:
					//print(importSource);
					//SubscribeTo(importSource);
					break;
				}
			}
		}
		private RecordingSourceMode m_currentRecordingSourceMode;
		
		public bool recording = false;
		public ClinicalMove currentlyRecording;
		
		public bool alterFPS = false;
		public float frameRate = 30;
		private float frameTime { get { return 1f / frameRate; } }
		
		private List<BodyOrientationFrame> buffer = new List<BodyOrientationFrame>();
		
		private BodyOrientationFrame bodyOrientationFrameBuffer;
		
		private float timer = 0;
		
		private ModelControllerFollow ignoreJointsReferenceBody;
		
		private System.Diagnostics.Stopwatch frameTimer = new System.Diagnostics.Stopwatch();
		
		public override void NewFrameAvailable()
		{
			long lastFrameDelta = frameTimer.ElapsedMilliseconds;
			frameTimer.Reset();
			frameTimer.Start();
			
			//print("FRAMETIME:"+lastFrameDelta);
			if (!alterFPS && recording)
			{
				CaptureFrame();
			}
		}
		
		// Update is called once per frame
		void Update()
		{
			/*if(Input.GetMouseButtonUp(1)) {
                if(!recording)StartRecording("test"+System.DateTime.Now.Ticks);
                else StopRecording(false);
            }*/
			
			if (alterFPS && recording)
			{
				timer -= Time.deltaTime;
				if (timer < 0)
				{
					timer += frameTime;
					CaptureFrame();
				}
			}
		}
		
		void CaptureFrame()
		{			
			bodySource.GetLatestFrame(out bodyOrientationFrameBuffer);
						
			BodyOrientationFrame frame = new BodyOrientationFrame();
			frame.jointFrames = new JointFrame[25];
			System.Array.Copy(bodyOrientationFrameBuffer.jointFrames, frame.jointFrames, 25);
			frame.rootPosition = bodyOrientationFrameBuffer.rootPosition;
			
			//Code below only works for the old authoring tool because it expects the ignored joints to be selected before recording
			if (ignoreJointsReferenceBody) frame.ignoredJoints = ignoreJointsReferenceBody.GetIgnoredJoints();
			else
			{
				frame.ignoredJoints = new bool[25];
				for (int j = 0; j < 25; j++) {
					frame.ignoredJoints[j] = true;
				}
				frame.ignoredJoints[(int)Windows.Kinect.JointType.WristRight] = false; //what
			}
			
			frame.isTracking = bodyOrientationFrameBuffer.isTracking;
			frame.quality = BodyOrientationFrameQuality.High;
			buffer.Add(frame);
			
			//print("Recorded frame!");
		}

		public void StartRecording(string moveName)
		{
			StartRecording(moveName, null);
		}
		
		public void StartRecording(string moveName/*, bool[] ignoredJoints*/, ModelControllerFollow jointsFromBody)
		{
			currentlyRecording = new ClinicalMove();
			currentlyRecording.moveName = moveName;
			currentlyRecording.frameRate = 30f; //Kinect is always 30 fps. (Do something else if adding support for other devices!)
			ignoreJointsReferenceBody = jointsFromBody;
			currentlyRecording.clipQuality = BodyOrientationFrameQuality.High;
			//currentlyRecording.jointsIgnored = ignoredJoints;
			recording = true;
			//currentlyRecording.orientationFrames = new BodyOrientationFrame[200];
			
			if (bodySource == null) Debug.LogWarning("Recording start requested but recorder has no source! It might not record any frames.");
		}
		
		public void AbandonRecording()
		{
			recording = false;
			buffer.Clear();
			print("Abandoned recording");
		}
		
		public void StopRecording()
		{
			recording = false;
			//currentlyRecording.orientationFrames = buffer.ToArray ();
			currentlyRecording.SetFrames(buffer.ToArray());
			buffer.Clear();
			
			currentlyRecording.moveNotes = "Recorded using Kinect on " + System.DateTime.Now.ToString("d.M.yyyy H:mm") + ".";
			
			print("Stopped recording, lenght:" + currentlyRecording.lenghtInFrames);
		}
	}
}
