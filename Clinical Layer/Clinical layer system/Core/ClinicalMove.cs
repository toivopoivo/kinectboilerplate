﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
//using Serialization; //UnitySerializer
using LitJson;
using Kinect = Windows.Kinect;
using System.Linq;
using ClinicalLayer;

namespace ClinicalLayer
{
    [System.Serializable]
    public class ClinicalMove
    {
        //FORMAT VERSION HISTORY:
        //Unversioned format 1: Inefficient UnitySerializer-utilizing binary format
        //Unversioned format 2: Efficient custom binary format created using BinaryReader and BinaryWriter
        //Unversioned format 3: Changed joint orientation interpretation from kinect2-oriented to so that in T-pose, joints have zero rotation
        //Version 1.0: 		Changed to human-readable JSON format, actual animation data retained as binary contained in a base64-encoded string
        //Version 1.001:	Renamed frameLenght to lenghtInFrames to avoid confusion

        //As functions to avoid being serialized as LitJSON doesn't allow much control in what gets put to the file (Even constants, wtf)...
        public static int GetGeneratorFileFormatVersionMajor() { return 1; }
        public static int GetGeneratorFileFormatVersionMinor() { return 1; }

        public static string GetgeneratorVersionVerbose()
        {
            return GetGeneratorFileFormatVersionMajor() + "." + GetGeneratorFileFormatVersionMinor();
        }

        public int fileFormatVersionMajor;
        public int fileFormatVersionMinor;

        public string GetFileFormatVersionVerbose()
        {
            return fileFormatVersionMajor + "." + fileFormatVersionMinor;
        }

        public string moveName;
        public string moveNotes = "";

        public int unLoadedLenghtInFrames; //Lenght of move when it was saved. This exists so that the lenght can be read without reading the actual animation data
        public float frameRate;
        public float angleComparingStrictness;
        public BodyOrientationFrameQuality clipQuality; //This exists both for a clip and for a single frame. Maybe change at some point? TODO

        //[System.NonSerialized]

        /*public BodyOrientationFrame[] orientationFrames {
            //get; private set;
            get {
                return m_orientationFrames;
            }
            private set {
                m_orientationFrames = value;
            }
        }
        private BodyOrientationFrame[] m_orientationFrames;*/

        private BodyOrientationFrame[] orientationFrames;

        public string orientationFrameDataString;

        public void SetFrames(BodyOrientationFrame[] inFrames)
        {
            orientationFrames = inFrames;
        }

        public int lenghtInFrames
        {
            get { return orientationFrames.Length; }
        }
        public float timeLenght
        {
            get { return (float)lenghtInFrames / frameRate; }
        }

        public BodyOrientationFrame GetFrame(int index)
        {
            return orientationFrames[index];
        }
        public BodyOrientationFrame[] GetFrames()
        {
            return orientationFrames;
        }

        //RENAMED PROPS
        /*public int frameLenght {
            set {
                lenghtInFrames = value;
            }
        }*/

        /*//New constructor, from JSON
        //NOPE, can't assign to "this"
        public SavedMove(string dataString) {
            this = JsonMapper.ToObject (dataString);
            ReadAnimFromBytes (System.Convert.FromBase64String (orientationFrameDataString));
        }*/

        //use this instead
        public static ClinicalMove CreateFromJson(string dataString)
        {
            Debug.Log("New deserialization (from-JSON) used!");
            System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();
            timer.Start();

            ClinicalMove created = JsonMapper.ToObject<ClinicalMove>(dataString);
            //Debug.Log (dataString.Substring(0,200)+" (cut)");
            created.ReadAnimFromBytes(System.Convert.FromBase64String(created.orientationFrameDataString)); //TODO: make this not always forced, allow reading animation data later or not at all
            if (created.frameRate < 0.1f) created.frameRate = 30f;
            if (created.angleComparingStrictness < 0.1f) created.angleComparingStrictness = 30f; //TODO: I don't can't

            Debug.LogWarning("Parsing ClinicalMove \""+created.moveName+"\" competed in " + timer.ElapsedMilliseconds + "ms");
            timer.Stop();
            return created;
        }

        //New serialization as JSON and animation data and base64 (part of json)
        public string ToJSON(BodyOrientationFrameQuality quality)
        {
            Debug.Log("New serialization (to-JSON) used!");
            System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();
            timer.Start();
            byte[] animData = AnimDataToBytes(quality);
            orientationFrameDataString = System.Convert.ToBase64String(animData);
            unLoadedLenghtInFrames = lenghtInFrames;
            fileFormatVersionMajor = GetGeneratorFileFormatVersionMajor();
            fileFormatVersionMinor = GetGeneratorFileFormatVersionMinor();
            StringWriter stringWriter = new StringWriter();
            JsonWriter prettyJsonWriter = new JsonWriter(stringWriter);
            prettyJsonWriter.PrettyPrint = true;
            JsonMapper.ToJson(this, prettyJsonWriter);
            Debug.LogWarning("Serializing ClinicalMove competed in " + timer.ElapsedMilliseconds + "ms");
            timer.Stop();
            return stringWriter.ToString();
            //return JsonMapper.ToJson (this);
        }

        //Overload (uses current quality of clip)
        public string ToJSON()
        {
            return ToJSON(clipQuality);
        }


        //New animation-only-part-to-bytes
        //TODO: count byte amount? or no need?
        public byte[] AnimDataToBytes(BodyOrientationFrameQuality clipQuality)
        {
            Debug.Log("Serializing animation frames to bytes");
            Stream s = new MemoryStream();
            BinaryWriter bw = new BinaryWriter(s);

            for (int i = 0; i < orientationFrames.Length; i++)
            {
                bw.Write(orientationFrames[i].ToBytes(clipQuality));
            }
            bw.Flush();

            byte[] bytes = new byte[s.Length];

            s.Position = 0;
            s.Read(bytes, 0, (int)s.Length);
            bw.Close();

            return bytes;
        }

        //New animation-from-bytes (from gotten JSON base64 part)
        public void ReadAnimFromBytes(byte[] bytes)
        {
            Debug.Log("Reading bytes to animation, lenght " + bytes.Length);
            System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();
            timer.Start();

            Stream s = new MemoryStream(bytes);
            BinaryReader br = new BinaryReader(s);

            orientationFrames = new BodyOrientationFrame[unLoadedLenghtInFrames];
            int frameByteLenght = BodyOrientationFrame.frameSizes[clipQuality];
            for (int i = 0; i < unLoadedLenghtInFrames; i++)
            {
                orientationFrames[i] = new BodyOrientationFrame(br.ReadBytes(frameByteLenght)); //lol hack //TODO:FIX
            }
            br.Close();

            timer.Stop();
            Debug.Log("Reading bytes to animation done in " + timer.ElapsedMilliseconds+" ms");
        }

        //Old binary-only constructor
        public ClinicalMove(byte[] bytes)
        {
            Debug.LogWarning("Old deserialization (from-bytes) used!");
            //Debug.LogWarning("Reading move, lenght "+bytes.Length);
            SetDefaultValues();

            Stream s = new MemoryStream(bytes);
            BinaryReader br = new BinaryReader(s);

            moveName = br.ReadString();
            unLoadedLenghtInFrames = br.ReadInt32();
            clipQuality = (BodyOrientationFrameQuality)br.ReadByte();
            //frameRate = B

            orientationFrames = new BodyOrientationFrame[unLoadedLenghtInFrames];
            int frameByteLenght = BodyOrientationFrame.frameSizes[clipQuality];
            for (int i = 0; i < unLoadedLenghtInFrames; i++)
            {
                orientationFrames[i] = new BodyOrientationFrame(br.ReadBytes(frameByteLenght)); //lol hack //TODO:FIX
            }
            br.Close();
            frameRate = 30f; //hardcoded, as in the old format there's no data about this
            angleComparingStrictness = 30f; //same
        }

        //parameterless constructor, forcibly making it visible, and adding some default settings
        public ClinicalMove()
        {
            SetDefaultValues();
        }

        void SetDefaultValues()
        {
			fileFormatVersionMajor = GetGeneratorFileFormatVersionMajor();
			fileFormatVersionMinor = GetGeneratorFileFormatVersionMinor();
            moveName = "Unnamed move";
            angleComparingStrictness = 30f;
        }

        //Old binary-only serialization
        public byte[] ToBytes(BodyOrientationFrameQuality clipQuality)
        {
            Debug.LogWarning("Old serialization (to-bytes) used!");
            Stream s = new MemoryStream();
            BinaryWriter bw = new BinaryWriter(s);

            bw.Write(moveName);
            bw.Write(orientationFrames.Length);
            bw.Write((byte)clipQuality);
            //bw.Write (frameRate);
            //bw.Write (angleComparingStrictness);

            for (int i = 0; i < orientationFrames.Length; i++)
            {
                bw.Write(orientationFrames[i].ToBytes(clipQuality));
            }
            bw.Flush();

            byte[] bytes = new byte[s.Length];

            s.Position = 0;
            s.Read(bytes, 0, (int)s.Length);
            bw.Close();

            return bytes;
        }

        public void SetIgnoredJointsForAllFrames(bool[] igns)
        {
            for (int i = 0; i < lenghtInFrames; i++)
            {
                orientationFrames[i].ignoredJoints = igns;
            }
        }

        /*public void AutomaticallySetIgnoredJoints () {
            //Automatically set the active / not active joints depending on how much movement there was
            //TODO: do this, get some code from BVHimporter PrintDebugStats function
            throw new System.NotImplementedException();
        }*/

        //Get the amount of "uncovered" rotation, so don't add if there's rotation delta towards back to a position where already been (simply get max angle compared to start angle)
        public bool[] CalculateMovedJoints(float treshold)
        {
            float[] maxDeltasFromStartPerJoint = new float[25];
            for (int i = 0; i < 25; i++)
            {
                maxDeltasFromStartPerJoint[i] = 0;
            }

            //GC avoidance
            float diffFromStart = 0;
            Vector3 startDir;
            Vector3 currDir;

            int startFrame = 0;
            int endFrame = orientationFrames.Length - 1;

            //This can (should) be commented out in other uses than clinical studio (unity app)
            if (ClinicalMovePropertiesControl.instance.cuttingTool != null)
            {
                startFrame = Mathf.RoundToInt(ClinicalMovePropertiesControl.instance.cuttingTool.activeStartFrame);
                endFrame = Mathf.RoundToInt(ClinicalMovePropertiesControl.instance.cuttingTool.activeEndFrame);
            }

            for (int jointIndex = 0; jointIndex < 25; jointIndex++)
            {
                //Go through all frames of a single joint
                startDir = Utils.GetJointDirectionVector(orientationFrames[0].jointFrames[jointIndex].jointRotLocal, (Kinect.JointType)jointIndex);
                for (int frameIndex = startFrame; frameIndex <= endFrame; frameIndex++)
                {
                    //Compare all the frames to the start frame, get the maximum difference in angle compared to the first frame
                    //diffFromStart = Quaternion.Angle(orientationFrames[0].jointFrames[jointIndex].jointRotLocal,orientationFrames[frameIndex].jointFrames[jointIndex].jointRotLocal); //have to ignore z, so this doesn't work
                    currDir = Utils.GetJointDirectionVector(orientationFrames[frameIndex].jointFrames[jointIndex].jointRotLocal, (Kinect.JointType)jointIndex);

                    diffFromStart = Vector3.Angle(startDir, currDir);

                    if (diffFromStart > maxDeltasFromStartPerJoint[jointIndex])
                    {
                        maxDeltasFromStartPerJoint[jointIndex] = diffFromStart;
                    }
                }
            }

            bool[] ignoreds = Enumerable.Repeat(true, 25).ToArray();
            for (int i = 0; i < 25; i++)
            {
                //Debug.Log((Windows.Kinect.JointType)i+":"+maxDeltasFromStartPerJoint[i]);
                if (maxDeltasFromStartPerJoint[i] > treshold) ignoreds[i] = false;
            }
            return ignoreds;
        }
    }
}