using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//using Microsoft.Kinect.Face;
using Kinect = Windows.Kinect;
using System.Text;

namespace ClinicalLayer
{
    public class KinectBodyOrientationSensor : BodyOrientationProvider
    {
        public bool multipleBodies = false;
        public int thisBodyNum;
		
        private Kinect.Body thisBody;
        private BodySourceManager bodyManager;

		private bool haveFocusBody = false;
		private ulong focusBodyID;

        private TrackedJoint[] realJoints;
        private Quaternion[] tPoseRotations;
        //private Vector3 rootPosition = Vector3.zero;
        private Vector3 rootPosition
        {
            get
            {
                return GetRootPosition();
            }
        }

        public Dictionary<Kinect.JointType, TrackedJoint> jointsByChild = new Dictionary<Kinect.JointType, TrackedJoint>();

        public bool isTracking
        {
            get
            {
                if (debugForceTracked) return true;
                return m_tracking;
            }
            set
            {
                m_tracking = value;
            }
        }
        private bool m_tracking = false;

        private bool haveNewRootPos;
        private Vector3 cachedRootPos;

        private bool getNewData;
        private BodyOrientationFrame buffer;

		private float feetToPelvisHeight = 0.91f;  //TODO: get this by calibrating

        public Transform rotAdjuster;
        public Transform posAdjuster;
        public Transform posRef;

        //DEBUG
        public bool debug = false;
        public Vector3 leftFootRotOffset;
        public Vector3 rightFootRotOffset;
        public bool showAllGizmos = true;
        public bool debugForceTracked = false; //REMEMBER TO TURN OFF!
        public GameObject bonePrefab;

        public bool zeroedBoxesDebug = false;
        public Transform[] zeroedBoxes = new Transform[25];
        public GameObject zeroedBoxPrefab;

		public GameObject topClipEdge;
		public GameObject rightClipEdge;
		public GameObject bottomClipEdge;
		public GameObject leftClipEdge;

        void Awake()
        {
            BodySourceManager.instance.Subscribe(this);
        }

        void Start()
        {
            transform.rotation = Quaternion.identity;
            //debug, create zeroed boxes
            if (zeroedBoxesDebug)
            {
                Vector3 basePos = new Vector3(0, 100, 0);
                for (int i = 0; i < 25; i++)
                {
                    if (/*Utils.IsTipJoint(t) || */(Kinect.JointType)i == Kinect.JointType.SpineBase) continue;
                    if (Utils.JointTypeIgnored((Kinect.JointType)i)) continue;
                    GameObject box = Instantiate(zeroedBoxPrefab) as GameObject;
                    box.transform.position = new Vector3(basePos.x + i * 4, basePos.y, basePos.z);
                    box.transform.localScale = Vector3.one * 3;
                    box.name = ((Kinect.JointType)i).ToString();
                    zeroedBoxes[i] = box.transform;
                }
            }

            bodyManager = BodySourceManager.instance;

            realJoints = GetComponentsInChildren<TrackedJoint>(); //get all the joints in the hierarchy
            GetJointsForDictionary();
            CreateBoxSkeleton();

            buffer = new BodyOrientationFrame();
            buffer.jointFrames = new JointFrame[25];

            ReadTPoseRotations();
        }

        //Calibrates the body so that in the current position, all reported joint rotations are zeroed. Body needs to be in T-pose!
        void ReadTPoseRotations()
        {
            StringBuilder sb = new StringBuilder();
            tPoseRotations = new Quaternion[25];
            for (Kinect.JointType t = 0; t < (Kinect.JointType)25; t++)
            {
                if (!jointsByChild.ContainsKey(t))
                {
                    sb.Append(t.ToString() + " ");
                    continue;
                }
                //tPoseRotations[(int)t] = jointsByChild[t].frame.jointRotLocal;
                tPoseRotations[(int)t] = jointsByChild[t].transform.rotation;
            }
            Debug.LogWarning("Base skeleton lacks joints: " + sb.ToString());
        }

        void Update()
        {
            //debug, zeroed boxes
            if (zeroedBoxesDebug)
            {
                for (Kinect.JointType t = 0; t < (Kinect.JointType)25; t++)
                {
                    if (zeroedBoxes[(int)t] == null) continue;
                    if (!jointsByChild.ContainsKey(t)) continue;
                    Quaternion zeroed = jointsByChild[t].transform.localRotation * Quaternion.Inverse(tPoseRotations[(int)t]);
                    zeroedBoxes[(int)t].rotation = zeroed;
                    Vector3 temp = zeroedBoxes[(int)t].eulerAngles;
                    temp.y = 0;
                    zeroedBoxes[(int)t].eulerAngles = temp;

                    //print("should be zeroed: "+zeroed.eulerAngles);
                }
            }

			//try {
				/*if(thisBody.TrackingId.ToString() == KinectActiveAreaBodySelectorSource.instance.GetMaximumPriorityMarker().name) {
					topClipEdge.SetActive((thisBody.ClippedEdges == Kinect.FrameEdges.Top));
					rightClipEdge.SetActive((thisBody.ClippedEdges == Kinect.FrameEdges.Right));
					bottomClipEdge.SetActive((thisBody.ClippedEdges == Kinect.FrameEdges.Bottom));
					leftClipEdge.SetActive((thisBody.ClippedEdges == Kinect.FrameEdges.Left));
				}*/
			//}
			/*catch {
				
			}*/
        }

        public override void GetLatestFrame(out BodyOrientationFrame returnbuffer)
        {
            if (!getNewData)
            { //TODO:RE-ENABLE!
                returnbuffer = buffer;
                return;
            }

            //debug test delta
            float delta = 0;

            Quaternion[] temprots = new Quaternion[25];

            int i = 0;
            for (Kinect.JointType t = 0; t < (Kinect.JointType)25; t++)
            {
                if (/*Utils.IsTipJoint(t) || */t == Kinect.JointType.SpineBase) continue;
                if (Utils.JointTypeIgnored(t)) continue;
                i = (int)t;

                //OLD : get local rotation of joint
                buffer.jointFrames[i] = jointsByChild[t].frame;

                //twist axis removal
                /*Vector3 tempRot = buffer.jointFrames[i].jointRotLocal.eulerAngles;
                tempRot.x = 0;
                buffer.jointFrames[i].jointRotLocal.eulerAngles = tempRot;*/

                /*//TEST : report rotations which are zeroed in T-pose
                //Do it by getting the difference between the T-pose and current pose
                //reported pose = current - original
                //buffer.jointFrames[i] = jointsByChild[t].frame;
                //buffer.jointFrames[i].jointRotLocal *= Quaternion.Inverse(tPoseRotations[i]); //TODO:test!

                //Quaternion tempRot = jointsByChild[t].transform.rotation;
                //temprots[(int)t] = jointsByChild[t].transform.localRotation;

                //jointsByChild[t].transform.rotation *= Quaternion.Inverse(tPoseRotations[i]);

                //jointsByChild[t].transform.eulerAngles = jointsByChild[t].transform.eulerAngles - tPoseRotations[i].eulerAngles;



                buffer.jointFrames[i] = new JointFrame();
                buffer.jointFrames[i].jointRotLocal = jointsByChild[t].transform.localRotation * Quaternion.Inverse(tPoseRotations[(int)t]);

                //buffer.jointFrames[i].jointRotLocal = Quaternion.Euler(jointsByChild[t].transform.localEulerAngles); //Quaternion.Euler(jointsByChild[t].transform.localEulerAngles - tPoseRotations[i].eulerAngles);



                //debug test delta
                delta += Quaternion.Angle(jointsByChild[t].transform.rotation,Quaternion.identity);*/

            }
            buffer.rootPosition = rootPosition;
            buffer.isTracking = isTracking;
            buffer.ignoredJoints = new bool[25];

            returnbuffer = buffer;

            getNewData = false;

            //debug test delta
            //print (delta/14);

            //reset rots
            /*for (Kinect.JointType t = 0; t < (Kinect.JointType)25; t++) {
                if(!jointsByChild.ContainsKey(t))continue;
                jointsByChild[t].transform.localRotation = temprots[(int)t];
                //print(t);
            }*/
        }

		public ulong GetCurrentBodyID() {
			return thisBody.TrackingId;
		}

        void GetJointsForDictionary()
        {
            foreach (TrackedJoint joint in realJoints)
            {
                if (joint.childJoint == null) continue;
                jointsByChild.Add(joint.childJoint.thisType, joint);
            }
        }
		
        public void NewBodyFrameArrived()
        {

            getNewData = true;

            isTracking = false; //assume body is not available, but set it to available if we get through these checks


            Kinect.Body[] bodies = bodyManager.GetData();

            if (bodies == null)
            {
                if (debug) print("Could not get body array!");
                return;
            }

            /*if(debugText)debugText.text = "";
            foreach(Kinect.Body b in data) {
                if(debugText)debugText.text+="Body:"+b.IsTracked+"\n";
            }*/

            thisBody = null;

            if (multipleBodies) {
                thisBody = bodies[thisBodyNum];
            }

            else {
                for (int i = 0; i < bodies.Length; i++)
                {
					if(!haveFocusBody) {
						if (bodies[i].IsTracked) {
							//Tracking obtained
							haveFocusBody = true;
							focusBodyID = bodies[i].TrackingId;
							thisBody = bodies[i];
						}
					}
					else {
						if(bodies[i].TrackingId == focusBodyID) {
							if(bodies[i].IsTracked)thisBody = bodies[i];
							else {
								//Tracking lost
								haveFocusBody = false;
							}
						}
					}
                }
            }

            if (thisBody == null || !thisBody.IsTracked)
            {
                if (debug) print("No bodies or set body not tracking!");
				haveFocusBody = false;

                //rootPosition = new Vector3(0,0,-3f); //default to a position 3 meters away from the sensor
                posAdjuster.localPosition = new Vector3(0, 0, -3f);
                return;
            }
			else posAdjuster.localPosition = Vector3.zero;

            isTracking = true; //got through null and tracking checks, so body should be available

            Kinect.Vector4 kinectVector;
            Quaternion properRot;

            foreach (TrackedJoint joint in realJoints)
            {
                Kinect.JointType childJointType;
                if (!joint.childJoint)
                {
                    continue; //this is end joint and has no children, don't try to rotate
                }
                childJointType = joint.childJoint.thisType;

                //debug
                Debug.DrawLine(joint.transform.position, joint.childJoint.transform.position);

                kinectVector = thisBody.JointOrientations[childJointType].Orientation;
                if (childJointType == Kinect.JointType.Head) kinectVector = bodyManager.headRot; //PURRKKa TODO
                properRot = Quaternion.identity; //do not set to zero if bone is ignored!

                properRot = new Quaternion(-kinectVector.X, -kinectVector.Y, kinectVector.Z, kinectVector.W); //convert rotation from right-handed system to left-handed system rotation

                //if(childJointType == Kinect.JointType.HandRight)print(properRot + " should work");
                //if(childJointType == Kinect.JointType.HandTipRight)print(properRot + " should not be same");


                //Set tracking to false if there's any edge clipping
                //if (thisBody.ClippedEdges != Kinect.FrameEdges.None) isTracking = false; //TODO: maybe re-enable?

				if(thisBody.ClippedEdges == Kinect.FrameEdges.Left || thisBody.ClippedEdges == Kinect.FrameEdges.Right) isTracking = false;
				

                //feet don't work, set manually
                /*if(joint.childJoint.thisType == Kinect.JointType.FootLeft) {
                    properRot = (jointsByChild[joint.thisType].transform.rotation*Quaternion.Euler(leftFootRotOffset));
                }
                if(joint.childJoint.thisType == Kinect.JointType.FootRight) {
                    properRot = (jointsByChild[joint.thisType].transform.rotation*Quaternion.Euler(rightFootRotOffset));
                }*/

                //Add some upwards rotation to shoulders, otherwise avatars look wrong
                if (childJointType == Kinect.JointType.ShoulderLeft)
                {
                    properRot *= Quaternion.Euler(0, 0, 12);
                }
                if (childJointType == Kinect.JointType.ShoulderRight)
                {
                    properRot *= Quaternion.Euler(0, 0, -12);
                }


                joint.transform.rotation = properRot; //use raw input, do smoothing down the line

                //floor plane adjustment
                //if(childJointType == Kinect.JointType.SpineMid || childJointType == Kinect.JointType.HipLeft || childJointType == Kinect.JointType.HipRight) {
                if (joint.thisType == Kinect.JointType.SpineBase)
                {

                }

                //This allows for rotations to be reported so that they are zeroed in T-pose)
                if (!(joint.thisType == Kinect.JointType.Head || joint.thisType == Kinect.JointType.Neck || joint.thisType == Kinect.JointType.AnkleLeft || joint.thisType == Kinect.JointType.AnkleRight))
                {
                    joint.transform.rotation *= Quaternion.Inverse(tPoseRotations[(int)joint.childJoint.thisType]);
                }

                //test delta test
                //joint.transform.rotation = tPoseRotations[(int)joint.childJoint.thisType];
            }

            //Adjust root for floor level
            /*if(Mathf.FloorToInt(Time.time) % 2 == 0) {
                //joint.transform.rotation *= bodyManager.floorAdjustmentQuat;
                //lol
                rotAdjuster.rotation = bodyManager.floorAdjustmentQuat;
                //transform.rotation = bodyManager.floorAdjustmentQuat;
                Debug.LogError("ioasio");
            }*/
            rotAdjuster.rotation = bodyManager.floorAdjustmentQuat;
            posAdjuster.rotation = Quaternion.Inverse(bodyManager.floorAdjustmentQuat);
            posRef.localPosition = CameraSpaceToUnityWorldSpace(thisBody.Joints[Kinect.JointType.SpineBase].Position);

            //test delta test
            /*for (Kinect.JointType t = 0; t < (Kinect.JointType)25; t++) {
                if(!jointsByChild.ContainsKey(t))continue;
                jointsByChild[t].transform.rotation = tPoseRotations[(int)t];
            }*/

            //rootPosition = GetRootPosition ();

            NotifySubscribersAboutNewFrame();
        }
		
        private Vector3 GetRootPosition()
        {
            /*if(getNewData) {
                cachedRootPos =	CameraSpaceToUnityWorldSpace(thisBody.Joints[Kinect.JointType.SpineBase].Position);
                //Take in to account camera height from ground
                cachedRootPos.y += bodyManager.floorPlane.W; //TODO: test with 2 persons
                //Root is applied to pelvis, but ground is at 0, so apply modifier to keep character on ground
                cachedRootPos.y += feetToPelvisHeight;

                print(bodyManager.floorPlane.W);
            }
            return cachedRootPos;*/

            //return posRef.position; //world position
            Vector3 vec = (posRef.position - transform.position); //because this actually isn't at zero
            vec.y -= feetToPelvisHeight;
            vec.y += bodyManager.floorPlane.W;
            return vec;
        }

        Vector3 CameraSpaceToUnityWorldSpace(Kinect.CameraSpacePoint campoint)
        {
            Vector3 worldpoint = new Vector3(campoint.X, campoint.Y, -campoint.Z);
            return worldpoint;
        }

		//TODO: maybe remove this and use the proper class
        [ContextMenu("Clear box skeleton")]
        void ClearBoxSkeleton()
        {
            List<GameObject> toDelete = new List<GameObject>();

            Transform[] allChildren = GetComponentsInChildren<Transform>();
            foreach (Transform child in allChildren)
            {
                if (child.name == "boxjoint") toDelete.Add(child.gameObject);
            }
            print(toDelete.Count);

            while (toDelete.Count > 0)
            {
                DestroyImmediate(toDelete[0]);
                toDelete.RemoveAt(0);
            }
        }

        [ContextMenu("Do box skeleton")]
        void DoBoxSkeleton()
        {
            realJoints = GetComponentsInChildren<TrackedJoint>(); //get all the joints in the hierarchy
            CreateBoxSkeleton();
        }

        void CreateBoxSkeleton()
        {

            foreach (TrackedJoint joint in realJoints)
            {
                if (!joint.childJoint) continue;
                GameObject box = GameObject.Instantiate(bonePrefab) as GameObject;
                box.name = "boxjoint";

                Vector3 thisPos = joint.transform.position;
                Vector3 childPos = joint.childJoint.transform.position;

                Vector3 middlePos = Vector3.Lerp(thisPos, childPos, 0.5f);

                box.transform.position = middlePos;

                box.transform.parent = joint.transform;
                //box.transform.localPosition = Vector3.zero;
                box.transform.localRotation = Quaternion.identity;

                float boxLenght = 0.8f * (Vector3.Distance(thisPos, childPos));

                box.transform.localScale = new Vector3(box.transform.localScale.x, boxLenght, box.transform.localScale.z);

            }
        }

#if UNITY_EDITOR
        void OnEnable()
        {
            UnityEditor.SceneView.onSceneGUIDelegate += OnSceneGUI;
        }

        void OnSceneGUI(UnityEditor.SceneView sceneview)
        {
            //if(!Application.isPlaying)return;
            if (!showAllGizmos) return;
            foreach (TrackedJoint realJoint in realJoints)
            {
                UnityEditor.Handles.PositionHandle(realJoint.transform.position, realJoint.transform.rotation);
                UnityEditor.Handles.Label(realJoint.transform.position, realJoint.thisType.ToString());
            }
        }
#endif
    }
}