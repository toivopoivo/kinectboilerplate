using UnityEngine;
using System.Collections;
using System.IO;
//using Serialization; //UnitySerializer

namespace ClinicalLayer
{
    public enum PlayBackMode { Free, Manual, EditPreview }; //free advances the animation in normal time. Manual expects changing frames by hand. EditPreview loops the active area in cutter.

    public class MoveRepeater : BodyOrientationProvider
    {

        public PlayBackMode currentPlayBackMode = PlayBackMode.Free;

        //[HideInInspector]
        public ClinicalMove currentlyPlayingBack { get; private set; }

        public int currentFrameNumber
        {
            get { return m_currentFrameNumber; }
            private set
            {
                if (value == m_currentFrameNumber) return;
                if (currentlyPlayingBack == null)
                {
                    Debug.LogWarning(this + ": Nothing playing!");
                    return;
                }
                if (value > (currentlyPlayingBack.lenghtInFrames - 1))
                {
                    Debug.LogWarning("Attempted to set frame number out of bounds");
                    return;
                }
                m_currentFrameNumber = value;
                NotifySubscribersAboutNewFrame();
                //print("Frame "+value+" set. Time:"+Time.time);
            }
        }
        private int m_currentFrameNumber = 0;

        private float currentTime = 0;

        public float frameRate
        {
            get { return m_frameRate; }
            set
            {
                currentTime = currentTime * (m_frameRate / value);
                m_frameRate = value;
            }
        }
        private float m_frameRate = 30;

        private float frameTime { get { return 1f / frameRate; } }

        private float timer = 0;

        public bool playing = false;

        private BodyOrientationFrame buffer;

        private string clipToLoadPath;

        [HideInInspector]
        public float clipLoadProgress = 0;

        public bool juryRig = false;
		public string juryRigClip = "kadetheiluu.clinicalMove";

        // Use this for initialization
        void Start()
        {
            //currentlyPlayingBack = null; //Why was this here? broke shit. Possibly as this needed to be null and inspector set it to not null?

            if (juryRig)
            {
                JuryRig();
            }
        }

        [ContextMenu("Jury rig")]
        void JuryRig()
        {
            //LoadClip(Utils.savedMovesFolder + "/" + juryRigClip);
			SetMoveToPlay(ClinicalMove.CreateFromJson(File.ReadAllText(Utils.appFilesPath + "/" + juryRigClip)));
            if (GetComponent<ModelControllerFollow>())
            {
                print("juryrigging played clip");
                if (GetComponent<ModelControllerFollow>()) GetComponent<ModelControllerFollow>().currentFollowMode = FollowMode.Recording;
				/*FindObjectOfType<MoveRepeatTracker>().bodySource = this;
				print(FindObjectOfType<MoveRepeatTracker>());*/
            }
			Play();
        }

        public void SetMoveToPlay(ClinicalMove move)
        {
            currentlyPlayingBack = move;
            currentFrameNumber = 0;
            frameRate = move.frameRate;
        }

        public void LoadClip(string fileName)
        {
            print("Clip loading path:\n" + fileName);
            Stop();
            currentlyPlayingBack = null;
            clipToLoadPath = fileName;
            StartCoroutine("LoadClipAsync");
        }

        //Call when setting from anywhere else than timer, as this also sets the timer
        public void SetFrame(int frame)
        {
            currentFrameNumber = frame;
            if (currentlyPlayingBack != null) currentTime = (float)frame / currentlyPlayingBack.frameRate;
            else currentTime = 0;
        }

        public void SetTime(float time)
        {
            currentTime = time;
            currentFrameNumber = Mathf.FloorToInt(time * frameRate);
        }

        public void Play()
        {
            /*if(currentlyPlayingBack == null) {
                Debug.LogWarning("Cannot play, no clip loaded!");
                return;
            }*/
            playing = true;
        }

        public void Pause()
        {
            playing = false;
        }

        public void Stop()
        {
            playing = false;
            currentFrameNumber = 0;
        }

        IEnumerator LoadClipAsync()
        {
            string pathForWWW = Utils.GetFilePathPrefixForWWW() + clipToLoadPath;
            pathForWWW = pathForWWW.Replace(" ", "%20"); //TODO:maybe use WWW.encodingthingwhatever to account for other special characters as well
            //print (pathForWWW);

            print(name + ": Loading clip from path:" + pathForWWW + " asynchronously");
            WWW asyncLoader = new WWW(pathForWWW);

            while (!asyncLoader.isDone)
            {
                clipLoadProgress = asyncLoader.progress;
                print("loading WWW... " + clipLoadProgress * 100 + "%");
                yield return null;
            }
            clipLoadProgress = 1;

            print(name + ": Loading WWW done, now deserializing");

            bool isNewFormat = (clipToLoadPath.Contains(".move"));
            print("is new format:" + isNewFormat);
            if (isNewFormat)
            {
                currentlyPlayingBack = new ClinicalMove(asyncLoader.bytes);
            }
            else
            {
                //currentlyPlayingBack = (SavedMove)UnitySerializer.Deserialize(asyncLoader.bytes);
                Debug.LogError("Support for this old crap is gone!");
            }

            print(name + ": Deserializing done, clip loaded");
            //if(asd)asd.sharedMaterial = origMat;
            Play();
        }

        public override void GetLatestFrame(out BodyOrientationFrame returnbuffer)
        {
            //print("tried to get frame "+currentFrameNumber+" current recording:"+currentlyPlayingBack.moveName+" lenght:"+currentlyPlayingBack.orientationFrames.Length);
            //print (name);
            returnbuffer = currentlyPlayingBack.GetFrame(currentFrameNumber);
            //print (returnbuffer.rootPosition);
        }

        void Update()
        {
            //print (name+" "+(currentlyPlayingBack == null));
            if (currentlyPlayingBack == null || /*currentlyPlayingBack.orientationFrames == null ||*/ currentlyPlayingBack.moveName == "") return; //TODO TODO TODO: check if the null check is necessary!

            int frameNumberToSet = currentFrameNumber;
            if ((currentPlayBackMode == PlayBackMode.Free || currentPlayBackMode == PlayBackMode.EditPreview) && playing)
            {
                //timer -= Time.deltaTime;
                currentTime += Time.deltaTime;
                if (Mathf.FloorToInt(currentTime * frameRate) != currentFrameNumber)
                {
                    //frameNumberToSet = Mathf.FloorToInt(currentTime * frameRate);
                    currentFrameNumber = Mathf.Clamp(Mathf.FloorToInt(currentTime * frameRate), 0, currentlyPlayingBack.lenghtInFrames - 1);
                    //print(name);
                }
            }

            //looping behaviour
            if (currentPlayBackMode == PlayBackMode.Free || currentPlayBackMode == PlayBackMode.Manual)
            {
                //if(currentFrameNumber > currentlyPlayingBack.frameLenght - 2) {
                if (currentTime > currentlyPlayingBack.timeLenght)
                {
                    SetTime(0);
                    //frameNumberToSet = 0;
                    //if(RepeaterControl.instance != null && (this == RepeaterControl.instance.repeater))RepeaterControl.instance.tracker.CycleDone();
                }

            }
            else if (currentPlayBackMode == PlayBackMode.EditPreview)
            {
                //if(currentFrameNumber>=RepeaterControl.instance.cutter.activeEnd)frameNumberToSet = (int)RepeaterControl.instance.cutter.activeStart;
                if (currentFrameNumber >= ClinicalMovePropertiesControl.instance.cuttingTool.activeEndFrame) SetFrame((int)ClinicalMovePropertiesControl.instance.cuttingTool.activeStartFrame);
                //print("calc lenght:"+currentlyPlayingBack.timeLenght);
                //print("curr time:"+currentTime);
                ClinicalMovePropertiesControl.instance.cuttingTool.PlayingTick(currentTime / currentlyPlayingBack.timeLenght);
            }
            //set the actual frame number only once as it has a special setter
            //currentFrameNumber = frameNumberToSet;
        }
    }
}