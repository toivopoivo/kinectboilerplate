﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace ClinicalLayer
{
    public abstract class BodyOrientationProvider : MonoBehaviour
    {
        private List<BodyOrientationUtilizer> subscribers = new List<BodyOrientationUtilizer>();
		public float lastFrameTimeStamp = 0f;

        public abstract void GetLatestFrame(out BodyOrientationFrame buffer);

        public void AddSubscriber(BodyOrientationUtilizer subscriber)
        {
            subscribers.Add(subscriber);
        }

        public void RemoveSubscriber(BodyOrientationUtilizer unsubscriber)
        {
            subscribers.Remove(unsubscriber);
        }

        public void NotifySubscribersAboutNewFrame()
        {
			lastFrameTimeStamp = Time.time;
            foreach (BodyOrientationUtilizer utilizer in subscribers)
            {
                utilizer.NewFrameAvailable();
            }
        }

		public float GetTimeSinceLastFrame() {
			return Time.time - lastFrameTimeStamp;
		}
    }
}