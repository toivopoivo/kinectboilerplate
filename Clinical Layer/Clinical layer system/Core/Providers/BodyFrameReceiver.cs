using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//using Serialization; //UnitySerializer

namespace ClinicalLayer
{
    public class BodyFrameReceiver : BodyOrientationProvider
    {
        private BodyOrientationFrame buffer;

		private long bytesReceived = 0;
        
		//void SetJointData(byte[] jointData) ORIGINAL
		//CHANGE 28.2.2015 to string because crashbug
	//	[RPC]
        void SetJointData(byte[] jointData)
        {
			bytesReceived += jointData.Length;
			//print("Bytes received: "+Utils.ByteLenghtToHumanReadable(bytesReceived,false));

            //buffer = (BodyOrientationFrame)UnitySerializer.Deserialize(jointData); //binary
            //buffer = new BodyOrientationFrame ();
            //UnitySerializer.DeserializeInto (jointData, buffer); //better binary?
            //buffer = (BodyOrientationFrame)UnitySerializer.JSONDeserialize (System.Text.Encoding.Default.GetString(jointData)); //JSON

            buffer = new BodyOrientationFrame(jointData); //badass ORIGINAL
			//buffer = new BodyOrientationFrame(/*MoveSender.GetBytes(jointData)*/System.Convert.FromBase64String(jointData));

            //print("Received frame! Data lenght:"+jointData.Length);
            //print (buffer.ToString ());
            NotifySubscribersAboutNewFrame();
        }

        public override void GetLatestFrame(out BodyOrientationFrame returnbuffer)
        {
            returnbuffer = buffer;
        }
    }
}