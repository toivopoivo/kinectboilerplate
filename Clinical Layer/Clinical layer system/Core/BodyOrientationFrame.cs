﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using ClinicalLayer;

namespace ClinicalLayer
{
    public enum BodyOrientationFrameQuality { Low, Med, High };

    //[System.Serializable]
    public struct BodyOrientationFrame
    {
        //Exact bytes sizes for serialized frames of different qualities. A bit hacky I guess...
        public static Dictionary<BodyOrientationFrameQuality, int> frameSizes = new Dictionary<BodyOrientationFrameQuality, int>() {
		{BodyOrientationFrameQuality.Low, 93},
		{BodyOrientationFrameQuality.Med, 168},
		{BodyOrientationFrameQuality.High, 418},
	};

        public bool isTracking;
        public BodyOrientationFrameQuality quality;
        public Vector3 rootPosition;
        public bool[] ignoredJoints;
        public JointFrame[] jointFrames; //25 long, slot for every joint. Contains joint rotations by child joint type (so ElbowLeft slot contains rotation for ShoulderLeft) This also means some are empty.

        public BodyOrientationFrame(byte[] bytes)
        {
            Stream s = new MemoryStream(bytes);
            BinaryReader br = new BinaryReader(s);

            isTracking = br.ReadBoolean();
            quality = (BodyOrientationFrameQuality)br.ReadByte();
            rootPosition.x = br.ReadSingle();
            rootPosition.y = br.ReadSingle();
            rootPosition.z = br.ReadSingle();

            ignoredJoints = new bool[25];
            BitArray bits = Utils.GetBitArrayFromInt(br.ReadInt32());
            for (int i = 0; i < 25; i++)
            {
                ignoredJoints[i] = bits[i];
            }

            jointFrames = new JointFrame[25];

            for (int i = 0; i < 25; i++)
            {
                if (quality == BodyOrientationFrameQuality.High)
                {
                    jointFrames[i].jointRotLocal.x = br.ReadSingle();
                    jointFrames[i].jointRotLocal.y = br.ReadSingle();
                    jointFrames[i].jointRotLocal.z = br.ReadSingle();
                    jointFrames[i].jointRotLocal.w = br.ReadSingle();
                    //Debug.Log("POS:"+s.Position+" i:"+i);
                }
                else if (quality == BodyOrientationFrameQuality.Med)
                {
                    jointFrames[i].jointRotLocal = Quaternion.Euler((float)br.ReadUInt16(), (float)br.ReadUInt16(), (float)br.ReadUInt16());
                }
                else if (quality == BodyOrientationFrameQuality.Low)
                {
                    jointFrames[i].jointRotLocal = Quaternion.Euler((float)br.ReadByte() * 1.411f, (float)br.ReadByte() * 1.411f, (float)br.ReadByte() * 1.411f);
                }
            }

            br.Close();
            s.Dispose();
        }

        public byte[] ToBytes(BodyOrientationFrameQuality quality)
        {
            Stream s = new MemoryStream();
            BinaryWriter bw = new BinaryWriter(s);

            bw.Write(isTracking);
            bw.Write((byte)quality);
            bw.Write(rootPosition.x);
            bw.Write(rootPosition.y);
            bw.Write(rootPosition.z);

            bw.Write(Utils.GetIntFromBitArray(new BitArray(ignoredJoints))); //this could be optimized maybe

            for (int i = 0; i < 25; i++)
            {
                if (quality == BodyOrientationFrameQuality.High)
                {
                    bw.Write(jointFrames[i].jointRotLocal.x);
                    bw.Write(jointFrames[i].jointRotLocal.y);
                    bw.Write(jointFrames[i].jointRotLocal.z);
                    bw.Write(jointFrames[i].jointRotLocal.w);
                    //Debug.Log("POS:"+s.Position+" i:"+i);
                }
                else if (quality == BodyOrientationFrameQuality.Med)
                {
                    Vector3 vec = jointFrames[i].jointRotLocal.eulerAngles;
                    bw.Write((ushort)vec.x);
                    bw.Write((ushort)vec.y);
                    bw.Write((ushort)vec.z);
                }
                else if (quality == BodyOrientationFrameQuality.Low)
                {
                    Vector3 vec = jointFrames[i].jointRotLocal.eulerAngles;
                    bw.Write((byte)(vec.x / 1.411f));
                    bw.Write((byte)(vec.y / 1.411f));
                    bw.Write((byte)(vec.z / 1.411f));
                    //Debug.LogError("ORIG:"+vec.x+" 8-BIT:"+(byte)(vec.x/1.411f));
                }
            }

            bw.Flush();

            byte[] bytes = new byte[s.Length];

            s.Position = 0;
            s.Read(bytes, 0, (int)s.Length);
            bw.Close();
            s.Dispose();

            return bytes;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("BodyOrientationFrame object:");
            sb.AppendLine("Tracking:" + isTracking);
            sb.AppendLine("Root pos:" + rootPosition.ToString());

            //byte[] bytes = ToBytes (); //TODO: delete
            //sb.AppendLine (Encoding.Default.GetString(bytes));
            //sb.AppendLine ("RAW lenght:" + bytes.Length);

            return sb.ToString();
        }
    }
}