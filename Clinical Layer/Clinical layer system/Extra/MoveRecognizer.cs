using UnityEngine;
using System.Collections;
using System.IO;
using ClinicalLayer;

public class MoveRecognizer : MonoBehaviour {

	public MoveRepeatTracker[] trackers;
	public string gesturesFolder; //load all moves from this folder

	public ModelControllerFollow liveBody;
	
	void Start () {
		//set reference so that they know who to call (not ghostbusters but this object)
		for (int i = 0; i < trackers.Length; i++) {
			trackers[i].parentRecognizer = this;
			trackers[i].liveBody = liveBody;
		}

		ResetMoves();

	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Space)) {
			string[] filenames = Directory.GetFiles(Utils.savedMovesFolder+"/"+gesturesFolder);
			for (int i = 0; i < filenames.Length; i++) {
				if(filenames[i].Contains(".meta"))continue;
				print(filenames[i]);
				AddMoveToTrack(new ClinicalMove(File.ReadAllBytes(filenames[i])));
			}
		}
	}

	//stop tracking all moves
	void ResetMoves() {
		for (int i = 0; i < trackers.Length; i++) {
			trackers[i].gameObject.SetActive(false);
		}
	}

	//stop tracking a single move
	void StopTrackingMove(ClinicalMove move) {
		for (int i = 0; i < trackers.Length; i++) {
			if(trackers[i].currentlyTrackingMove == move) {
				trackers[i].gameObject.SetActive(false);
				print("Stopped tracking "+move.moveName+".");
				return;
			}
		}
		Debug.LogWarning("Not tracking "+move.moveName+"!");
	}

	//add move to ones being tracked
	void AddMoveToTrack(ClinicalMove move) {
		for (int i = 0; i < trackers.Length; i++) {
			if(trackers[i].currentlyTrackingMove == move) {
				Debug.LogWarning("Already tracking "+move.moveName+"!");
				return;
			}
		}

		for (int i = 0; i < trackers.Length; i++) {
			if(!trackers[i].gameObject.activeSelf) {
				trackers[i].gameObject.SetActive(true);
				trackers[i].currentlyTrackingMove = move;
				trackers[i].StartTracking();
				//trackers[i].GetComponent<PreviewPlayer>().PlayPreview(move.moveName);
				print("Started tracking "+move.moveName+"!");
				return;
			}
		}
		Debug.LogWarning("Cannot track "+move.moveName+"! Already tracking max amount of moves. Stop tracking something else first");
	}

	//Called from trackers
	public void Recognized (ClinicalMove currentlyTrackingMove)
	{
		print("Recognized "+currentlyTrackingMove.moveName+"!");

		//Set all tracker's current frames to 0, to nullify any other expectations up to this point
		foreach (MoveRepeatTracker tracker  in trackers) {
			tracker.currentFrame = 0;
		}
	}
}
