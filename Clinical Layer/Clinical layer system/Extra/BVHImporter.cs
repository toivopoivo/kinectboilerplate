using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Kinect = Windows.Kinect;
using System.Linq;
using ClinicalLayer;

public enum ChannelType {Xposition,Yposition,Zposition,Xrotation,Yrotation,Zrotation}

//Joints defined in the BVH header are saved as ImportJoints
[System.Serializable]
public class ImportJoint {
	public string sourceName; //waist, lShldr, etc
	public ChannelType[] channelTypes;
	public Vector3 offset; //The initial position of the joint, related to the parent joint. Not used in Clinical layer

	public bool isEndSite = false;
	public int hierarchyLevel;

	//The actual frame-specific data, accessed by the channel type enum, and frame number
	//Clinical layer uses 3 rotations channels for all joints, and for root also the position channels
	public Dictionary<ChannelType,float[]> channelsData;

	public Quaternion GetRotQuat(int frameNum) {
		//return Quaternion.Euler (GetVec (frameNum));

		//Transform temp = new GameObject().transform;
		//Transform temp = BVHImporter.tempTransform;
		//temp.localRotation = Quaternion.identity;

		Quaternion firstQuat = Quaternion.identity;
		Quaternion secondQuat = Quaternion.identity;
		Quaternion thirdQuat = Quaternion.identity;

		Quaternion finalQuat = Quaternion.identity;

		float angle = 0;

		int count = 0;

		Quaternion qvaak = Quaternion.identity;
		foreach(var axis in channelTypes) {

			angle = channelsData[axis][frameNum];


			//TODO: find out how rotations could be applied in right order by rotating axle by axle in a test script, comparing to transform.Rotate functions
			if(axis == ChannelType.Xrotation) {
				//temp.Rotate(Vector3.right,angle, relativeTo:Space.Self); //works
				//temp.Rotate(temp.right,angle, relativeTo:Space.World); //works
				qvaak = Quaternion.AngleAxis(angle,Utils.GetRightVector(qvaak)) * qvaak; //wait what, works
				//qvaak = Quaternion.Euler(angle,0,0);
			}
			else if(axis == ChannelType.Yrotation) {
				//temp.Rotate(-Vector3.up,angle, relativeTo:Space.Self);
				//temp.Rotate(-temp.up,angle, relativeTo:Space.World);
				qvaak = Quaternion.AngleAxis(angle,Utils.GetDownVector(qvaak)) * qvaak;
				//qvaak = Quaternion.Euler(0,-angle,0);
			}
			else if(axis == ChannelType.Zrotation) {
				//temp.Rotate(-Vector3.forward,angle, relativeTo:Space.Self);
				//temp.Rotate(-temp.forward,angle,relativeTo:Space.World);
				qvaak = Quaternion.AngleAxis(angle,Utils.GetBackwardVector(qvaak)) * qvaak;
				//qvaak = Quaternion.Euler(0,0,-angle);
			}

			if(count == 0) {
				firstQuat = qvaak;
			}
			else if(count == 1) {
				secondQuat = qvaak;
			}
			else if(count == 2) {
				thirdQuat = qvaak;
			}
			count++;
		}
		//hack rigged 
		/*firstQuat = Quaternion.AngleAxis(channelsData[ChannelType.Xrotation][frameNum],Vector3.right);
		secondQuat = Quaternion.AngleAxis(channelsData[ChannelType.Yrotation][frameNum],Vector3.up);
		thirdQuat = Quaternion.AngleAxis(channelsData[ChannelType.Zrotation][frameNum],Vector3.forward);*/

		//GameObject.Destroy (temp.gameObject);

		//return temp.localRotation;
		return qvaak;

		/*finalQuat *= thirdQuat;
		finalQuat *= secondQuat;
		finalQuat *= firstQuat;
		return finalQuat;*/

		//return firstQuat * secondQuat * thirdQuat; //doesn't work, dunno why
		//return thirdQuat * secondQuat * firstQuat;
		//return Quaternion.Euler (firstQuat.eulerAngles + secondQuat.eulerAngles + thirdQuat.eulerAngles);// wat
	}

	//Attempts to get the proper euler representation of the data
	public Vector3 GetRot (int frameNum)
	{
		return GetRotQuat (frameNum).eulerAngles;
	}

	//Returns vector with the angles set to the data. May not be the right way?
	public Vector3 GetVec (int frameNum)
	{
		Vector3 vec;
		vec.x = channelsData[ChannelType.Xrotation][frameNum];
		vec.y = channelsData[ChannelType.Yrotation][frameNum];
		vec.z = channelsData[ChannelType.Zrotation][frameNum];
		return vec;
	}

	public Vector3 GetPos (int frameNum)
	{
		Vector3 vec;
		vec.x = channelsData[ChannelType.Xposition][frameNum];
		vec.y = channelsData[ChannelType.Yposition][frameNum];
		vec.z = channelsData[ChannelType.Zposition][frameNum];
		return vec;
	}

	public void SetPos (int i, Vector3 posToSet)
	{
		channelsData [ChannelType.Xposition][i] = posToSet.x;
		channelsData [ChannelType.Yposition] [i] = posToSet.y;
		channelsData [ChannelType.Zposition] [i] = posToSet.z;
	}
}

[System.Serializable]
public class BVHAnimation {
	public List<ImportJoint> importJoints = new List<ImportJoint> ();
	public int frameCount;
	public float frameTime;
}

public class BVHImporter : MonoBehaviour {

	public static BVHImporter instance;

	public string importPath;

	//public string mappingFilePathUnderStreamingAssets;
	public TextAsset mappingFile;

	public Dictionary<string,Kinect.JointType> rigBonesToJointTypeMapping = new Dictionary<string, Kinect.JointType>();
	
	[HideInInspector]
	private string importedData;

	private ClinicalMove importedMove;
	
	public bool usePositionData = false;
	public float scaleModifier = 0.01f;

	private string loadedFilePath;
	
	private bool debugHeaderParsing = false;
	private bool debugDataParsing = false;

	private string[] lines;
	private string[] frames;
	private int currLineIndex;

	public BVHAnimation currentBVH  /*{ private set; get; }*/; //TODO maybe cannot be property?
	public ClinicalMove lastImported { private set; get; }

	void Awake() {
		if(!instance)instance = this;
		else {
			Debug.LogError("Multiple BVH importers! Singleton pattern in use, so destroying the extra one.");
			Destroy(this);
		}
	}

	void PrintParts (string[] parts)
	{
		//debug
		StringBuilder sb = new StringBuilder ("Parts of line:\n");
		for (int i = 0; i < parts.Length; i++) {
			sb.AppendLine(i+"#"+parts[i]);
		}	
		print(sb.ToString());
	}

	string[] GetParts(string line) {
		//remove tabs and spaces from start
		while(line.Length > 0 && (line[0] == '\t' || line[0] == ' ')) {
			line = line.Substring(1);
		}
		//Remove line break from end
		if(line[line.Length-1] == '\n' || line[line.Length-1] == '\r') {
			line = line.Substring(0,line.Length-1);
			//print("Removed linebreak from "+line);
		}
		string[] parts = line.Split(new char[]{' ','\t'},System.StringSplitOptions.RemoveEmptyEntries);
		//PrintParts (parts);
		return parts;
	}

	void LoadMapping() {
		print ("Loading BVH joint name -> Kinect joint type mapping file");
		rigBonesToJointTypeMapping.Clear ();
		//string[] mappings = File.ReadAllLines (mappingFilePath);
		//string[] mappings = File.ReadAllText (Application.streamingAssetsPath+"/"+mappingFilePathUnderStreamingAssets).Split ('\n');
		string[] mappings = mappingFile.text.Split ('\n','\r');

		print("Loading BVH->Kinect joint mappings file:");
		PrintParts(mappings);

		foreach(string mapping in mappings) {
			if(mapping.Length == 0)continue;
			if(mapping[0] == '#')continue;
			string[] parts = mapping.Split(';');
			if(parts == null || parts.Length != 2) {
				//print("ignoring:"+mapping);
				continue;
			}
			Kinect.JointType readType = (Kinect.JointType)System.Enum.Parse(typeof(Kinect.JointType),parts[0]);

            //string originalJointName = parts[1].Substring(0,parts[1].Length-1);
			string originalJointName = parts[1];

			if(rigBonesToJointTypeMapping.ContainsKey(originalJointName)) {
				Debug.LogWarning(parts[1]+ " is defined twice in: "+/*mappingFilePathUnderStreamingAssets*/"the mapping text file"+"!");
			}
			else {
				rigBonesToJointTypeMapping.Add(originalJointName,readType);
			}
		}

		StringBuilder sb = new StringBuilder();
		sb.AppendLine("Loaded mapping file. Mappings:\n");

		Kinect.JointType[] tempArr = (Kinect.JointType[])System.Enum.GetValues(typeof(Kinect.JointType));
		List<Kinect.JointType> jointTypesUnUsed = tempArr.ToList();

		foreach(var pair in rigBonesToJointTypeMapping) {
			sb.AppendLine(pair.Value+":"+pair.Key);
			if(jointTypesUnUsed.Contains(pair.Value))jointTypesUnUsed.Remove(pair.Value);
		}
		sb.AppendLine();

		sb.AppendLine("Kinect joint types not mapped to anything:");
		foreach(var type in jointTypesUnUsed) {
			sb.AppendLine(type.ToString());
		}

		print(sb.ToString());
	}

	void LoadFile(string path) {
		print ("BVH importer: loading:"+path);
		loadedFilePath = path;
		importedData = File.ReadAllText (path);		
		lines = importedData.Split ('\n');

		currLineIndex = 0;

		currentBVH = new BVHAnimation ();

		System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch ();
		timer.Start ();

		//Run the main functions
		ReadHeaders ();

		long headersTime = timer.ElapsedMilliseconds;
		Debug.LogWarning ("HEADERS "+headersTime+"ms");
		timer.Reset ();
		timer.Start ();

		ReadChannelData ();

		long channelDataTime = timer.ElapsedMilliseconds;
		Debug.LogWarning ("CHANNELDATA "+channelDataTime+"ms");
		timer.Reset ();
		timer.Start ();

		ConstructSavedMove ();

		long savedMoveTime = timer.ElapsedMilliseconds;
		Debug.LogWarning ("SAVEDMOVE "+savedMoveTime+"ms");

		Debug.LogWarning ("TOTAL time for importing BVH: "+(headersTime+channelDataTime+savedMoveTime)+"ms");
		
		PrintDebugStats ();
		//Debug.LogWarning ("DEBUGSTATS"+timer.ElapsedMilliseconds);

		//temporary lol
		//FindObjectOfType<BVHDebugSkeletonBuilder> ().BuildDebugSkelly (currentBVH);
	}

	void ReadHeaders() {
		currentBVH.importJoints.Clear ();
		
		//find "HIERARCHY" keyword
		for (int i = 0; i < lines.Length; i++) {
			if(lines[i].StartsWith("HIERARCHY")) {
				currLineIndex = i+1;
				print("Found hierarchy start at "+currLineIndex);
				break;
			}
		}

		int currHierarcyLevel = 0;
		
		ImportJoint newJointTemp;
		while(true) {
			//print("new line:"+currLineIndex);
			string[] currentLineParts = GetParts(lines[currLineIndex]);

			bool isEndSite = currentLineParts[0] == "End";
			
			if(currentLineParts[0] == "ROOT" || currentLineParts[0] == "JOINT" || isEndSite) {
			
				newJointTemp = new ImportJoint();

				if(!isEndSite) {
					newJointTemp.sourceName = currentLineParts[1]/*.Substring(0,currentLineParts[1].Length-1)*/;//TODO: check if works?
					//newJointTemp.sourceName = currentLineParts[1];
				}
				else newJointTemp.sourceName = "ENDSITE";
				//print("new: "+newJointTemp.sourceName);				

				newJointTemp.isEndSite = isEndSite;

				currLineIndex++;
				if(lines[currLineIndex].Contains("{"))currHierarcyLevel++;
				currLineIndex++;
				
				//offset
				currentLineParts = GetParts(lines[currLineIndex]);

				if(currLineIndex != 3) { //offset of root is weirdly delimited, but it's usually 0 anyway
					newJointTemp.offset = new Vector3(float.Parse(currentLineParts[1]),float.Parse(currentLineParts[2]),float.Parse(currentLineParts[3])) * scaleModifier;
				}
				else newJointTemp.offset = Vector3.zero;
				//print ("Set offset to:"+newJointTemp.offset);

				//level in hierarchy. Parent is always previous joint with one smaller level
				newJointTemp.hierarchyLevel = currHierarcyLevel;

				if(!isEndSite) {
					//channels
					currLineIndex++;
					//currentLineParts = lines[currLineIndex].Split(' ');
					currentLineParts = GetParts(lines[currLineIndex]);
					//PrintParts(currentLineParts);
					
					//remove line break at the end of last part (in some cases there isn't a line break there so check)
					string lastPart = currentLineParts[currentLineParts.Length-1];
					//temp = temp.Replace("\n","");
					//if(lastPart.Substring(lastPart.Length-2,1) == System.Environment.NewLine) {

					/*if(lastPart[lastPart.Length-1] == '\n' || lastPart[lastPart.Length-1] == '\r') { //damn different types of line breaks
						lastPart = lastPart.Substring(0,lastPart.Length-1);
						currentLineParts[currentLineParts.Length-1] = lastPart;
					}*/

					//PrintParts(currentLineParts);
					newJointTemp.channelTypes = new ChannelType[int.Parse(currentLineParts[1])]; //The number of channels is stated in the BVH file
					//print("channel amount:"+newJointTemp.channelTypes.Length);
					
					int channelIndex = 0;
					while(channelIndex < newJointTemp.channelTypes.Length) {
						string channelTag = currentLineParts[channelIndex+2];
						//print(channelTag);
						
						string[] tags = System.Enum.GetNames(typeof(ChannelType));
						bool found = false;
						for (int i = 0; i < tags.Length; i++) {
							if(channelTag == tags[i]) {
								newJointTemp.channelTypes[channelIndex] = (ChannelType)i;
								found = true;
								//print("is:"+tags[i]);
								break;
							}
							else if(debugHeaderParsing)print(channelTag+" is not "+tags[i]+" lenghts:"+channelTag.Length+", "+tags[i].Length);
						}
						if(!found) {
							Debug.LogError("Channel type unknown:\n"+channelTag+channelTag+" on "+newJointTemp.sourceName);
							return;
						}
						channelIndex++;
					}
				}
				currentBVH.importJoints.Add(newJointTemp);
			}
			else if(currentLineParts.Last() == "}") {
				currHierarcyLevel--; 
			}
			
			else if(/*lines[currLineIndex] == "MOTION"*/ lines[currLineIndex].Contains("MOTION")) { //y u
				currLineIndex++;
				currentBVH.frameCount = int.Parse (GetParts (lines [currLineIndex]).Last());
				currLineIndex++;
				currentBVH.frameTime = float.Parse(GetParts(lines[currLineIndex]).Last());
				currLineIndex++;
				break;
			}
			//else print("Not same\n"+lines[currLineIndex]+"\nMOTION");
			currLineIndex++;
		}
	}
	
	void ReadChannelData() {
		int dataStartLine = currLineIndex;
		
		/*//data start finding loop
		int dataStartLine = 0;
		for (int i = 0; i < lines.Length; i++) {
			if(lines[i].StartsWith("Frame Time:")) {
				dataStartLine = i+1;
				print("data starts at line index:"+dataStartLine);
				break;
			}
		}*/
			
		print ("Start parsing channel data");
		
		frames = new string[(lines.Length - 1) - dataStartLine];
		if(frames.Length != currentBVH.frameCount)Debug.LogWarning("Number of frames stated in file("+currentBVH.frameCount+") different from actual("+frames.Length+").");

		System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch ();
		timer.Start ();
		System.Array.Copy (lines, dataStartLine, frames, 0, frames.Length);
		
		//Now we have the frame data, start parsing
		
		foreach(ImportJoint ij in currentBVH.importJoints) {
			//ij.channelsData = new float[ij.channelTypes.Length,frames.Length];
			ij.channelsData = new Dictionary<ChannelType, float[]> (); //works here while importjoint is a class, not while it is a struct. wth
		}

		
		for (int parsingFrameNum = 0; parsingFrameNum < frames.Length; parsingFrameNum++) {
			//print("Parsing data, frame:"+parsingFrameNum);

			//separate by space, resulting parts are simple floats
			//string[] floats = frames[parsingFrameNum].Split(' ');
			string[] floats = GetParts(frames[parsingFrameNum]);
			//print("floats count:"+floats.Length);
			
			//print("last float:"+floats[floats.Length-1]);

			int errorCount = 0;
			//go through all joints in order, give them floats in order (should result in them being distributed correctly)
			//print(rigBonesToJointTypeMapping.Count);
			int floatsIndex = 0;
			foreach(ImportJoint ij in currentBVH.importJoints) {
				if(ij.isEndSite)continue; //End sites don't have any data, only a static offset
				
				foreach(ChannelType channel in ij.channelTypes) {					
					//print("Parse:"+floats[floatsIndex]);
					if(debugDataParsing && parsingFrameNum == 5) print("Parsing channel data from float index "+floatsIndex+", Original joint name: "+ij.sourceName+", channel: "+channel);
					
					try {							
						//New (7.8.2014)
						if(!ij.channelsData.ContainsKey(channel)) {
							if(ij.channelsData.Keys.Count > 6) {
								Debug.LogError("Too many channel keys, did you export the BVH's with scale data (preserve scale or whatever checkbox)? Don't do that.");
								throw new System.Exception("Too many channel keys, did you export the BVH's with scale data (preserve scale or whatever checkbox)? Don't do that.");
								return;
							}
							ij.channelsData.Add(channel,new float[frames.Length]);
							//print("Added channel "+channel.ToString());
						}
						//Actually parsing and setting the data
						ij.channelsData[channel][parsingFrameNum] = float.Parse(floats[floatsIndex]);												
					}
					catch(System.Exception e) {
						if(floatsIndex > floats.Length-1) Debug.LogError("Error while parsing BVH floats, out of range! pos:"+floatsIndex+", floats lenght:"+floats.Length);
						else Debug.LogError("Error while parsing BVH floats:"+floats[floatsIndex]);
						print("extra details:"+e);
						errorCount++;
					}	
					
					floatsIndex++;
					if(errorCount > 60) {
						Debug.LogError("Too many errors while parsing BVH floats, giving up.");
						throw new System.Exception("Too many errors while parsing BVH floats, giving up.");
					}
				}
			}
		}					
		
		Debug.LogWarning("Channel data parsing done.");
	}

	void ConstructSavedMove() {
		print("Creating SavedMove from channel data");
		LoadMapping ();
		//Construct the SavedMove out of the parsed data
		importedMove = new ClinicalMove ();
		importedMove.moveName = "Imported: "+new DirectoryInfo(loadedFilePath).Name;
		importedMove.moveNotes = "Imported from BVH file "+new DirectoryInfo(loadedFilePath).Name+".";
		importedMove.clipQuality = BodyOrientationFrameQuality.High;
		importedMove.frameRate = 1f / currentBVH.frameTime;

		//importedMove.orientationFrames = new BodyOrientationFrame[frames.Length];
		BodyOrientationFrame[] framesToSet = new BodyOrientationFrame[frames.Length];
		
		for (int frameNum = 0; frameNum < frames.Length; frameNum++) {
			
			BodyOrientationFrame bodyFrame = new BodyOrientationFrame();
			bodyFrame.ignoredJoints = new bool[25];
			bodyFrame.jointFrames = new JointFrame[25];
			
			int foundcount = 0;
			Vector3 posVec = Vector3.zero; //only one position vector is processed, that is the root position
			if(usePositionData)posVec = currentBVH.importJoints[0].GetPos(frameNum) * scaleModifier;
			//posVec.x = -posVec.x; //REVERT!
			bodyFrame.rootPosition = posVec;


			//Set all to zero at first so if something doesn't get assigned, it doesn't produce an error
			for (int i = 0; i < bodyFrame.jointFrames.Length; i++) {
				bodyFrame.jointFrames[i] = new JointFrame();
				bodyFrame.jointFrames[i].jointRotLocal = Quaternion.identity;
			}
			
			foreach(ImportJoint ij in currentBVH.importJoints) {
				Quaternion rotVecQuat = Quaternion.identity;
				
				if(!rigBonesToJointTypeMapping.ContainsKey(ij.sourceName)) {
					/*string error = "Joint of name not found: "+ij.sourceName+ij.sourceName.Length+" available joints:\n";
					
					foreach(string key in rigBonesToJointTypeMapping.Keys) {
						error+=key+key.Length+"\n";
					}*/
					//if(frameNum == 0)Debug.LogWarning(error);
					//floatsIndex += ij.channelTypes.Length;
					continue;
				}
				Kinect.JointType type = rigBonesToJointTypeMapping[ij.sourceName];
				//if(frameNum == 0)print(ij.sourceName+" kinect type: "+type);

				//Channel data is set, now read them as a rotation
				rotVecQuat = ij.GetRotQuat(frameNum);				
								
				bodyFrame.jointFrames[(int)type] = new JointFrame();

				//what need to do: make hip drive both thighs?
                //This is wrong, as here the rotation if the parent joint is added after the rotation of the child joints is set
				/*if(type == Kinect.JointType.HipLeft || type == Kinect.JointType.HipRight) {
					rotVecQuat = bodyFrame.jointFrames[(int)Kinect.JointType.SpineMid].jointRotLocal*  rotVecQuat;
					//rotVecQuat = bodyFrame.jointFrames[(int)Kinect.JointType.SpineMid].jointRotLocal;
				}*/

				//Kinect hierarchy has left and right hip separately, so they need to be rotated to the same rotation as root
                //HipRight and HipLeft aren't yet set at this point, so this acts the same as applying hierarchical rotations in "parent->child->child's child" order
				if(type == Kinect.JointType.SpineMid) {
					bodyFrame.jointFrames[(int)Kinect.JointType.HipLeft].jointRotLocal *= rotVecQuat;
					bodyFrame.jointFrames[(int)Kinect.JointType.HipRight].jointRotLocal *= rotVecQuat;
				}

				bodyFrame.jointFrames[(int)type].jointRotLocal = rotVecQuat; //new

				
				foundcount++;
			}

			//print("founds:"+foundcount);		
			framesToSet[frameNum] = bodyFrame;
		}
		importedMove.SetFrames (framesToSet);
		lastImported = importedMove;
	}
	
	void PrintDebugStats() {
		float[] jointDeltaTotalsAll = new float[currentBVH.importJoints.Count];

		float[] jointDeltaTotalsX = new float[currentBVH.importJoints.Count];
		float[] jointDeltaTotalsY = new float[currentBVH.importJoints.Count];
		float[] jointDeltaTotalsZ = new float[currentBVH.importJoints.Count];
		
		//get the total amounts the joints rotated (cumulative delta) for all the joints, including those ignored		
		for (int i = 0; i < currentBVH.importJoints.Count; i++) {
			if(currentBVH.importJoints[i].isEndSite)continue;

			Quaternion prevRot;
			for (int frame = 1; frame < frames.Length; frame++) { //start from second frame, because the loop compares current to previous
				Quaternion lastRot = currentBVH.importJoints[i].GetRotQuat(frame-1);
				Quaternion currRot = currentBVH.importJoints[i].GetRotQuat(frame);
				float diff = Quaternion.Angle(lastRot,currRot);
				if(Mathf.Approximately(diff,180))diff = 0;
				jointDeltaTotalsAll[i] += diff;

				Vector3 currRotEuler = currRot.eulerAngles;
				Vector3 lastRotEuler = lastRot.eulerAngles;

				float xDelta = Mathf.Abs(currRotEuler.x - lastRotEuler.x);
				float yDelta = Mathf.Abs(currRotEuler.y - lastRotEuler.y);
				float zDelta = Mathf.Abs(currRotEuler.z - lastRotEuler.z);

				if(xDelta > 350)xDelta = 0;
				if(yDelta > 350)yDelta = 0;
				if(zDelta > 350)zDelta = 0;

				jointDeltaTotalsX[i] += xDelta;
				jointDeltaTotalsY[i] +=	yDelta;			
				jointDeltaTotalsZ[i] += zDelta;

				//if(i == 0)print("Y change for frame "+frame+"for hip:"+yDelta+" ("+currRotEuler+"-"+lastRotEuler+")");
			}
		}
		
		StringBuilder sb = new StringBuilder ();
		sb.AppendLine("Stats for joint movements during the move:");
		
		for (int i = 0; i < currentBVH.importJoints.Count; i++) {
			string usedSuffix = "";
			if(rigBonesToJointTypeMapping.ContainsKey(currentBVH.importJoints[i].sourceName))usedSuffix += " (Used,"+rigBonesToJointTypeMapping[currentBVH.importJoints[i].sourceName].ToString()+")";

			string totalsForAxlesText = " X:"+jointDeltaTotalsX[i]+" Y:"+jointDeltaTotalsY[i]+" Z:"+jointDeltaTotalsZ[i];
			sb.AppendLine(currentBVH.importJoints[i].sourceName+": "+jointDeltaTotalsAll[i]+usedSuffix+totalsForAxlesText);
		}
		sb.Append ("###END###");
		Debug.LogWarning (sb.ToString ());
	}
	
	void Reload() {
		LoadFile (loadedFilePath);
	}



	//Converts rotations from right handed coordinate system to left handed coordinate system (Unity is left-handed)
	public static Quaternion MayaRotationToUnity(Vector3 inVec) {

		//return Quaternion.Euler(inVec);
		Vector3 flippedRotation = new Vector3(inVec.x, -inVec.y, -inVec.z); // flip Y and Z axis for right->left handed conversion
		// convert XYZ to ZYX
		Quaternion qx = Quaternion.AngleAxis(flippedRotation.x, Vector3.right);
		Quaternion qy = Quaternion.AngleAxis(flippedRotation.y, Vector3.up);
		Quaternion qz = Quaternion.AngleAxis(flippedRotation.z, Vector3.forward);
		Quaternion qq = qz * qy * qx ; // this is the order

		return qq;
		
		//nope
		//return Quaternion.Euler (flippedRotation.z, flippedRotation.y, flippedRotation.x);
	}

	public static ClinicalMove ImportAndConvertToMove(string path) {
		BVHImporter importer = FindObjectOfType<BVHImporter> ();
		if (importer == null) {
			GameObject temp = new GameObject("Temp (BVH importer)");
			importer = temp.AddComponent<BVHImporter>();
		}
		importer.LoadFile (path);
		return importer.importedMove;
	}
}