﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Kinect = Windows.Kinect;
using ClinicalLayer;

//#define TURBODEBUG

namespace ClinicalLayer
{
    public enum TrackedJointState { Ignored, WithinBounds, OutOfBounds, WillBeTracked };

    [System.Serializable]
    public struct JointFrame
    {
        //public Quaternion jointRotWorld;
        public Quaternion jointRotLocal;

        public JointFrame(Quaternion locRot)
        {
            jointRotLocal = locRot;
        }
    }

    public class TrackedJoint : MonoBehaviour
    {

        public Kinect.JointType thisType;
        public TrackedJoint childJoint;

        [HideInInspector]
        public TrackedJoint parentJoint;
        //refresh frame data only when requested
        public JointFrame frame
        {
            get
            {
                //old
                //m_JointFrame.jointRotWorld = transform.rotation;
                m_JointFrame.jointRotLocal = transform.localRotation;
                return m_JointFrame;

                /*//crazy shit (add the rotations from bones not contained in our rig) (this may actually not be needed)
                List<Transform> ExtraBonesBetweenThisAndParent = new List<Transform>();

                //Get list of bones between this and the joint parent in transform hierarchy
                //print(thisType+""+parentJoint);
                Transform comparedTransform = transform;
                while(true) {
                    if(parentJoint == null)break;

                    bool found = false;
                    //stop adding "between bones" if the parent is found at this level
                    foreach(Transform child in comparedTransform.parent) {
                        if(child == parentJoint.transform) found = true;
                    }

                    if(!found) {
                        Debug.LogError("uuaa");
                        ExtraBonesBetweenThisAndParent.Add(comparedTransform);
                        comparedTransform = comparedTransform.parent;
                    }
                    else break;
                }

                m_JointFrame.jointRotLocal = transform.localRotation;

                print (ExtraBonesBetweenThisAndParent.Count);
                for (int i = ExtraBonesBetweenThisAndParent.Count-1; i > -1; i--) {
                    m_JointFrame.jointRotLocal *= ExtraBonesBetweenThisAndParent[i].localRotation;
                    print(thisType+"("+name+"): Added rotation from extra bone "+i+":"+ExtraBonesBetweenThisAndParent[i].name);
                }

                return m_JointFrame;*/

            }
        }
        private JointFrame m_JointFrame = new JointFrame();

        public bool ignored
        {
            get { return m_ignored; }
            set
            {
                m_ignored = value;
                if (value) state = TrackedJointState.Ignored;
                else state = TrackedJointState.WillBeTracked;
            }
        }

        private bool m_ignored;

        public Renderer[] colorableMeshes;

        public TrackedJointState state
        {
            get { return m_state; }
            set
            {
                if (m_state == value) return;
                switch (value)
                {
                    case TrackedJointState.Ignored:
                        //TODO: maybe find a better way to vizualize a color of a single joint
                        ChangeColor(Color.grey);
                        break;
                    case TrackedJointState.OutOfBounds:
                        ChangeColor(Color.red);
                        break;
                    case TrackedJointState.WithinBounds:
                        ChangeColor(Color.green);
                        break;
                    case TrackedJointState.WillBeTracked:
                        ChangeColor(Color.blue);
                        break;
                    default:
                        break;
                }
                m_state = value;
            }
        }

        private TrackedJointState m_state = TrackedJointState.WithinBounds;

        [HideInInspector]
        public Windows.Kinect.JointType setChildToType; //temporary, for joint orientation fixing


        private bool parentSet = false;
		       

        // Use this for initialization
        void Start()
        {
            ignored = true; //default to ignoring
        }

        void LateUpdate()
        {
            if (parentSet) return;
            if (childJoint)
            {
                childJoint.parentJoint = this;
                //print ("set:"+childJoint.thisType + " parent to:" + childJoint.parentJoint);
            }
            parentSet = true;

        }

        void ChangeColor(Color c)
        {
            if (colorableMeshes == null || colorableMeshes.Length == 0) return;
            foreach (Renderer r in colorableMeshes)
            {
				//if(r.name.StartsWith("IGNORECOL"))return;
                r.material.color = c;
            }
        }

        public void Clicked()
        {
            ignored = !ignored;
        }

        void OnDrawGizmos()
        {
            //return;
            if (!childJoint) Gizmos.color = Color.red;
            else Gizmos.color = Color.green;

            Gizmos.color *= 0.5f;

            Gizmos.DrawSphere(transform.position, 0.05f);

            //TODO:re-enable
            /*if(childJoint) {
                //mini position gizmo
                Vector3 pos = transform.position;
                Vector3 dir;
                float len = 0.07f;

                //x
                Gizmos.color = Color.red;
                dir = transform.right*len;
                Gizmos.DrawRay(pos,dir);

                //y
                Gizmos.color = Color.green;
                dir = transform.up*len;
                Gizmos.DrawRay(pos,dir);

                //z
                Gizmos.color = Color.blue;
                dir = transform.forward*len*10;
                Gizmos.DrawRay(pos,dir);
            }*/
        }

        /*void OnMouseUpAsButton() {
            ignored = !ignored;
        }*/

#if UNITY_EDITOR && TURBODEBUG
        void OnEnable()
        {
            UnityEditor.SceneView.onSceneGUIDelegate += OnSceneGUI;
        }

        void OnDestroy()
        {
            UnityEditor.SceneView.onSceneGUIDelegate -= OnSceneGUI;
        }

        void OnSceneGUI(UnityEditor.SceneView sceneView)
        {
            if (!Application.isPlaying || !this) return;
            try
            {
                //UnityEditor.Handles.Label (transform.position, thisType.ToString());
                UnityEditor.Handles.Label(transform.position, name);
            }
            catch (UnityException e)
            {
                //dunno
                Debug.LogWarning("Shit's broken! " + e);
            }
        }
#endif
    }

}
