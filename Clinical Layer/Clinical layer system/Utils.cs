﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Kinect = Windows.Kinect;

namespace ClinicalLayer {
	public static class Utils {

		//Converts Kinect.Vector4 to unity quaternion.
		public static Quaternion KinectVec4ToQuaternion(Windows.Kinect.Vector4 kinectVector) {
			return new  Quaternion(-kinectVector.X, -kinectVector.Y, kinectVector.Z, kinectVector.W);
		}

		public static Vector3 CamSpaceToVec(Kinect.CameraSpacePoint camPoint) {
			return new Vector3 (camPoint.X, camPoint.Y, camPoint.Z);
		}

		//Not used
		/*public static bool IsTipJoint(Kinect.JointType type) {
			if(type == Kinect.JointType.FootLeft ||
			   type == Kinect.JointType.FootRight ||
			   type == Kinect.JointType.HandTipLeft ||
			   type == Kinect.JointType.HandTipRight ||
			   type == Kinect.JointType.Head ||
			   type == Kinect.JointType.ThumbLeft ||
			   type == Kinect.JointType.ThumbRight) {
					return true;
			}
			else return false;
		}*/

		public static bool JointTypeIgnored (Kinect.JointType t)
		{
			if(//t == Kinect.JointType.HandLeft ||
			   //t == Kinect.JointType.HandRight ||
			   //t == Kinect.JointType.HandTipLeft ||
			   //t == Kinect.JointType.HandTipRight ||
			   t == Kinect.JointType.ThumbLeft ||
			   t == Kinect.JointType.ThumbRight) {
				return true; //these not implemented (yet) (or ever), so skip
			}
			else return false;
		}

		//source: http://nic-gamedev.blogspot.fi/2011/11/quaternion-math-getting-local-axis.html
		public static Vector3 GetUpVector(Quaternion inQuat) {
			return new Vector3( 2 * (inQuat.x * inQuat.y - inQuat.w * inQuat.z), 
			                   1 - 2 * (inQuat.x * inQuat.x + inQuat.z * inQuat.z),
			                   2 * (inQuat.y * inQuat.z + inQuat.w * inQuat.x));
		}

		public static Vector3 GetDownVector(Quaternion inQuat) {
			return -GetUpVector (inQuat);
		}

		public static Vector3 GetForwardVector(Quaternion inQuat) {
			return new Vector3( 2 * (inQuat.x * inQuat.z + inQuat.w * inQuat.y), 
			               2 * (inQuat.y * inQuat.x - inQuat.w * inQuat.x),
			               1 - 2 * (inQuat.x * inQuat.x + inQuat.y * inQuat.y));	
		}

		public static Vector3 GetBackwardVector(Quaternion inQuat) {
			return -GetForwardVector (inQuat);
		}

		public static Vector3 GetRightVector(Quaternion inQuat) {
			return new Vector3( 1 - 2 * (inQuat.y * inQuat.y + inQuat.z * inQuat.z),
			               2 * (inQuat.x * inQuat.y + inQuat.w * inQuat.z),
			               2 * (inQuat.x * inQuat.z - inQuat.w * inQuat.y));
		}

		public static Vector3 GetLeftVector(Quaternion inQuat) {
			return -GetRightVector (inQuat);
		}

		public static Vector3 GetJointPointingDirection(Kinect.JointType childType) {
			if(childType == Kinect.JointType.SpineMid ||
			   childType == Kinect.JointType.SpineShoulder ||
			   childType == Kinect.JointType.Neck ||
			   childType == Kinect.JointType.Head) return Vector3.up;
			
			else if(childType == Kinect.JointType.KneeLeft ||
			        childType == Kinect.JointType.AnkleLeft ||
			        childType == Kinect.JointType.KneeRight ||
			        childType == Kinect.JointType.AnkleRight) return Vector3.down;
			
			else if(childType == Kinect.JointType.HipLeft ||
			        childType == Kinect.JointType.ShoulderLeft ||
			        childType == Kinect.JointType.ElbowLeft ||
			        childType == Kinect.JointType.WristLeft) return Vector3.left;
			
			else if(childType == Kinect.JointType.HipRight ||
			        childType == Kinect.JointType.ShoulderRight ||
			        childType == Kinect.JointType.ElbowRight ||
			        childType == Kinect.JointType.WristRight) return Vector3.right;

			//satisfy compiler
			else return Vector3.forward;
		}

		public static Vector3 GetJointDirectionVector(Quaternion rot, Kinect.JointType childType) {		
			if(childType == Kinect.JointType.SpineMid ||
			   childType == Kinect.JointType.SpineShoulder ||
			   childType == Kinect.JointType.Neck ||
			   childType == Kinect.JointType.Head) return Utils.GetUpVector(rot);
			
			else if(childType == Kinect.JointType.KneeLeft ||
			        childType == Kinect.JointType.AnkleLeft ||
			        childType == Kinect.JointType.KneeRight ||
			        childType == Kinect.JointType.AnkleRight) return Utils.GetDownVector(rot);
			
			else if(childType == Kinect.JointType.HipLeft ||
			        childType == Kinect.JointType.ShoulderLeft ||
			        childType == Kinect.JointType.ElbowLeft ||
			        childType == Kinect.JointType.WristLeft) return Utils.GetLeftVector(rot);
			
			else if(childType == Kinect.JointType.HipRight ||
			        childType == Kinect.JointType.ShoulderRight ||
			        childType == Kinect.JointType.ElbowRight ||
			        childType == Kinect.JointType.WristRight) return Utils.GetRightVector(rot);
			
			//satisfy compiler
			else return Vector3.forward;
		}
		
		
		public static bool KinectPossiblyAvailable() {
			return(Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer); //TODO: make smarter
		}

		public static bool GetBit(byte b, int bitNumber) {
			return (b & (1 << bitNumber)) != 0;
		}

		public static byte[] ReadStreamFully(Stream input)
		{
			byte[] buffer = new byte[16*1024];
			using (MemoryStream ms = new MemoryStream())
			{
				int read;
				while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
				{
					ms.Write(buffer, 0, read);
				}
				return ms.ToArray();
			}
		}

		public static string defaultAssetsListPath {
			get {
				return Application.dataPath+"/Resources/DefaultAssetsList.txt";
			}
		}

		public static string defaultAssetsRootPath {
			get {
				return Application.streamingAssetsPath+"/AppFiles";
			}
		}

		public static string appFilesPath {
			get {
				/*switch(Application.platform) {
					case RuntimePlatform.Android:
					case RuntimePlatform.IPhonePlayer:
						//Assets need to be copied here first, which kind of sucks, but atleast then its read/write with the default assets in place.
						return Application.persistentDataPath;
					default:
						return Application.streamingAssetsPath+"/AppFiles";
				}*/
				if(copyFilesCondition) {
					return Application.persistentDataPath+"/AppFiles";
				}
				else return Application.streamingAssetsPath+"/AppFiles";
			}
		}

		/*public static bool GoToCopyFilesIfNeeded ()
		{
			Debug.Log ("Copy files condition: "+Utils.copyFilesCondition);
			if (Utils.copyFilesCondition && (!PlayerPrefs.HasKey ("DefaultAssetsCopied") || PlayerPrefs.GetString ("DefaultAssetsCopied") != bool.TrueString)) {
				InitialAssetsCopy.returnToLevel = Application.loadedLevelName;
				Debug.Log(InitialAssetsCopy.returnToLevel);
				Application.LoadLevel ("InitialAssetsCopying");
				return true;
			}
			else return false;
		}*/

		public static string savedMovesFolder {
			get {
				return appFilesPath+"/SavedMoves";
			}
		}

		public static bool copyFilesCondition {
			get {
				return (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer); //TODO: test iOS, not tested yet
				//return true; //DEV! TODO:Change!
			}
		}


		public static int GetIntFromBitArray(BitArray bitArray) {		
			if (bitArray.Length > 32) throw new System.ArgumentException("Argument length shall be at most 32 bits.");
			
			int[] array = new int[1];
			bitArray.CopyTo(array, 0);
			return array[0];		
		}

		public static BitArray GetBitArrayFromInt (int input) {
			return new BitArray(new int[1] {input});
		}
		
		//public static bool 

		public static Dictionary<Kinect.JointType, Kinect.JointType> boneMap = new Dictionary<Kinect.JointType, Kinect.JointType>()
		{
			{ Kinect.JointType.FootLeft, Kinect.JointType.AnkleLeft },
			{ Kinect.JointType.AnkleLeft, Kinect.JointType.KneeLeft },
			{ Kinect.JointType.KneeLeft, Kinect.JointType.HipLeft },
			{ Kinect.JointType.HipLeft, Kinect.JointType.SpineBase },
			
			{ Kinect.JointType.FootRight, Kinect.JointType.AnkleRight },
			{ Kinect.JointType.AnkleRight, Kinect.JointType.KneeRight },
			{ Kinect.JointType.KneeRight, Kinect.JointType.HipRight },
			{ Kinect.JointType.HipRight, Kinect.JointType.SpineBase },
			
			{ Kinect.JointType.HandTipLeft, Kinect.JointType.HandLeft },
			{ Kinect.JointType.ThumbLeft, Kinect.JointType.HandLeft },
			{ Kinect.JointType.HandLeft, Kinect.JointType.WristLeft },
			{ Kinect.JointType.WristLeft, Kinect.JointType.ElbowLeft },
			{ Kinect.JointType.ElbowLeft, Kinect.JointType.ShoulderLeft },
			{ Kinect.JointType.ShoulderLeft, Kinect.JointType.SpineShoulder },
			
			{ Kinect.JointType.HandTipRight, Kinect.JointType.HandRight },
			{ Kinect.JointType.ThumbRight, Kinect.JointType.HandRight },
			{ Kinect.JointType.HandRight, Kinect.JointType.WristRight },
			{ Kinect.JointType.WristRight, Kinect.JointType.ElbowRight },
			{ Kinect.JointType.ElbowRight, Kinect.JointType.ShoulderRight },
			{ Kinect.JointType.ShoulderRight, Kinect.JointType.SpineShoulder },
			
			{ Kinect.JointType.SpineBase, Kinect.JointType.SpineMid },
			{ Kinect.JointType.SpineMid, Kinect.JointType.SpineShoulder },
			{ Kinect.JointType.SpineShoulder, Kinect.JointType.Neck },
			{ Kinect.JointType.Neck, Kinect.JointType.Head },
		};

		public static string tempDataPath {
			get {
				return Application.persistentDataPath;
			}
		}

		public static string previewDataPath {
			get {
				return Path.Combine (tempDataPath, "PreviewMovieTemp");
			}
		}


		//public static string

		public static string GetFilePathPrefixForWWW() {
			string prefix = "";
	/*#if UNITY_ANDROID
			//prefix = "jar:file://"; //ummh. This may get added automatically...
	#else
			prefix = "file://";
	#endif*/
			if(Application.platform == RuntimePlatform.Android) {
				//prefix = ""; //gets added automatically when using WWW // (OR NOT?!)wtf android. Get your shit together, this isn't reliable
				//prefix = "file://";
				prefix = ""; //WHAT THE HELL IS ACTUALLY RIGHT?!
			}
			else {
				prefix = "file://";
			}

			return prefix;
		}

		public static string GetTimeStamp(float seconds) {
			// Can't use (cycle/3600) like this, or else the time will go from 00:59:59 directly to 01:60:00
			string timeStamp = /*string.Format("{0:00}", (cycle/3600)) + ":" +*/string.Format("{0:00}", (seconds/60)) + ":" + string.Format("{0:00}", (seconds%60));
			return timeStamp;
		}	

		public static string GetTimeStampSeconds (float seconds)	{
			return seconds.ToString("F2")+" seconds";
		}



		public static string ByteLenghtToHumanReadable(long byteLenght, bool useBits) {
			string suffix = "";
			long lenght;
			if (useBits) lenght = byteLenght * 8;
			else lenght = byteLenght;
			
			if (lenght < 1024) {
				if (useBits) suffix = " bits";
				else suffix = " B";
				return lenght + suffix;
			}
			else if (lenght < 1048576) {
				if (useBits) suffix = " kbits";
				else suffix = " kB";
				return (lenght / 1024) + suffix;
			}
			else {
				if (useBits) suffix = " Mbits";
				else suffix = " MB";
				return System.Math.Round(((float)lenght / (float)1048576), 2) + suffix;
			}
		}

		//Last day purkka for Remote proto, allows the hand-animated moves to have different moves for the actual tracking
		public static ClinicalMove GetTrackingMoveFor (ClinicalMove possiblyFakeMove)
		{
			string fakePairsPath = Utils.appFilesPath+"/FAKEMAPPINGSFILE.txt";
			Debug.Log("FAKE PAIRS PATH:"+fakePairsPath);
			string fakePairsFile = File.ReadAllText(fakePairsPath);
			string[] lines = fakePairsFile.Split('\n');
			
			ClinicalMove realTrackingMove = null;
			
			string nameOfFakeMove;
			string pathOfrealMove;
			for (int i = 0; i < lines.Length; i++) {
				
				string[] parts = lines[i].Split(';');
				if(parts.Length < 2)continue;
				nameOfFakeMove = parts[0];
				pathOfrealMove = parts[1];
				pathOfrealMove = pathOfrealMove.Substring(0,pathOfrealMove.Length-1);
				
				if(possiblyFakeMove.moveName == nameOfFakeMove) {
					//Found match, give the real tracking move
					Debug.Log(possiblyFakeMove.moveName+" is fake move, found real tracking move: "+pathOfrealMove+"asd");
					realTrackingMove = ClinicalMove.CreateFromJson(File.ReadAllText(Utils.appFilesPath+"/"+pathOfrealMove));
				}
			}
			if(realTrackingMove == null) {
				Debug.Log("Using actual visual move for tracking "+possiblyFakeMove.moveName);
				realTrackingMove = possiblyFakeMove;
			}
			return realTrackingMove;
		}

		public static string remoteProtoSpecialPath {
			get { return Utils.appFilesPath+"/TESTABLE MOVES/Final/Visual"; }
		}
	}
}