using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Kinect = Windows.Kinect;
using ClinicalLayer;

//Will the orientation fixing run at start? (use for all but rigs already made for kinect)
public class RigAdjusterForKinect : MonoBehaviour {

	public KinectBodyOrientationSensor refBody; //the reference body, this will be used for determining correct hierarchy and zero rotations for joints.
	private TrackedJoint[] joints; //all the TrackedJoint scripts acting as bones that this rig has.

	private List<TrackedJoint> originalsToDestroy = new List<TrackedJoint>();
	private List<GameObject> duplicates = new List<GameObject>();
	private List<TrackedJoint> duplicatesJoints = new List<TrackedJoint>();
	private Dictionary<Kinect.JointType,TrackedJoint> duplicatedJointsBySelf = new Dictionary<Kinect.JointType, TrackedJoint>();
	public bool debug = false;

	private bool tPoseZeroRots = true;

	void Start() {
		if(!GetComponent<ModelControllerFollow>())MakeParentsWithRightOrientation ();
		//StartCoroutine("MakeParentsWithRightOrientation"); //### IF DEBUGGING USE THIS INSTEAD!
	}

	public void MakeParentsWithRightOrientation() {
		if(!enabled) {
			Debug.LogError("CANCELLING JOINT HIERARCHY ADJUST FOR "+name+" AS MONOBEHAVIOUR DISABLED");
			return;
		}
		print ("Adjusting joint hierarchy for "+name);

		if(!refBody)refBody = GetComponent<ModelControllerFollow> ().refToFollow;

		joints = GetComponentsInChildren<TrackedJoint>();

		//For this part the base needs to have zero rotation, it is set back after this is done though (all inside one frame)
		Quaternion baseOriginalRot = transform.rotation;
		transform.rotation = Quaternion.identity;
		
		foreach(TrackedJoint joint in joints) {
			joint.enabled = false; //for checking if we are looking at an original joint or a created one
		}
		
		//goes through tracked joints, makes parents for all TrackedJoints so that they are aligned properly
		foreach (TrackedJoint joint in joints) {
			//if(!joint.childJoint)continue; //TODO NEXT: MAKE THIS WORK SO TIP JOINTS WORK
			
			/*if(orienatationFixDebug) {
				print("Processing:"+joint.name+" type:"+joint.thisType);
				yield return null;
			}*/
			
			//If the original joint transform has children without the TrackedJoint script, reparent their children to us but dont destroy them themselves as they may belong to the rig
			//This is done so that localRotations work as they should always be relatively to the created parents which are in right orientation
			int count = 0;
			foreach(Transform child in joint.transform) {
				if(child.GetComponent<TrackedJoint>() == null) {
					/*while(child.childCount>0) {
						child.GetChild(0).parent = joint.transform;
					}*/

					if(debug)print("GameObject "+child.name+" doesn't have TrackedJoint script, reparenting its children to us. (to "+joint.name+")");
					count++;
				}
			}
			if(count>0)joint.gameObject.name += "Added children:"+count;
			
			/*if(orienatationFixDebug) {
				print("Did reparenting");
				yield return null;
			}*/
			
			//Also if the body isn't completely connected... Check if the parent (or any parent going upwards) has the script with us set as wanted child type.
			//If not, create one at our position. (or figure out position for shoulders)
			//TODO: make the root check better!
			if(!(joint.thisType == Kinect.JointType.SpineBase)) { //"is root?" check

				//find the nearest(in hierarchy) parent with the TrackedJoint script
				Transform firstParentWithJointScript = null; //this will be assigned to, if found
				Transform testing = joint.transform.parent; //this is the currently being tested parent. Initially set to immediate parent (1 level up), then go higher if needed
				while(firstParentWithJointScript == null) {
					TrackedJoint testingJointScript = testing.GetComponent<TrackedJoint>();
					if(testingJointScript && testingJointScript.enabled)firstParentWithJointScript = testing;
					else testing = testing.parent;
					
					bool createNewParent = false;
					if(testing == null || testing == transform) createNewParent = true; //True if at top of the hierarchy, therefore parent == null.

					if(firstParentWithJointScript != null) {
						if(testingJointScript.setChildToType != joint.thisType) createNewParent = true; //First found joint script doesn't want this joint as child joint
						//if(testingJointScript.thisType != refToFollow.jointsByChild[joint.thisType].childJoint.thisType) createNewParent = true; //try this instead, try avoiding multiple same joint types at different hierarchy levels (test 1.6.2014)
					}
					
					if(createNewParent) {
						if(debug)print("creating a new parent for "+joint.thisType+" as no proper parents are available");
						//create a parent for this with the right orientation so that local rotations will work
						GameObject createdParent = new GameObject("AddedParentFor:"+joint.thisType);
						TrackedJoint createdJoint = createdParent.AddComponent<TrackedJoint>();

						//TODO: if shoulder, figure out parent position between shoulders (later also fix hips maybe?)
						if(joint.thisType == Kinect.JointType.ShoulderLeft || joint.thisType == Kinect.JointType.ShoulderRight) {
							//Figure out where spineshoulder should be
							//Cannot do by getting midway point between right and left shoulders, because other shoulder may not be available yet
							//Instead do by figuring out the middle point in relation to this joint and the root transform (this transform), and zeroing that
							Vector3 rootPos = transform.position;
							Vector3 thisJointPos = joint.transform.position;

							Vector3 middlePos = new Vector3(rootPos.x,thisJointPos.y,thisJointPos.z);

							createdParent.transform.position = middlePos;
						}
						else createdParent.transform.position = joint.transform.position;
						
						if(refBody.jointsByChild.ContainsKey(joint.thisType)) { //fix the ref skeleton later, so that this part can be removed

							if(!tPoseZeroRots)createdParent.transform.rotation = refBody.jointsByChild[joint.thisType].transform.rotation; //search by this type, so result will be parent
							else createdParent.transform.rotation = Quaternion.identity;

							createdJoint.thisType = refBody.jointsByChild[joint.thisType].thisType;
						}
						else if(debug) Debug.LogError(name+": ref missing joint child "+joint.thisType+".");
						
						//find eglible parent for this parent (needs to have this parent's type as wanted child) (or just forget doing this initialization with local rots...)
						//shit! need to make this work properly still though...
						
						//createdParent.transform.parent = joint.transform.parent; (this is old)
						
						//idk, this may help as the rot is now set otherwise when it doesn't have any child joints it knows of
						TrackedJoint basejointforthis;
						if(testingJointScript && testingJointScript.enabled) {
							basejointforthis = testingJointScript;
							if(basejointforthis.setChildToType == Kinect.JointType.SpineBase) { //spinebase so not set yet!
								basejointforthis.setChildToType = createdJoint.thisType;
								
								//HACKY, fix the parent's rotation without affecting its children
								Transform basejointTransform = basejointforthis.transform;
								Transform temproot = new GameObject("temp").transform;
								while(basejointTransform.childCount>0) {
									basejointTransform.GetChild(0).parent = temproot;
								}
								
								if(!tPoseZeroRots)basejointTransform.rotation = refBody.jointsByChild[createdJoint.thisType].transform.rotation;
								else basejointTransform.rotation = Quaternion.identity;
								
								while(temproot.childCount>0) {
									temproot.GetChild(0).parent = basejointTransform;
								}
								DestroyImmediate(temproot.gameObject);
								
								createdParent.transform.parent = basejointTransform;
								
							}
							else if(debug) Debug.LogWarning("Cannot be child for this joint, has something else set already. base:"+basejointforthis);
							createdParent.transform.parent = basejointforthis.transform;
						}
						else  {
							if(debug)Debug.LogWarning(joint.thisType+": Could not find any parent with joint script for created parent. Just using old one (may not work well)");
							createdParent.transform.parent = joint.transform.parent;
						}
						
						firstParentWithJointScript = createdParent.transform;							
						
						createdJoint.setChildToType = joint.thisType;
						duplicates.Add(createdParent);
						break;
					}

					if(debug) {
						//printout if successfully found a proper parent from hierarchy
						if(firstParentWithJointScript) {
							bool yes = (testingJointScript && testingJointScript.setChildToType == joint.thisType);
							if(yes)print(testing+" does have right type of child for "+joint+":"+yes+" :"+firstParentWithJointScript.GetComponent<TrackedJoint>().childJoint.thisType);
							else Debug.LogWarning("baad:"+firstParentWithJointScript.GetComponent<TrackedJoint>());
						}
					}
				}
				firstParentWithJointScript.GetComponent<TrackedJoint>().setChildToType = joint.thisType; //redundant, but provides greatness
				joint.transform.parent = firstParentWithJointScript;
				
				if(debug)Debug.LogWarning("Moved "+joint.thisType+" to "+firstParentWithJointScript);
			}
			
			/*if(orienatationFixDebug) {
				print("Checked own parent");
				yield return null;
			}*/
			
			//duplicate jointGameObject (verify it gets duplicated to the same position?) Add it to list.
			GameObject duplicate = Instantiate(joint.gameObject) as GameObject;
			duplicate.transform.position = joint.transform.position;
			duplicates.Add(duplicate);
			
			duplicate.GetComponent<TrackedJoint>().enabled = true; //for reference, that this is a created joint
			
			/*if(orienatationFixDebug) {
				print("Duplicated");
				yield return null;
			}*/
			
			
			//remember what type the child joint for this joint is, before the children get destroyed and the info lost
			if(joint.childJoint) {
				Kinect.JointType thisChildTypeCached = joint.childJoint.thisType;
				duplicate.GetComponent<TrackedJoint>().setChildToType = thisChildTypeCached;
				if(debug)print(joint.name+duplicate.GetComponent<TrackedJoint>().setChildToType);
			}
			
			//destroy the children from the duplicate (we don't want them)
			
			/*string temp = "";
			foreach (Transform childTransform in duplicate.transform) {
				temp+=childTransform.name+"\n";
				childTransform.name += "(Should be dead)";
				Destroy(childTransform.gameObject);
			}

			print("destroyed duplicated children:"+temp+"Now childcount should be zero:"+duplicate.transform.childCount);
			duplicate.transform.DetachChildren();
			print("Detached children, NOW atleast childcount should be zero:"+duplicate.transform.childCount);
			yield return null;*/
			
			while(duplicate.transform.childCount>0) {
				DestroyImmediate(duplicate.transform.GetChild(0).gameObject);
			}
			
			//set the parent of the duplicate to the joint's parent
			duplicate.transform.parent = joint.transform.parent;
			
			//set rotation of the joint to the rotation the ref has for the child JointType (this is the right orientation for kinect while in T-pose)
			//print(joint.childJoint.thisType);
			if(joint.childJoint) { //(cannot set if this is tip joint)
				if(!tPoseZeroRots)duplicate.transform.rotation = refBody.jointsByChild[joint.childJoint.thisType].transform.rotation; //potentially would also work with localrotations
				else duplicate.transform.rotation = Quaternion.identity;
			}
			
			//set the old joint to be the duplicate's child
			joint.transform.parent = duplicate.transform;
			
			//take the children from the old joint and reparent them to the created parent
			/*foreach(Transform child in joint.transform) {
				child.parent = duplicate.transform;
				child.name += "(reparented)";
			}*/

			//TODO: figure out if the while below is necessary at all!
			/*while(joint.transform.childCount>0) {
				joint.transform.GetChild(0).name += "(reparented)";
				joint.transform.GetChild(0).parent = duplicate.transform;
			}*/
			
			//copy the TrackedJoint component from the joint to the duplicate (may get complicated with references) (not needed!)
			
			//delete the original TrackedJoint component
			originalsToDestroy.Add(joint);
			//Destroy(joint,0.5f);
			
			duplicate.name = "PAR"+duplicate.name+" t:"+joint.thisType;
			
			//print("Processing joint: "+joint+"type:"+joint.thisType+" joint child:"+joint.childJoint.thisType+" Duplicate type:"+duplicate.GetComponent<TrackedJoint>().thisType);
			//return;
			
			//yield return null;##
		}
		
		//Now that we are getting rid of the old TrackedJoint scripts, references to them from the created (and old) parent joints will break.
		//We'll need to set the references to the created parents again.
		
		foreach(GameObject duplicate in duplicates) {
			duplicatesJoints.Add(duplicate.GetComponent<TrackedJoint>());
		}		
		
		/*foreach(TrackedJoint duplicateJoint in duplicatesJoints) {
			if(duplicateJoint.setChildToType == Kinect.JointType.SpineBase)continue;
			duplicatedJointsByChild.Add(duplicateJoint.childJoint.thisType,duplicateJoint);
		}*/
		
		foreach(TrackedJoint duplicateJoint in duplicatesJoints) {
			if(!duplicatedJointsBySelf.ContainsKey(duplicateJoint.thisType))duplicatedJointsBySelf.Add(duplicateJoint.thisType,duplicateJoint);
		}		
		
		//1.Determine what type the child of this joint is by reading the cached variable for that (before children of the duplicate got destroyed)
		//2.Now that we have the type, we have to get the new joint of this type. Using dictionary for that. We are only search joints that are children, so it's ok to search by the joint's own name.
		
		foreach (GameObject duplicate in duplicates) {
			TrackedJoint tmp = duplicate.GetComponent<TrackedJoint>();
			//tmp.childJoint = duplicateJointsByChild[originalJointsByChild[tmp.childJoint.thisType].childJoint.thisType]; //replace the reference to the original script by getting the new script by the child type
			if(tmp.setChildToType == Kinect.JointType.SpineBase)continue; //was not set because it is end joint
			if(debug)Debug.LogWarning("Finding child for "+tmp.thisType);
			//print(duplicatedJointsByChild[tmp.setChildToType]); //NULL WTF
			//tmp.childJoint = duplicatedJointsByChild[tmp.setChildToType].childJoint;
			if(!duplicatedJointsBySelf.ContainsKey(tmp.setChildToType)) {
				if(debug)print(tmp.setChildToType+" not found");
				continue;
			}
			if(tmp.setChildToType != tmp.thisType) {
				tmp.childJoint = duplicatedJointsBySelf[tmp.setChildToType];
				if(debug)print("child type:"+tmp.setChildToType+" result:"+tmp.childJoint.thisType+" name: "+tmp.childJoint.name);
			}
			else Debug.LogWarning("Avoided "+tmp.name+" obtaining itself as its child joint");
		}
		
		//move created parents to better position, if possible (added 2.6.2014)
		//only really works for spinebase (tested on autodesk biped)
		foreach (TrackedJoint duplicateJoint in duplicatesJoints) {
			if(duplicateJoint.thisType != Kinect.JointType.SpineBase)continue;
			
			if(duplicateJoint.name.Contains("AddedParentFor")) {
				//find joints with same thisType, if they aren't added parents, go to their position
				TrackedJoint jointWithBetterPosAndSameType = null;
				foreach(TrackedJoint candidateBetter in duplicatesJoints) {
					if(candidateBetter.thisType == duplicateJoint.thisType && !candidateBetter.name.Contains("AddedParentFor")) {
						if(debug)print("Found better position for created parent "+duplicateJoint.name+": position of "+candidateBetter.name);
						jointWithBetterPosAndSameType = candidateBetter;
						break;
					}
				}
				
				if(jointWithBetterPosAndSameType != null) {
					//move without moving children
					Transform temproot = new GameObject("temp").transform;
					while(duplicateJoint.transform.childCount>0) {
						duplicateJoint.transform.GetChild(0).parent = temproot;
					}
					
					duplicateJoint.transform.position = jointWithBetterPosAndSameType.transform.position;
					duplicateJoint.transform.parent = jointWithBetterPosAndSameType.transform.parent;
					
					while(temproot.childCount>0) {
						temproot.GetChild(0).parent = duplicateJoint.transform;
					}
					DestroyImmediate(temproot.gameObject);
				}
			}
		}
		
		//Destroy the original joint scripts, now that the transforms are being driven by their parents.
		while(originalsToDestroy.Count>0) {
			originalsToDestroy[0].name += "OrigJoint";
			DestroyImmediate (originalsToDestroy[0]);
			originalsToDestroy.RemoveAt(0);
		}
		
		//Get the joints again now.
		//joints = GetComponentsInChildren<TrackedJoint>();
		
		/*//idk, fix joints list?
		joints = new TrackedJoint[duplicates.Count];
		for (int i = 0; i < joints.Length; i++) {
			joints[i] = duplicates[i].GetComponent<TrackedJoint>();
		}*/
		
		transform.rotation = baseOriginalRot;

		if(debug)joints = GetComponentsInChildren<TrackedJoint>();
	}

	void Update() {
		if(!debug) return;

		foreach(TrackedJoint joint in joints) {
			if(joint.childJoint != null) {
				Debug.DrawLine(joint.transform.position,joint.childJoint.transform.position,Color.yellow,0,false);
			}
		}
	}
}