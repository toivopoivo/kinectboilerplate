var target : Transform;
var damping = 6.0;
var smooth = true;

var changeFov : boolean = false;

@script AddComponentMenu("Camera-Control/Smooth Look At")

function LateUpdate () {
	if (target) {
		if (smooth)
		{
			// Look at and dampen the rotation
			var rotation = Quaternion.LookRotation(target.position - transform.position);
			transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * damping);
			
			var fovMod = Mathf.InverseLerp(17,5,Vector3.Distance(transform.position,target.position));
			
			//print(fovMod);
			
			if(changeFov)GetComponent.<Camera>().fieldOfView = Mathf.Lerp(15,90,fovMod);
		}
		else
		{
			// Just lookat
		    transform.LookAt(target);
		}
	}
}

function Start () {
	// Make the rigid body not change rotation
   	if (GetComponent.<Rigidbody>())
		GetComponent.<Rigidbody>().freezeRotation = true;
}