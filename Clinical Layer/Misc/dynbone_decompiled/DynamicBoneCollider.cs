﻿using System;
using UnityEngine;

[AddComponentMenu("Dynamic Bone/Dynamic Bone Collider")]
public class DynamicBoneCollider : MonoBehaviour
{
    public Vector3 m_Center = Vector3.zero;
    public Direction m_Direction;
    public float m_Height;
    public float m_Radius = 0.5f;

    public void Collide(ref Vector3 particlePosition, float particleRadius)
    {
        float sphereRadius = this.m_Radius * base.transform.lossyScale.x;
        float num2 = (this.m_Height * 0.5f) - sphereRadius;
        if (num2 <= 0f)
        {
            CollideSphere(ref particlePosition, particleRadius, base.transform.TransformPoint(this.m_Center), sphereRadius);
        }
        else
        {
            Vector3 center = this.m_Center;
            Vector3 vector2 = this.m_Center;
            switch (this.m_Direction)
            {
                case Direction.X:
                    center.x -= num2;
                    vector2.x += num2;
                    break;
				case Direction.Xminus:
					center.x += num2;
					vector2.x -= num2;
					break;

                case Direction.Y:
                    center.y -= num2;
                    vector2.y += num2;
                    break;

                case Direction.Z:
                    center.z -= num2;
                    vector2.z += num2;
                    break;
            }
            CollideCapsule(ref particlePosition, particleRadius, base.transform.TransformPoint(center), base.transform.TransformPoint(vector2), sphereRadius);
        }
    }

    private static void CollideCapsule(ref Vector3 particlePosition, float particleRadius, Vector3 capsuleP0, Vector3 capsuleP1, float capsuleRadius)
    {
        float num = capsuleRadius + particleRadius;
        float num2 = num * num;
        Vector3 vector = capsuleP1 - capsuleP0;
        Vector3 vector2 = particlePosition - capsuleP0;
        float num3 = Vector3.Dot(vector2, vector);
        if (num3 <= 0f)
        {
            float num4 = vector2.sqrMagnitude;
            if ((num4 > 0f) && (num4 < num2))
            {
                float num5 = Mathf.Sqrt(num4);
                particlePosition = capsuleP0 + ((Vector3) (vector2 * (num / num5)));
            }
        }
        else
        {
            float num6 = vector.sqrMagnitude;
            if (num3 >= num6)
            {
                vector2 = particlePosition - capsuleP1;
                float num7 = vector2.sqrMagnitude;
                if ((num7 > 0f) && (num7 < num2))
                {
                    float num8 = Mathf.Sqrt(num7);
                    particlePosition = capsuleP1 + ((Vector3) (vector2 * (num / num8)));
                }
            }
            else if (num6 > 0f)
            {
                num3 /= num6;
                vector2 -= (Vector3) (vector * num3);
                float num9 = vector2.sqrMagnitude;
                if ((num9 > 0f) && (num9 < num2))
                {
                    float num10 = Mathf.Sqrt(num9);
                    particlePosition = (Vector3) (particlePosition + (vector2 * ((num - num10) / num10)));
                }
            }
        }
    }

    private static void CollideSphere(ref Vector3 particlePosition, float particleRadius, Vector3 sphereCenter, float sphereRadius)
    {
        float num = sphereRadius + particleRadius;
        float num2 = num * num;
        Vector3 vector = particlePosition - sphereCenter;
        float num3 = vector.sqrMagnitude;
        if ((num3 > 0f) && (num3 < num2))
        {
            float num4 = Mathf.Sqrt(num3);
            particlePosition = sphereCenter + ((Vector3) (vector * (num / num4)));
        }
    }

    private void OnDrawGizmosSelected()
    {
        if (base.enabled)
        {
            Gizmos.color = Color.yellow;
            float num = this.m_Radius * base.transform.lossyScale.x;
            float num2 = (this.m_Height * 0.5f) - num;
            if (num2 <= 0f)
            {
                Gizmos.DrawWireSphere(base.transform.TransformPoint(this.m_Center), num);
            }
            else
            {
                Vector3 center = this.m_Center;
                Vector3 vector2 = this.m_Center;
                switch (this.m_Direction)
                {
                    case Direction.X:
                        center.x -= num2;
                        vector2.x += num2;
                        break;
					case Direction.Xminus:
						center.x += num2;
						vector2.x -= num2;
						break;

                    case Direction.Y:
                        center.y -= num2;
                        vector2.y += num2;
                        break;

                    case Direction.Z:
                        center.z -= num2;
                        vector2.z += num2;
                        break;
                }
                Gizmos.DrawWireSphere(base.transform.TransformPoint(center), num);
                Gizmos.DrawWireSphere(base.transform.TransformPoint(vector2), num);
            }
        }
    }

    private void OnValidate()
    {
        this.m_Radius = Mathf.Max(this.m_Radius, 0f);
        this.m_Height = Mathf.Max(this.m_Height, 0f);
    }

    public enum Direction
    {
        X,
        Y,
        Z,
		Xminus
    }
}

