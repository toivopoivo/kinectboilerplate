﻿using System;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Dynamic Bone/Dynamic Bone")]
public class DynamicBone : MonoBehaviour
{
    public List<DynamicBoneCollider> m_Colliders;
    [Range(0f, 1f)]
    public float m_Damping = 0.1f;
    [Range(0f, 2f)]
    public float m_DampingDecay = 1f;
    [Range(0f, 1f)]
    public float m_Elasticity = 0.1f;
    [Range(0f, 2f)]
    public float m_ElasticityDecay = 1f;
    public float m_EndLength;
    public Vector3 m_EndOffset = Vector3.zero;
    public Vector3 m_Force = Vector3.zero;
    public Vector3 m_Gravity = Vector3.zero;
    [Range(0f, 1f)]
    public float m_Inert;
    [Range(0f, 2f)]
    public float m_InertDecay = 1f;
    private Vector3 m_LocalGravity = Vector3.zero;
    private Vector3 m_ObjectMove = Vector3.zero;
    private Vector3 m_ObjectPrevPosition = Vector3.zero;
    private float m_ObjectScale = 1f;
    private List<Particle> m_Particles = new List<Particle>();
    public float m_Radius;
    [Range(0f, 2f)]
    public float m_RadiusDecay = 1f;
    public Transform m_Root;
    [Range(0f, 1f)]
    public float m_Stiffness = 0.1f;
    [Range(0f, 2f)]
    public float m_StiffnessDecay = 1f;
    private float m_Time;
    public float m_UpdateRate = 60f;
    private float m_Weight = 1f;

    private void AppendParticles(Transform b, int parentIndex, float damping, float elasticity, float stiffness, float inert, float radius)
    {
        Particle item = new Particle {
            m_Transform = b,
            m_ParentIndex = parentIndex
        };
        if (b != null)
        {
            item.m_Position = item.m_PrevPosition = b.position;
            item.m_InitLocalPosition = b.localPosition;
            item.m_InitLocalRotation = b.localRotation;
        }
        else
        {
            Transform transform = this.m_Particles[parentIndex].m_Transform;
            if (this.m_EndLength > 0f)
            {
                Transform transform2 = transform.parent;
                if (transform2 != null)
                {
                    item.m_EndOffset = (Vector3) (transform.InverseTransformPoint(((Vector3) (transform.position * 2f)) - transform2.position) * this.m_EndLength);
                }
                else
                {
                    item.m_EndOffset = new Vector3(this.m_EndLength, 0f, 0f);
                }
            }
            else
            {
                item.m_EndOffset = transform.InverseTransformPoint(base.transform.TransformDirection(this.m_EndOffset) + transform.position);
            }
            item.m_Position = item.m_PrevPosition = transform.TransformPoint(item.m_EndOffset);
        }
        damping = Mathf.Clamp01(damping);
        elasticity = Mathf.Clamp01(elasticity);
        stiffness = Mathf.Clamp01(stiffness);
        inert = Mathf.Clamp01(inert);
        radius = Mathf.Max(radius, 0f);
        item.m_Damping = damping;
        item.m_Elasticity = elasticity;
        item.m_Stiffness = stiffness;
        item.m_Inert = inert;
        item.m_Radius = radius;
        if (parentIndex >= 0)
        {
            damping *= this.m_DampingDecay;
            elasticity *= this.m_ElasticityDecay;
            stiffness *= this.m_StiffnessDecay;
            inert *= this.m_InertDecay;
            radius *= this.m_RadiusDecay;
        }
        int count = this.m_Particles.Count;
        this.m_Particles.Add(item);
        if (b != null)
        {
            for (int i = 0; i < b.childCount; i++)
            {
                this.AppendParticles(b.GetChild(i), count, damping, elasticity, stiffness, inert, radius);
            }
            if ((b.childCount == 0) && ((this.m_EndLength > 0f) || (this.m_EndOffset != Vector3.zero)))
            {
                this.AppendParticles(null, count, damping, elasticity, stiffness, inert, radius);
            }
        }
    }

    private void ApplyParticlesToTransforms()
    {
        for (int i = 1; i < this.m_Particles.Count; i++)
        {
            Particle particle = this.m_Particles[i];
            Particle particle2 = this.m_Particles[particle.m_ParentIndex];
            if (particle2.m_Transform.childCount <= 1)
            {
                Quaternion quaternion;
                if (particle.m_Transform != null)
                {
                    quaternion = Quaternion.FromToRotation(particle2.m_Transform.TransformDirection(particle.m_Transform.localPosition), particle.m_Position - particle2.m_Position);
                }
                else
                {
                    quaternion = Quaternion.FromToRotation(particle2.m_Transform.TransformDirection(particle.m_EndOffset), particle.m_Position - particle2.m_Position);
                }
                particle2.m_Transform.rotation = quaternion * particle2.m_Transform.rotation;
            }
            if (particle.m_Transform != null)
            {
                particle.m_Transform.position = (particle.m_Position);
            }
        }
    }

    public float GetWeight()
    {
        return this.m_Weight;
    }

    private void InitTransforms()
    {
        foreach (Particle particle in this.m_Particles)
        {
            if (particle.m_Transform != null)
            {
                particle.m_Transform.localPosition = (particle.m_InitLocalPosition);
                particle.m_Transform.localRotation = (particle.m_InitLocalRotation);
            }
        }
    }

    private void LateUpdate()
    {
        if (this.m_Weight > 0f)
        {
            this.UpdateDynamicBones(Time.deltaTime);
        }
    }

    private void OnDisable()
    {
        this.InitTransforms();
    }

    private void OnDrawGizmosSelected()
    {
        if (base.enabled && (this.m_Root != null))
        {
            if ((Application.isEditor && !Application.isPlaying) && base.transform.hasChanged)
            {
                this.InitTransforms();
                this.SetupParticles();
            }
            Gizmos.color = (Color.white);
            foreach (Particle particle in this.m_Particles)
            {
                if (particle.m_ParentIndex >= 0)
                {
                    Particle particle2 = this.m_Particles[particle.m_ParentIndex];
                    Gizmos.DrawLine(particle.m_Position, particle2.m_Position);
                }
                if (particle.m_Radius > 0f)
                {
                    Gizmos.DrawWireSphere(particle.m_Position, particle.m_Radius * this.m_ObjectScale);
                }
            }
        }
    }

    private void OnEnable()
    {
        this.ResetParticlesPosition();
    }

    private void OnValidate()
    {
        this.m_UpdateRate = Mathf.Max(this.m_UpdateRate, 0f);
        this.m_Damping = Mathf.Clamp01(this.m_Damping);
        this.m_Elasticity = Mathf.Clamp01(this.m_Elasticity);
        this.m_Stiffness = Mathf.Clamp01(this.m_Stiffness);
        this.m_Inert = Mathf.Clamp01(this.m_Inert);
        this.m_Radius = Mathf.Max(this.m_Radius, 0f);
        this.m_DampingDecay = Mathf.Max(this.m_DampingDecay, 0f);
        this.m_ElasticityDecay = Mathf.Max(this.m_ElasticityDecay, 0f);
        this.m_StiffnessDecay = Mathf.Max(this.m_StiffnessDecay, 0f);
        this.m_InertDecay = Mathf.Max(this.m_InertDecay, 0f);
        this.m_RadiusDecay = Mathf.Max(this.m_RadiusDecay, 0f);
        if (Application.isEditor && Application.isPlaying)
        {
            this.InitTransforms();
            this.SetupParticles();
        }
    }

    private void ResetParticlesPosition()
    {
        foreach (Particle particle in this.m_Particles)
        {
            if (particle.m_Transform != null)
            {
                particle.m_Position = particle.m_PrevPosition = particle.m_Transform.position;
            }
            else
            {
                Transform transform = this.m_Particles[particle.m_ParentIndex].m_Transform;
                particle.m_Position = particle.m_PrevPosition = transform.TransformPoint(particle.m_EndOffset);
            }
        }
    }

    private void SetupParticles()
    {
        this.m_Particles.Clear();
        if (this.m_Root != null)
        {
            this.m_LocalGravity = this.m_Root.InverseTransformDirection(this.m_Gravity);
            this.m_ObjectScale = base.transform.lossyScale.x;
            this.m_ObjectPrevPosition = base.transform.position;
            this.m_ObjectMove = Vector3.zero;
            this.AppendParticles(this.m_Root, -1, this.m_Damping, this.m_Elasticity, this.m_Stiffness, this.m_Inert, this.m_Radius);
        }
    }

    public void SetWeight(float w)
    {
        if (this.m_Weight != w)
        {
            if (w == 0f)
            {
                this.InitTransforms();
            }
            else if (this.m_Weight == 0f)
            {
                this.ResetParticlesPosition();
            }
            this.m_Weight = w;
        }
    }

    private void SkipUpdateParticles()
    {
        foreach (Particle particle in this.m_Particles)
        {
            if (particle.m_ParentIndex >= 0)
            {
                float num;
                Vector3 vector = (Vector3) (this.m_ObjectMove * particle.m_Inert);
                particle.m_PrevPosition += vector;
                particle.m_Position += vector;
                Particle particle2 = this.m_Particles[particle.m_ParentIndex];
                if (particle.m_Transform != null)
                {
                    num = (particle2.m_Transform.position - particle.m_Transform.position).magnitude;
                }
                else
                {
                    num = particle.m_EndOffset.magnitude * this.m_ObjectScale;
                }
                float num2 = Mathf.Lerp(1f, particle.m_Stiffness, this.m_Weight);
                if (num2 > 0f)
                {
                    Vector3 vector2;
                    Matrix4x4 matrixx = particle2.m_Transform.localToWorldMatrix;
                    matrixx.SetColumn(3, particle2.m_Position);
                    if (particle.m_Transform != null)
                    {
                        vector2 = matrixx.MultiplyPoint3x4(particle.m_Transform.localPosition);
                    }
                    else
                    {
                        vector2 = matrixx.MultiplyPoint3x4(particle.m_EndOffset);
                    }
                    Vector3 vector3 = vector2 - particle.m_Position;
                    float num3 = vector3.magnitude;
                    float num4 = (num * (1f - num2)) * 2f;
                    if (num3 > num4)
                    {
                        particle.m_Position += (Vector3) (vector3 * ((num3 - num4) / num3));
                    }
                }
                Vector3 vector4 = particle2.m_Position - particle.m_Position;
                float num5 = vector4.magnitude;
                if (num5 > 0f)
                {
                    particle.m_Position += (Vector3) (vector4 * ((num5 - num) / num5));
                }
            }
            else
            {
                particle.m_PrevPosition = particle.m_Position;
                particle.m_Position = particle.m_Transform.position;
            }
        }
    }

    private void Start()
    {
        this.SetupParticles();
    }

    private void Update()
    {
        if (this.m_Weight > 0f)
        {
            this.InitTransforms();
        }
    }

    private void UpdateDynamicBones(float t)
    {
        if (this.m_Root != null)
        {
            this.m_ObjectScale = base.transform.lossyScale.x;
            this.m_ObjectMove = base.transform.position - this.m_ObjectPrevPosition;
            this.m_ObjectPrevPosition = base.transform.position;
            int num = 1;
            if (this.m_UpdateRate > 0f)
            {
                float num2 = 1f / this.m_UpdateRate;
                this.m_Time += t;
                num = 0;
                while (this.m_Time >= num2)
                {
                    this.m_Time -= num2;
                    if (++num >= 3)
                    {
                        this.m_Time = 0f;
                        break;
                    }
                }
            }
            if (num > 0)
            {
                for (int i = 0; i < num; i++)
                {
                    this.UpdateParticles1();
                    this.UpdateParticles2();
                    this.m_ObjectMove = Vector3.zero;
                }
            }
            else
            {
                this.SkipUpdateParticles();
            }
            this.ApplyParticlesToTransforms();
        }
    }

    private void UpdateParticles1()
    {
        Vector3 gravity = this.m_Gravity;
        Vector3 vector2 = this.m_Gravity.normalized;
        Vector3 vector3 = this.m_Root.TransformDirection(this.m_LocalGravity);
        Vector3 vector4 = (Vector3) (vector2 * Mathf.Max(Vector3.Dot(vector3, vector2), 0f));
        gravity -= vector4;
        gravity = (Vector3) ((gravity + this.m_Force) * this.m_ObjectScale);
        foreach (Particle particle in this.m_Particles)
        {
            if (particle.m_ParentIndex >= 0)
            {
                Vector3 vector5 = particle.m_Position - particle.m_PrevPosition;
                Vector3 vector6 = (Vector3) (this.m_ObjectMove * particle.m_Inert);
                particle.m_PrevPosition = particle.m_Position + vector6;
                particle.m_Position += (((Vector3) (vector5 * (1f - particle.m_Damping))) + gravity) + vector6;
            }
            else
            {
                particle.m_PrevPosition = particle.m_Position;
                particle.m_Position = particle.m_Transform.position;
            }
        }
    }

    private void UpdateParticles2()
    {
        for (int i = 1; i < this.m_Particles.Count; i++)
        {
            float num2;
            Particle particle = this.m_Particles[i];
            Particle particle2 = this.m_Particles[particle.m_ParentIndex];
            if (particle.m_Transform != null)
            {
                num2 = (particle2.m_Transform.position - particle.m_Transform.position).magnitude;
            }
            else
            {
                num2 = particle.m_EndOffset.magnitude * this.m_ObjectScale;
            }
            float num3 = Mathf.Lerp(1f, particle.m_Stiffness, this.m_Weight);
            if ((num3 > 0f) || (particle.m_Elasticity > 0f))
            {
                Vector3 vector;
                Matrix4x4 matrixx = particle2.m_Transform.localToWorldMatrix;
                matrixx.SetColumn(3, particle2.m_Position);
                if (particle.m_Transform != null)
                {
                    vector = matrixx.MultiplyPoint3x4(particle.m_Transform.localPosition);
                }
                else
                {
                    vector = matrixx.MultiplyPoint3x4(particle.m_EndOffset);
                }
                Vector3 vector2 = vector - particle.m_Position;
                particle.m_Position += (Vector3) (vector2 * particle.m_Elasticity);
                if (num3 > 0f)
                {
                    vector2 = vector - particle.m_Position;
                    float num4 = vector2.magnitude;
                    float num5 = (num2 * (1f - num3)) * 2f;
                    if (num4 > num5)
                    {
                        particle.m_Position += (Vector3) (vector2 * ((num4 - num5) / num4));
                    }
                }
            }
            float particleRadius = particle.m_Radius * this.m_ObjectScale;
            foreach (DynamicBoneCollider collider in this.m_Colliders)
            {
                if ((collider != null) && collider.enabled)
                {
                    collider.Collide(ref particle.m_Position, particleRadius);
                }
            }
            Vector3 vector3 = particle2.m_Position - particle.m_Position;
            float num7 = vector3.magnitude;
            if (num7 > 0f)
            {
                particle.m_Position += (Vector3) (vector3 * ((num7 - num2) / num7));
            }
        }
    }

    private class Particle
    {
        public float m_Damping;
        public float m_Elasticity;
        public Vector3 m_EndOffset = Vector3.zero;
        public float m_Inert;
        public Vector3 m_InitLocalPosition = Vector3.zero;
        public Quaternion m_InitLocalRotation = Quaternion.identity;
        public int m_ParentIndex = -1;
        public Vector3 m_Position = Vector3.zero;
        public Vector3 m_PrevPosition = Vector3.zero;
        public float m_Radius;
        public float m_Stiffness;
        public Transform m_Transform;
    }
}

