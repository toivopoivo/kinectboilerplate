﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

//Keeps an up-to date list of files and folders under the specified folder, so arbitrary folder structure with files can be copied from the streamingassets folder on android.
//InitialAssetsCopy uses this to copy streamingassets contents to persistent assets path. This is done so that there can be "default" files in the user-writable folder.
//The main function is run automatically on editor assets refresh.
using ClinicalLayer;


public class UpdateDefaultAssetsList : AssetPostprocessor {

	static List<string> foldersList;
	static List<string> filesList;

	static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths) {
		return;

		Debug.Log("Updating default (to-copy) assets list.");

		StreamWriter sw;
		if(!File.Exists(Utils.defaultAssetsListPath)) {
			File.Create(Utils.defaultAssetsListPath);
		}
		sw = new StreamWriter(Utils.defaultAssetsListPath);

		foldersList = new List<string>();
		filesList = new List<string>();

		AddToEntriesRecursive(Utils.defaultAssetsRootPath);

		//Write the amount of folders to the beginning
		sw.WriteLine(foldersList.Count.ToString("D8")+" folders"); //leading zeroes
		//Shorten the paths to only contain the part after the root, then write to the file.
		int rootPathLenght = Utils.defaultAssetsRootPath.Length;
		for (int i = 0; i < foldersList.Count; i++) {
			//if(foldersList[i] == Utils.defaultAssetsRootPath+"/" || foldersList[i] == Utils.defaultAssetsRootPath)continue;
			//Debug.Log(foldersList[i]);
			foldersList[i] = foldersList[i].Substring(rootPathLenght+1);
			foldersList[i] = foldersList[i].Replace("\\","/"); // \\ is escape for a single \
			if(i != 0)sw.WriteLine();
			sw.Write(foldersList[i]);
		}
		
		for (int i = 0; i < filesList.Count; i++) {
			filesList[i] = filesList[i].Substring(rootPathLenght+1);
			filesList[i] = filesList[i].Replace("\\","/");;
			sw.WriteLine();
			sw.Write(filesList[i]);
		}

		sw.Close();
	}

	static void AddToEntriesRecursive(string thisFolder) {
		string[] filesHere = Directory.GetFiles(thisFolder);
		string[] foldersHere = Directory.GetDirectories(thisFolder);

		foreach(string filePath in filesHere) {
			if(filePath.Contains(".meta"))continue;
			filesList.Add(filePath);
		}

		foreach(string folderPath in foldersHere) {
			foldersList.Add(folderPath);
			AddToEntriesRecursive(folderPath);
		}
	}
}
