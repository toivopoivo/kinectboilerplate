﻿using UnityEngine;
using System.Collections;
using ClinicalLayer;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Text;

public class PlayerProgressMoverExample : MonoBehaviour {
	public bool showDebugData;
	public MoveRepeatTracker tracker;
	public float speedMod = 0.2f;

	public List<string> frameprogs = new List<string>();
	public List<string> normalizedprogs = new List<string>();

	public Text frameproglog;
	public Text normalizedproglog;
	
	// Update is called once per frame
	void Update () {
		if(!tracker.trackingActive)return; //Don't do anything if the tracking is not yet activated (activate with StartTracking(ClinicalMove move))

		//The main functionality of this script: Move the cube depending on how much we progressed with the move we're doing. Could be smoothed
//		transform.Translate(0,0,tracker.frameProgressDelta * speedMod);	
        //if (tracker.frameProgressDelta > 1) {
        //    Player.instance.Movement ();
        //}
		//Stuff in the debug region only prints the progress information to the screen, and is optional
		#region debug
		if(showDebugData){
			frameprogs.Add(tracker.frameProgressDelta.ToString());
			if(frameprogs.Count > 50)frameprogs.RemoveAt(0);

			StringBuilder sb = new StringBuilder();
			foreach (var item in frameprogs) {
				sb.AppendLine(item);
			}
			frameproglog.text = sb.ToString();

			normalizedprogs.Add(tracker.normalizedProgressDelta.ToString());
			if(normalizedprogs.Count > 50)normalizedprogs.RemoveAt(0);

			sb = new StringBuilder();
			foreach (var item in normalizedprogs) {
				sb.AppendLine(item);
			}
			normalizedproglog.text = sb.ToString();

			if(Time.time > 1f) {
				frameproglog.text = frameproglog.text.Remove(0,frameproglog.text.Split('\n')[0].Length);
				normalizedproglog.text = normalizedproglog.text.Remove(0,normalizedproglog.text.Split('\n')[0].Length);
			}
		}
		#endregion

	}
}
