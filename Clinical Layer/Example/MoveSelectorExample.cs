﻿using UnityEngine;
using System.Collections;
using ClinicalLayer;
using System.IO;

public class MoveSelectorExample : MonoBehaviour {

	public MoveRepeatTracker tracker;

	private bool picking = false;

	// Use this for initialization
	void Start () {
		StartCoroutine(MoveSelecting());
	}

	void Update() {
		//!picking
		if(Input.GetKeyDown(KeyCode.Escape))StartCoroutine(MoveSelecting());
	}
	
	IEnumerator MoveSelecting() {
		picking = true;
		yield return StartCoroutine(FileBrowserManager.instance.PromptForFile(".clinicalMove",false));

		if(FileBrowserManager.instance.lastCancelled) {
			Application.LoadLevel(Application.loadedLevel);
			yield break;
		}
		else {
			picking = false;
			tracker.StartTrackingMove(ClinicalMove.CreateFromJson(File.ReadAllText(FileBrowserManager.instance.lastSelection.FullName)));
		}
	}
}
