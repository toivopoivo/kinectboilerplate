﻿using UnityEngine;
using System.Collections;

public class SimplePropSpawner : MonoBehaviour {

	public GameObject prop;

	private Transform parent;

	public int spawnCount;

	public float paddingBetween;

	public float minX;
	public float maxX;

	public float scaleMin;
	public float scaleMax;

	public float middleWidth;

	[ContextMenu("Spawn props")]
	void SpawnProps() {

		parent = new GameObject("spheresparent").transform;
		parent.parent = transform;

		for (int i = 0; i < spawnCount; i++) {
				
			GameObject prefab = prop;

			GameObject spawned = Instantiate(prefab) as GameObject;
			spawned.transform.parent = parent;

			Vector3 placePos;
			placePos.y = 0f;
			placePos.z = (float)i * paddingBetween;

			do {
				placePos.x = Random.Range(minX,maxX);
			}
			while (Mathf.Abs(placePos.x) < middleWidth);

			spawned.transform.localScale = Vector3.one * Random.Range(scaleMin,scaleMax);

			spawned.transform.localPosition = placePos;
		}
	}
}
