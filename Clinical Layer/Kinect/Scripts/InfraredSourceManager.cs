﻿using UnityEngine;
using System.Collections;
using Windows.Kinect;

public class InfraredSourceManager : MonoBehaviour 
{
	public bool longExp = false;

    private KinectSensor _Sensor;
    private InfraredFrameReader _Reader;
	private LongExposureInfraredFrameReader _LReader;
    private ushort[] _Data;
    private byte[] _RawData;
    
    // I'm not sure this makes sense for the Kinect APIs
    // Instead, this logic should be in the VIEW
    private Texture2D _Texture;

    public Texture2D GetInfraredTexture()
    {
        return _Texture;
    }
    
    void Start()
    {
        _Sensor = KinectSensor.GetDefault();
        if (_Sensor != null) 
        {
            if(!longExp)_Reader = _Sensor.InfraredFrameSource.OpenReader();
			else _LReader = _Sensor.LongExposureInfraredFrameSource.OpenReader();

            var frameDesc = _Sensor.InfraredFrameSource.FrameDescription;
			//var frameDescL = _Sensor.LongExposureInfraredFrameSource.FrameDescription;

            _Data = new ushort[frameDesc.LengthInPixels];
            _RawData = new byte[frameDesc.LengthInPixels * 4];
            _Texture = new Texture2D(frameDesc.Width, frameDesc.Height, TextureFormat.BGRA32, false);
            
            if (!_Sensor.IsOpen)
            {
                _Sensor.Open();
            }
        }
    }

    void Update () 
    {

        if ((!longExp && _Reader != null) || (longExp && _LReader != null)) 
        {
			InfraredFrame frame = null;
			LongExposureInfraredFrame frameL = null;

			if(!longExp) {
				frame = _Reader.AcquireLatestFrame();
				if (frame != null)frame.CopyFrameDataToArray(_Data);
			}
			else {
				frameL = _LReader.AcquireLatestFrame();
				//print(frameL);
				if (frameL != null)frameL.CopyFrameDataToArray(_Data);
				else print("apua");
			}

			if((!longExp && frame != null) || (longExp && frameL != null)) {
                
                int index = 0;
                foreach(var ir in _Data)
                {
                    byte intensity = (byte)(ir >> 8);
                    _RawData[index++] = intensity;
                    _RawData[index++] = intensity;
                    _RawData[index++] = intensity;
                    _RawData[index++] = 255; // Alpha
                }
                
                _Texture.LoadRawTextureData(_RawData);
                _Texture.Apply();

				//print(_Data.Length);
                
				if(!longExp) {
                	frame.Dispose();
                	frame = null;
				}
				else {
					frameL.Dispose();
					frameL = null;
				}
            }
        }
    }
    
    void OnApplicationQuit()
    {
        if (_Reader != null) 
        {
            _Reader.Dispose();
            _Reader = null;
        }
        
        if (_Sensor != null) 
        {
            if (_Sensor.IsOpen)
            {
                _Sensor.Close();
            }
            
            _Sensor = null;
        }
    }
}
