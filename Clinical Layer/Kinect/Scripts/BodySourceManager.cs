using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Windows.Kinect;
using Microsoft.Kinect.Face;
using ClinicalLayer;

public class BodySourceManager : MonoBehaviour 
{
	public static BodySourceManager instance;
		
	private KinectSensor kinectSensor;
	private BodyFrameReader bodyFrameReader;
	private Body[] bodiesArray = null;
	
	private List<KinectBodyOrientationSensor> subscribers = new List<KinectBodyOrientationSensor>();
		
	//public Transform rotThis;
	public Windows.Kinect.Vector4 floorPlane;

	[HideInInspector]
	public Quaternion floorAdjustmentQuat;

	private FaceFrameSource faceFrameSource;
	private FaceFrameReader faceFrameReader;	
	private FaceFrame faceFrame = null;
	public Windows.Kinect.Vector4 headRot;
	private ulong focusTrackingID = 0;

	private bool naamaOffDebug = true;
	
	void Awake() {
		if(!instance)instance = this;
		else Debug.LogWarning("More than one BodySourceManager! Something's off...");

		if(naamaOffDebug)UnityEngine.Debug.LogWarning("NAAAMA");
	}
	
	public Body[] GetData() {
		return bodiesArray;
	}
	
	//Called from subscribers
	public void Subscribe(KinectBodyOrientationSensor sensor) {
		subscribers.Add (sensor);
	}
	
	public void Unsubscribe(KinectBodyOrientationSensor sensor) {
		subscribers.Remove (sensor);
	}	
	
	void Start () {
		if(!Utils.KinectPossiblyAvailable()) {
			this.enabled = false;
			return;
		}
		kinectSensor = KinectSensor.GetDefault();
		
		if (kinectSensor != null)
		{
			bodyFrameReader = kinectSensor.BodyFrameSource.OpenReader();            
			if (!kinectSensor.IsOpen) kinectSensor.Open();
		}

		/*FaceFrameFeatures _faceFrameFeatures =
			FaceFrameFeatures.BoundingBoxInColorSpace |
				FaceFrameFeatures.PointsInColorSpace | 
				FaceFrameFeatures.MouthOpen |
				FaceFrameFeatures.LookingAway |
				FaceFrameFeatures.Happy |
				FaceFrameFeatures.FaceEngagement | 
				FaceFrameFeatures.Glasses |
				FaceFrameFeatures.LeftEyeClosed | 
				FaceFrameFeatures.MouthMoved | 
				FaceFrameFeatures.RightEyeClosed |
				FaceFrameFeatures.RotationOrientation;
		*/

		FaceFrameFeatures _faceFrameFeatures =
			FaceFrameFeatures.BoundingBoxInColorSpace
				| FaceFrameFeatures.PointsInColorSpace
				| FaceFrameFeatures.RotationOrientation
				| FaceFrameFeatures.FaceEngagement
				| FaceFrameFeatures.Glasses
				| FaceFrameFeatures.Happy
				| FaceFrameFeatures.LeftEyeClosed
				| FaceFrameFeatures.RightEyeClosed
				| FaceFrameFeatures.LookingAway
				| FaceFrameFeatures.MouthMoved
				| FaceFrameFeatures.MouthOpen;
		
		
		//FaceFrameSource faceFrameSource = new FaceFrameSource (kinectSensor, bodiesArray [0].TrackingId, _faceFrameFeatures);

		if (!naamaOffDebug) {
			faceFrameSource = FaceFrameSource.Create (kinectSensor, 0, _faceFrameFeatures);
		
			faceFrameReader = faceFrameSource.OpenReader ();
		}
	}
	
	void DistributeNewFrame(BodyFrame frame) {
		//print ("bodyframe");

		frame.GetAndRefreshBodyData(bodiesArray);

		//get tracking ID for the first found active body
		for (int i = 0; i < bodiesArray.Length; i++) {
			if(bodiesArray[i].IsTracked) {
				focusTrackingID = bodiesArray[i].TrackingId;
				if(!naamaOffDebug)faceFrameSource.TrackingId = focusTrackingID;
				if(!naamaOffDebug)if(faceFrameSource.IsTrackingIdValid == false)Debug.LogWarning("Should not spam");
				break;
			}
		}

		//print (Utils.KinectVec4ToQuaternion (headRot));

		//the stuff below doesn't work! Can't assign to the bodies joint orientations
		/*JointOrientation headOrientation = bodiesArray [purkkaActiveBody].JointOrientations [JointType.Head];
		headOrientation.Orientation = headRot;
		bodiesArray [purkkaActiveBody].JointOrientations [JointType.Head] = headOrientation;*/

		//print (Utils.KinectVec4ToQuaternion(bodiesArray [purkkaActiveBody].JointOrientations [JointType.Head].Orientation));
		
		//Debug.Log ("Body count:"+frame.BodyCount);

		floorPlane = frame.FloorClipPlane;
		//if(rotThis)rotThis.rotation = new Quaternion (-floorPlane.X, -floorPlane.Y, floorPlane.Z, floorPlane.W); //lol no, not a quaternion
		Vector3 up = new Vector3 (floorPlane.X, floorPlane.Y, -floorPlane.Z);
		Vector3 forward = new Vector3 (0, floorPlane.Z, floorPlane.Y);
		Vector3 side = new Vector3 (floorPlane.Y, -floorPlane.X, 0);
		//print (up);
		Debug.DrawRay (Vector3.zero, up * 5f,UnityEngine.Color.green,0.2f);
		Debug.DrawRay (Vector3.zero, forward * 5f,UnityEngine.Color.blue,0.2f);
		Debug.DrawRay (Vector3.zero, side * 5f,UnityEngine.Color.red,0.2f);

		floorAdjustmentQuat = Quaternion.LookRotation (forward, up);

		/*if(rotThis) {
			//rotThis.rotation = Quaternion.Inverse(Quaternion.LookRotation(forward,up));
			//rotThis.localPosition = new Vector3(0,floorPlane.W,0);
		}*/

		//debug
		//GameObject.Find ("recording info text").GetComponent<UnityEngine.UI.Text> ().text = "Kinect altitude: " + frame.FloorClipPlane.W;

		//GameObject.Find("Cubebe").transform.position = new Vector3(0,floorPlane.W,0);
		
		foreach(KinectBodyOrientationSensor subscriber in subscribers) {
			subscriber.NewBodyFrameArrived();
		}
	}
	
	void Update ()	{
		if(faceFrameReader != null) {
			faceFrame = faceFrameReader.AcquireLatestFrame ();

			if(faceFrame != null) {
				FaceFrameResult faceResult = faceFrame.FaceFrameResult;
				if(faceResult != null)headRot = faceResult.FaceRotationQuaternion;
				//Debug.Log("not null face");

				// extract each face property information and store it in faceText
				/*string faceText = "";
				if (faceResult.FaceProperties != null) //19.11.2014: even the check gives null reference. Find out why
				{
					foreach (var item in faceResult.FaceProperties)
					{
						faceText += item.Key.ToString() + " : ";
						
						// consider a "maybe" as a "no" to restrict 
						// the detection result refresh rate
						if (item.Value == DetectionResult.Maybe)
						{
							faceText += DetectionResult.No + "\n";
						}
						else
						{
							faceText += item.Value.ToString() + "\n";
						}                    
					}
				}
				Debug.LogWarning(faceText);*/		
				
			}
			//else Debug.Log("null face");
		}
		
		if (bodyFrameReader != null)
		{
			var frame = bodyFrameReader.AcquireLatestFrame();
			if (frame != null)	{
				if(bodiesArray == null) {
					bodiesArray = new Body[kinectSensor.BodyFrameSource.BodyCount];
				}
				DistributeNewFrame(frame);
				frame.Dispose ();
				frame = null;
				//print("frame! "+Time.time);
				//Debug.LogError("frame not null");
			}
			//else print("frame null");
		}
		//else print("reader null"); //DEBUG
	}
	
	
	void OnApplicationQuit()
	{
		print("Quitting, disposing kinect sensor");
		if (bodyFrameReader != null)
		{
			bodyFrameReader.Dispose();
			bodyFrameReader = null;
		}
		
		if (kinectSensor != null)
		{
			if (kinectSensor.IsOpen)
			{
				kinectSensor.Close();
			}
			
			kinectSensor = null;
		}
	}
}
