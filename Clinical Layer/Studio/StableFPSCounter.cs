﻿using UnityEngine;
using System.Collections;

public class StableFPSCounter : MonoBehaviour {
	
	public bool on;
	
	private int qty = 0;
	private float currentAvgFPS = 0;
	private float currFps = 0;
	
	private float worstFps = 0;
	private float worstTimer = 0;
	private float shownWorst = 0;
	
	string badText = "";
	int badCount = 0;
	int badTresholdThisTime;
	
	void Start() {
		badTresholdThisTime = Random.Range(12, 100);
	}
	
	void Update () {
		currFps = (1/Time.deltaTime) * Time.timeScale;
		currentAvgFPS = UpdateCumulativeMovingAverageFPS(currFps);
		
		if(currFps < worstFps) {
			worstFps = currFps;
		}
		worstTimer += Time.deltaTime;
		if(worstTimer > 0) {
			worstTimer -= 1f;
			shownWorst = worstFps;
			worstFps = 10000;
			
			/*if (shownWorst < 40) {
                badCount++;
                if (badCount > 10 && badCount < 30) badText += "\nLol :DD\nköyhä";
                if (badCount == badTresholdThisTime) {
#if UNITY_EDITOR
                UnityEditor.EditorUtility.DisplayDialogComplex(":DDD", "Lol köyhä :DDDD\n:DDDDDDDDDD", "Olen", "Olen", "Olen");
#endif
                }
            }*/
		}
		
		if(Input.touchCount > 3 && Input.touches[3].phase == TouchPhase.Began)on = !on;
	}
	
	float UpdateCumulativeMovingAverageFPS(float newFPS)
	{
		qty++;
		qty = Mathf.Clamp(qty,0,15);
		currentAvgFPS += (newFPS - currentAvgFPS)/qty;
		
		return currentAvgFPS;
	}
	
	void OnGUI () {
		if(!on) return;
		GUI.Label(new Rect(Screen.width-50,0f,70,700),Mathf.Round(currFps)+" fps\n"+Mathf.Round(currentAvgFPS)+" avg\n"+Mathf.Round(shownWorst)+" worst"+badText);
	}
}
