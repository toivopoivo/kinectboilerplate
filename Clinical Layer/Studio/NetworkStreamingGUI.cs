﻿//using UnityEngine;
//using System.Collections;
//using ClinicalLayer;

//public class NetworkStreamingGUI : MonoBehaviour {

//	public bool visible;
	
//	public bool autoStartServer = false;
//	public bool autoConnect = false;

//	private int port = 50005;
//	public string LANIP = "192.168.100.27";
//	//public string InternetIP = "91.152.135.27";
//	public string InternetIP = "193.166.139.112";

//	void Start() {
//		if(autoStartServer && Utils.KinectPossiblyAvailable())StartServer();
//		else if(autoConnect)Connect(InternetIP);
//	}

//	void OnGUI() {
//		int w = Screen.width;
//		int h = Screen.height;

//		visible = GUI.Toggle (new Rect (5, h - 30, 200, 30), visible, "Show networking interface");

//		if(!visible)return;

//		float buttonHeight = h * 0.1f;
//		float buttonWidth = w * 0.2f;

//		Rect buttonRect = new Rect (w * 0.02f, h - buttonHeight - h*0.02f, buttonWidth, h*0.07f);
//		Rect fieldRect  = new Rect (w * 0.02f, buttonRect.y-20-2, buttonWidth, 20);

//		//Client
//		if(Network.peerType != NetworkPeerType.Client) {
//			LANIP = GUI.TextField(fieldRect,LANIP);
//			if(GUI.Button(buttonRect,"Connect\n(LAN)"))Connect(LANIP);
//			buttonRect.x += buttonWidth*1.1f;
//			fieldRect.x += buttonWidth*1.1f;
//			InternetIP = GUI.TextField(fieldRect,InternetIP);
//	   		if(GUI.Button(buttonRect,"Connect\n(Internet)"))Connect(InternetIP);
//		}
//		else {
//			buttonRect.x-=buttonWidth*1.1f;
//			//buttonRect.width*=2.2f;
//			if(GUI.Button(buttonRect,"Connected. Click to disconnect.")) {
//				Disconnect();
//			}
//		}

//		//Server
//		//Rect rect = new Rect (Screen.width * 0.5f - Screen.width * 0.1f, Screen.height - Screen.height * 0.1f, Screen.width * 0.2f, Screen.height * 0.07f);

//		buttonRect.x += buttonWidth*1.3f;
//		fieldRect.x += buttonWidth*1.3f;

//		string buttonText = "Start server";
//		if(Network.peerType == NetworkPeerType.Server)buttonText = "Stop server";
//		if(GUI.Button(buttonRect,buttonText)) {
//			if(Network.peerType == NetworkPeerType.Server) {
//				StopServer();
//			}
//			else {
//				StartServer();
//			}
//		}

//		//network info
//		fieldRect.x = w - 200;
//		fieldRect.height = 100f;
//		GUI.Label(fieldRect,Network.peerType.ToString()+" IP:"+Network.player.ipAddress+"\nExternal IP:"+Network.player.externalIP);
//		fieldRect.y += 20;
//		if(Network.connections.Length>0)GUI.Label(fieldRect,"\nPing:"+Network.GetAveragePing(Network.connections[0]));
//	}

//	public void StartServer() {
//		//print ("Trying to start server: 1");
//		Network.InitializeServer(10,port,false);
//		//print ("Trying to start server: 2");
//	}
	
//	public void StopServer() {
//		Network.Disconnect();
//	}

	
//	public NetworkConnectionError Connect(string IP) {
//		print("KINECT AVAIL:"+Utils.KinectPossiblyAvailable());
//		NetworkConnectionError err = Network.Connect(IP,port);
//        return err;
//	}
	
//	public void Disconnect() {
//		Network.Disconnect ();
//	}

//	void OnApplicationQuit() {
//		Network.Disconnect ();
//	}
//}
