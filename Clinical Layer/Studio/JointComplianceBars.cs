﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class JointComplianceBars : MonoBehaviour {

	public List<Transform> bars = new List<Transform>();
	public GameObject barPrefab;

	void Start() {
		for (int i = 0; i < 25; i++) {
			GameObject notbar = Instantiate(barPrefab) as GameObject;
			notbar.transform.SetParent(transform,false);
			notbar.GetComponentInChildren<Text>().text = ((Windows.Kinect.JointType)i).ToString();

			bars.Add(notbar.transform.Find("BAR"));
		}
	}

	public void DisplayJointCompliance(float[] jointComplyValuesPerJointType) {
		for (int i = 0; i < 25; i++) {
			if(jointComplyValuesPerJointType[i] < -500f)jointComplyValuesPerJointType[i] = 0f;
			bars[i].localScale = new Vector3(jointComplyValuesPerJointType[i],1,1);
		}
	}
}
