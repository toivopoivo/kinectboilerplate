﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BVHDebugSkeletonAnimator : MonoBehaviour {

	public BVHDebugSkeletonBuilder skeleton;
	public int currentFrameNum = 0;

	public bool playing = false;

	public bool zeroOnly = false;

	// Update is called once per frame
	void Update () {
		if(!playing)return;
		if(skeleton.currentBVH == null)return;
		currentFrameNum++;
		if(currentFrameNum == skeleton.currentBVH.frameCount)currentFrameNum = 0;
		SetFrame (currentFrameNum);
	}

	public void SetFrame(int f) {
        //debug
        //if (!skeleton.createdRoot.gameObject.GetComponent<BVHToAnimClipTest>()) skeleton.createdRoot.gameObject.AddComponent<BVHToAnimClipTest>();

		currentFrameNum = f;

		Dictionary<string, Transform> nameToTransform = skeleton.bvhJointNameToSkeletonTransform;
		BVHAnimation bvh = skeleton.currentBVH;

		Vector3 pos = bvh.importJoints [0].GetPos (currentFrameNum) * BVHImporter.instance.scaleModifier/* 0.02f*/;
		pos.x = -pos.x;
		skeleton.createdRoot.localPosition = pos;

		//foreach(string jointName in nameToTransform.Keys) {
		foreach(ImportJoint bvhJoint in bvh.importJoints) {
			if(bvhJoint.isEndSite)continue;
			Transform trans = nameToTransform[bvhJoint.sourceName];

			//Vector3 perse = bvhJoint.GetRotQuat(currentFrameNum).eulerAngles;
			//trans.localRotation = BVHImporter.MayaRotationToUnity(perse);

			/*Quaternion aaaaaaaa = bvhJoint.GetRotQuat(currentFrameNum);
			Quaternion properRot = new Quaternion(-aaaaaaaa.x, -aaaaaaaa.y, aaaaaaaa.z, aaaaaaaa.w);
			trans.localRotation = Quaternion.Inverse(properRot);*/
			trans.localRotation = bvhJoint.GetRotQuat(currentFrameNum);
			if(zeroOnly) {
				trans.localRotation = Quaternion.identity;
				Debug.LogWarning("Showing zero rots for BVH reference");
			}
		}
	}
}
