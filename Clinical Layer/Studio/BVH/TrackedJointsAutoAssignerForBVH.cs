﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Kinect = Windows.Kinect;
using ClinicalLayer;

public class TrackedJointsAutoAssignerForBVH : MonoBehaviour {

	public BVHDebugSkeletonBuilder skeleton;
	public BVHImporter bvhImporter;

	[HideInInspector]
	public MoveRepeater repeater;

	public void AssignJoints () {
		Dictionary<string, Transform> nameToTransform = skeleton.bvhJointNameToSkeletonTransform;
		Dictionary<string,Kinect.JointType> rigBonesToJointTypeMapping = bvhImporter.rigBonesToJointTypeMapping;
		BVHAnimation bvh = skeleton.currentBVH;

		for (int i = 0; i < bvh.importJoints.Count; i++) {
			ImportJoint bvhJoint = bvh.importJoints[i];

			if(bvhJoint.isEndSite)continue;

			Kinect.JointType jointType;
			if((i != 0) && !bvhImporter.rigBonesToJointTypeMapping.ContainsKey(bvhJoint.sourceName)) {
				//Not included
				continue;
			}
			//if(i == 0)continue;
			if(false /*i == 0*/)jointType = Kinect.JointType.SpineBase;
			else jointType = bvhImporter.rigBonesToJointTypeMapping[bvhJoint.sourceName];
			GameObject go = nameToTransform[bvhJoint.sourceName].gameObject;
			TrackedJoint addedJointScript = go.AddComponent<TrackedJoint>();
			addedJointScript.thisType = jointType;
		}
		//return;

		ModelControllerFollow modelController = skeleton.createdRoot.gameObject.AddComponent<ModelControllerFollow> ();
		modelController.moveRoot = true;
		RigAdjusterForKinect adjuster = skeleton.createdRoot.gameObject.AddComponent<RigAdjusterForKinect> ();
		//adjuster.debug = true;
		//adjuster.MakeParentsWithRightOrientation ();
		repeater = skeleton.createdRoot.gameObject.AddComponent<MoveRepeater> ();
		modelController.overrideSource = repeater;
		print ("MOVENAME"+bvhImporter.lastImported.moveName);
		repeater.SetMoveToPlay (bvhImporter.lastImported);
		//repeater.Play ();
		//Debug.LogError(bvhImporter.lastExported.ToJSON());
		print ("MOVENAME"+bvhImporter.lastImported.moveName);
	}
}