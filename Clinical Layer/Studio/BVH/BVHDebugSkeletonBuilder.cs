﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BVHDebugSkeletonBuilder : MonoBehaviour {

	public BVHAnimation currentBVH;
	public float scaleMultiplier;

	public Dictionary<string,Transform> bvhJointNameToSkeletonTransform = new Dictionary<string, Transform> ();

	public Transform createdRoot;

	public BVHImporter bvhImporter;

	public void DestroySkelly() {
		bvhJointNameToSkeletonTransform.Clear();
		while(transform.childCount > 0) {
			DestroyImmediate(transform.GetChild(0).gameObject);
		}
	}

	public void BuildDebugSkelly (BVHAnimation bvh) {	
		currentBVH = bvh;
		/*StringBuilder sb = new StringBuilder ();
		foreach(ImportJoint ij in importJoints) {
			sb.AppendLine(new string('\t',ij.hierarchyLevel) + ij.sourceName);
		}
		print (sb.ToString ());*/
		
		List<GameObject> hierarchyAbove = new List<GameObject> ();
		
		ImportJoint previousJoint = null;
		GameObject previousGO = null;		
		
		foreach(ImportJoint ij in currentBVH.importJoints) {
			GameObject go = new GameObject();//GameObject.CreatePrimitive(PrimitiveType.Cube);
			go.name = ij.sourceName;
			
			//print(ij.hierarchyLevel);
			
			int levelDiff = 0;
			if(previousJoint != null)levelDiff = ij.hierarchyLevel - previousJoint.hierarchyLevel;
			//print("leveldiff:"+levelDiff);
			
			if(levelDiff == 1) {
				hierarchyAbove.Add(previousGO);
				//print(ij.sourceName+ " is child of: "+previousJoint.sourceName);
			}
			else if(levelDiff < 0) {
				for (int i = 0; i < -levelDiff; i++) {
					hierarchyAbove.RemoveAt(hierarchyAbove.Count-1);
				}
			}
			if(hierarchyAbove.Count > 0)go.transform.parent = hierarchyAbove[hierarchyAbove.Count-1].transform;
			else {
				createdRoot = new GameObject("BVH hierarchy root").transform;
				createdRoot.parent = transform;
				createdRoot.localPosition = Vector3.zero;
				go.transform.parent = createdRoot;		
			}

			Vector3 offset = ij.offset * scaleMultiplier;
			offset.x = -offset.x;
			go.transform.localPosition = offset;
			
			if(ij.hierarchyLevel != 1) {
				GameObject parentsVisualGO = GameObject.CreatePrimitive(PrimitiveType.Cube);
				parentsVisualGO.name = "LinkTo_"+ij.sourceName;
				parentsVisualGO.transform.parent = go.transform.parent;
				parentsVisualGO.transform.localPosition = Vector3.zero;
				parentsVisualGO.transform.LookAt(go.transform);
				parentsVisualGO.transform.localScale = new Vector3(0.05f * scaleMultiplier,0.05f * scaleMultiplier,(Vector3.Distance(go.transform.position,go.transform.parent.position) * 0.9f));
				parentsVisualGO.transform.Translate(parentsVisualGO.transform.localScale.z * 0.5f * Vector3.forward,Space.Self);
				
				if(bvhImporter != null && bvhImporter.rigBonesToJointTypeMapping.ContainsKey(ij.sourceName)) {
					//parentsVisualGO.renderer.material.color = Color.green;
					go.name += " ("+bvhImporter.rigBonesToJointTypeMapping[ij.sourceName]+")";
				}
			}
			
			/*// :D
			//visualGO.AddComponent<BoxCollider>();
			go.AddComponent<Rigidbody>();
			go.rigidbody.mass = visualGO.transform.localScale.z * 10;
			if(go.transform.parent.rigidbody != null) {
				go.AddComponent<CharacterJoint>();
				go.GetComponent<CharacterJoint>().connectedBody = go.transform.parent.rigidbody;
				//go.GetComponent<FixedJoint>().anchor = Vector3.zero;
				//Physics.IgnoreCollision(visualGO.collider,go.transform.parent.FindChild("Cube").collider);
				//Time.timeScale = 0.1f;
			}
			//go.rigidbody.isKinematic = true;
			//visualGO.hideFlags = HideFlags.HideInHierarchy; //lol*/
			
			/*go.transform.LookAt(previousGO.transform);
			float dist = Vector3.Distance(go.transform.position,previousGO.transform.position);
			go.transform.localScale = new Vector3(0.05f,0.05f,dist);
			go.transform.Translate(go.transform.forward * dist * 0.5f);*/

			if(bvhJointNameToSkeletonTransform.ContainsKey(ij.sourceName)) {
				Debug.LogWarning("DOUBLE NAME FOR JOINT IN BVH FILE: "+ij.sourceName);
			}
			else if(!ij.isEndSite)bvhJointNameToSkeletonTransform.Add(ij.sourceName,go.transform);
			
			previousJoint = ij;
			previousGO = go;
		}
		//SendMessage("AssignJoints",SendMessageOptions.DontRequireReceiver);
	}
}
