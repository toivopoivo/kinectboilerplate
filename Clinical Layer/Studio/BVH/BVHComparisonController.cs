﻿using UnityEngine;
using System.Collections;
using ClinicalLayer;

public class BVHComparisonController : MonoBehaviour {
	public static BVHComparisonController instance;

	public BVHDebugSkeletonBuilder bvhSkeletonBuilderForReference;
	public BVHDebugSkeletonAnimator referenceSkeletonAnimator;

	public BVHDebugSkeletonBuilder bvhSkeletonBuilderForClinical;
	public TrackedJointsAutoAssignerForBVH clinicalJointAssigner;

	//The moveRepeater of which's acting to follow
	public MoveRepeater leadRepeater;

	private int setFrame;

    private bool isActive = false;

	void Awake() {
		instance = this;
	}

	public void JustImported ()	{
		bvhSkeletonBuilderForReference.DestroySkelly ();
		bvhSkeletonBuilderForClinical.DestroySkelly ();

		bvhSkeletonBuilderForReference.BuildDebugSkelly (BVHImporter.instance.currentBVH);
		bvhSkeletonBuilderForClinical.BuildDebugSkelly (BVHImporter.instance.currentBVH);

		//clinicalJointAssigner.AssignJoints (); //TODO:re-enable!

        isActive = true;

		FrameChanged (0);
	}

	void Update() {
        if (!isActive) return;
		if(leadRepeater.currentFrameNumber != setFrame)FrameChanged(leadRepeater.currentFrameNumber);
	}

	public void FrameChanged(int frame) {
		setFrame = frame;
		referenceSkeletonAnimator.SetFrame (frame);
        //clinicalJointAssigner.repeater.SetFrame(frame); //TODO:re-enable!
	}
}
