﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Kinect = Windows.Kinect;

/*[System.Serializable]
public class MeshForJointType {
	public Renderer mesh;
	public Windows.Kinect.JointType joint;
}*/


[System.Serializable]
public class JointHighlightMeshes {
	public Windows.Kinect.JointType jointType;
	public Renderer[] highlightMeshes;
}


public class JointHighlightMeshManager : MonoBehaviour {

	public Color ignoredColor;

	public JointHighlightMeshes[] jointHighlightMeshes;
	private Dictionary<Kinect.JointType,JointHighlightMeshes> jointHighlightMeshesDic;
	
	[ContextMenu("MakehighlightCollForEveryJoint")]
	void MakehighlightCollForEveryJoint() {
		jointHighlightMeshes = new JointHighlightMeshes[25];
		for (int i = 0; i < 25; i++) {
			jointHighlightMeshes[i] = new JointHighlightMeshes();
			jointHighlightMeshes[i].jointType = (Kinect.JointType)i;
		}
	}

	void Start() {
		jointHighlightMeshesDic = new Dictionary<Kinect.JointType, JointHighlightMeshes>();
		foreach(var h in jointHighlightMeshes) {
			jointHighlightMeshesDic.Add(h.jointType,h);
		}
	}

	public void DisplayJointCompliance(float[] jointComplyValuesPerJointType) {
		for (int i = 0; i < 25; i++) {
			Kinect.JointType t = (Kinect.JointType)i;
			Color highlightColor;
			if(jointComplyValuesPerJointType[i] < -500f)highlightColor = ignoredColor;
			else highlightColor = Color.Lerp(Color.black,Color.green,jointComplyValuesPerJointType[i]);

			foreach(Renderer r in jointHighlightMeshesDic[t].highlightMeshes) {
				float a = r.sharedMaterial.color.a;
				r.material.SetColor("_Color",new Color(highlightColor.r,highlightColor.g,highlightColor.b,a));
				//r.material.color = new Color(highlightColor.r,highlightColor.g,highlightColor.b,a);
			}
		}
	}
}
