﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ClinicalLayer;
using Kinect = Windows.Kinect; //TODO: undepend

public class BoxSkeleton : MonoBehaviour {

	//public List<GameObject> boneBoxes = new List<GameObject>();
	public GameObject bonePrefab;
	public GameObject boneBallPrefab;

	public bool makeNonColorable;

	[ContextMenu("Create box skeleton")]
	public void CreateBoxSkeleton()
	{
		TrackedJoint[] joints = GetComponentsInChildren<TrackedJoint>();

		RemoveBoxSkeleton();
		/*while (boneBoxes.Count > 0)
		{
			DestroyImmediate(boneBoxes[0]);
			boneBoxes.RemoveAt(0);
		}*/
		
		foreach (TrackedJoint joint in joints)
		{
			if (!joint.childJoint) continue;
			GameObject box = GameObject.Instantiate(bonePrefab) as GameObject;
			Vector3 thisPos = joint.transform.position;
			Vector3 childPos = joint.childJoint.transform.position;
			
			Vector3 middlePos = Vector3.Lerp(thisPos, childPos, 0.5f);
			box.transform.position = middlePos;
			
			float boxLenght = Vector3.Distance(thisPos, childPos) - 0.05f;
			float thickness = box.transform.localScale.x;
			box.transform.localScale = new Vector3(thickness, thickness, boxLenght);
			
			box.transform.parent = joint.transform;
			//box.transform.localRotation = Quaternion.identity;
			box.transform.rotation = Quaternion.LookRotation(Utils.GetJointPointingDirection(joint.childJoint.thisType)); //NOTE taking in to account the root's rotation (to which this component is attached)
			//box.transform.Rotate(90,0,0);
			//box.transform.LookAt(joint.childJoint.transform);
			
			//float boxLenght = 0.8f*(Vector3.Distance(thisPos,childPos));
			//Debug.LogWarning(joint.name+" distance to "+joint.childJoint.name+" ###IS### "+boxLenght);
			box.name += joint.thisType + " to " + joint.childJoint.thisType;
			
			
			if (joint.childJoint.thisType == Kinect.JointType.HipLeft || joint.childJoint.thisType == Kinect.JointType.HipRight) thickness *= 0.5f;
			
			//box.transform.localScale = Vector3.Scale(new Vector3(thickness,thickness,boxLenght),(new Vector3(1 / box.transform.lossyScale.x,1 / box.transform.lossyScale.y,1 / box.transform.lossyScale.z)));
			
			//boneBoxes.Add(box);
			
			GameObject ball = GameObject.Instantiate(boneBallPrefab) as GameObject;
			ball.transform.parent = joint.transform;
			ball.transform.localPosition = Vector3.zero;
			
			//boneBoxes.Add(ball);

			if(!makeNonColorable) {
				joint.colorableMeshes = new Renderer[2];
				joint.colorableMeshes[0] = box.GetComponentInChildren<Renderer>();
				joint.colorableMeshes[1] = ball.GetComponentInChildren<Renderer>();
			}
			//print(joint.colorableMeshes[0]);
		}
	}

	[ContextMenu("Remove box skeleton")]
	void RemoveBoxSkeleton() {
		RemoveChildrenOfNameRecursive(transform,"BOX");
	}

	void RemoveChildrenOfNameRecursive (Transform parent, string nameToDelete) {
		List<GameObject> toDeleteList = new List<GameObject>();
		foreach(Transform child in parent) {
			RemoveChildrenOfNameRecursive(child,nameToDelete);
			if(child.name.StartsWith(nameToDelete))toDeleteList.Add(child.gameObject);
		}
		while(toDeleteList.Count > 0) {
			DestroyImmediate(toDeleteList[0]);
			toDeleteList.RemoveAt(0);
		}
	}
}
