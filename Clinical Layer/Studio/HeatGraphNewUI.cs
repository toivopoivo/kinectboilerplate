﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class HeatGraphNewUI : MonoBehaviour {

	public GameObject graphPointPrefab;
	private List<UnityEngine.UI.Image> graphPointsList = new List<UnityEngine.UI.Image>();
	private UnityEngine.UI.Image[] graphPoints;

	public RectTransform graphContainer;

	public RectTransform currentMarker;
	public RectTransform expectedMarker;

	private Vector3 pointOrigSize;

	void Start() {
		//DestroyGraph ();
	}

	public void CreateGraph(float numItems) {
		DestroyGraph ();
		print ("Creating heat graph, num items:"+numItems);

		//float pointWidth = graphWidth / numItems;
		pointOrigSize = graphPointPrefab.transform.localScale;

		for (int i = 0; i < numItems; i++) {
			//Vector3 pos = Vector3.zero;
			//pos.x = (i*pointWidth)-graphWidth*0.5f;
			//GameObject temp = Instantiate(graphPointPrefab.gameObject,transform.position+pos,Quaternion.identity) as GameObject;
			GameObject temp = Instantiate(graphPointPrefab) as GameObject;
			temp.transform.SetParent(graphContainer,true);

			//temp.transform.localScale = new Vector3(pointWidth,pointOrigSize.y,pointOrigSize.z);
			//temp.transform.parent = transform;
			//temp.layer = LayerMask.NameToLayer("drawover");
			graphPointsList.Add(temp.GetComponent<UnityEngine.UI.Image>());
		}
		graphPoints = graphPointsList.ToArray ();

		//currentMarker.gameObject.SetActive (true);
		//expectedMarker.gameObject.SetActive (true);

		float[] emptyfloats = new float[(int)numItems];
		for (int i = 0; i < numItems; i++) {
			emptyfloats[i] = 0f;
		}
		UpdateHeatMap(emptyfloats);
	}

	public void DestroyGraph() {
		while(graphPointsList.Count>0) {
			DestroyImmediate(graphPointsList[0].gameObject);
			graphPointsList.RemoveAt(0);
		}
		graphPointsList.Clear ();

		//currentMarker.gameObject.SetActive (false);
		//expectedMarker.gameObject.SetActive (false);
		//UnityEditor.EditorApplication.Beep ();
		//Debug.LogWarning ("Destroyed graph "+transform.parent.name);
	}

	public void UpdateMarkers(float currentPos,float expectedPos) {
		float graphWidth = graphContainer.rect.width;

		Vector3 pos = currentMarker.localPosition;
		pos.x = (currentPos*graphWidth) - graphWidth*0.5f;
		currentMarker.localPosition= pos;

		pos = expectedMarker.localPosition;
		pos.x = (expectedPos*graphWidth) - graphWidth*0.5f;
		expectedMarker.localPosition = pos;
		
		//currentMarker.localPosition = new Vector2((graphContainer.rect.width * currentPos) - graphContainer.rect.width * 0.5f,0);
		
	}

	public void UpdateHeatMap(float[] values) {
		for (int i = 0; i < graphPoints.Length; i++) {
			if(values[i]>2f)graphPoints[i].color = Color.black;
			else {
				graphPoints[i].color = Color.Lerp(Color.red,Color.green,values[i]);
				Vector3 scale = graphPoints[i].transform.localScale;
				scale.y = Mathf.Clamp01(values[i])*pointOrigSize.y;
				graphPoints[i].transform.localScale = scale;
			}
		}
	}
}
