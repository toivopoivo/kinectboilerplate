﻿using UnityEngine;
using System.Collections;

public class GlueToTarget : MonoBehaviour {

	public Transform target;
	private Vector3 offset;

    private Vector3 origPos;

    public bool lockY = false;

	public bool dontKeepStartRelation = false;


	public bool followRotation = false;
	public bool followScale = false;

	void Start () {
        origPos = transform.position;
		offset = target.position - transform.position;
		if(dontKeepStartRelation) offset = Vector3.zero;
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = Vector3.Lerp (transform.position, target.position - offset, Time.time * 5f);
        if(lockY)transform.position = new Vector3(transform.position.x,origPos.y,transform.position.z);

		if(followRotation) {
			transform.rotation = target.rotation;
		}
		if(followScale) {
			transform.localScale = target.localScale;
		}
	}
}
