%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!1011 &101100000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: handsonlymask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Dummy001
    m_Weight: 0
  - m_Path: Dummy001/adamsapple
    m_Weight: 0
  - m_Path: Dummy001/back
    m_Weight: 0
  - m_Path: Dummy001/bicepL
    m_Weight: 0
  - m_Path: Dummy001/bicepR
    m_Weight: 0
  - m_Path: Dummy001/breast
    m_Weight: 0
  - m_Path: Dummy001/buttL
    m_Weight: 0
  - m_Path: Dummy001/buttR
    m_Weight: 0
  - m_Path: Dummy001/hamstringsL
    m_Weight: 0
  - m_Path: Dummy001/hamstringsR
    m_Weight: 0
  - m_Path: Dummy001/head
    m_Weight: 0
  - m_Path: Dummy001/innerthighsL
    m_Weight: 0
  - m_Path: Dummy001/innerthighsR
    m_Weight: 0
  - m_Path: Dummy001/kneeL
    m_Weight: 0
  - m_Path: Dummy001/kneeR
    m_Weight: 0
  - m_Path: Dummy001/l_calf
    m_Weight: 0
  - m_Path: Dummy001/l_foot
    m_Weight: 0
  - m_Path: Dummy001/l_hand
    m_Weight: 0
  - m_Path: Dummy001/lateralL
    m_Weight: 0
  - m_Path: Dummy001/lateralR
    m_Weight: 0
  - m_Path: Dummy001/neck
    m_Weight: 0
  - m_Path: Dummy001/outerThighL
    m_Weight: 0
  - m_Path: Dummy001/outerThighR
    m_Weight: 0
  - m_Path: Dummy001/r_calf
    m_Weight: 0
  - m_Path: Dummy001/r_foot
    m_Weight: 0
  - m_Path: Dummy001/r_hand
    m_Weight: 0
  - m_Path: Dummy001/shouldersL
    m_Weight: 0
  - m_Path: Dummy001/shouldersR
    m_Weight: 0
  - m_Path: Dummy001/sideL
    m_Weight: 0
  - m_Path: Dummy001/sideR
    m_Weight: 0
  - m_Path: Dummy001/stomach
    m_Weight: 0
  - m_Path: Dummy001/trapeziusL
    m_Weight: 0
  - m_Path: Dummy001/trapeziusR
    m_Weight: 0
  - m_Path: Dummy001/tricepsL
    m_Weight: 0
  - m_Path: Dummy001/tricepsR
    m_Weight: 0
  - m_Path: Dummy001/wristsL
    m_Weight: 0
  - m_Path: Dummy001/wristsR
    m_Weight: 0
  - m_Path: rigi
    m_Weight: 0
  - m_Path: rigi/rigi Footsteps
    m_Weight: 0
  - m_Path: rigi Pelvis
    m_Weight: 0
  - m_Path: rigi Pelvis/rigi L Thigh
    m_Weight: 0
  - m_Path: rigi Pelvis/rigi L Thigh/rigi L Calf
    m_Weight: 0
  - m_Path: rigi Pelvis/rigi L Thigh/rigi L Calf/rigi L Foot
    m_Weight: 0
  - m_Path: rigi Pelvis/rigi L Thigh/rigi L Calf/rigi L Foot/rigi L Toe0
    m_Weight: 0
  - m_Path: rigi Pelvis/rigi L Thigh/rigi L Calf/rigi L Foot/rigi L Toe0/rigi L Toe01
    m_Weight: 0
  - m_Path: rigi Pelvis/rigi L Thigh/rigi L Calf/rigi L Foot/rigi L Toe0/rigi L Toe01/rigi
      L Toe02
    m_Weight: 0
  - m_Path: rigi Pelvis/rigi L Thigh/rigi L Calf/rigi L Foot/rigi L Toe0/rigi L Toe01/rigi
      L Toe02/rigi L Toe0Nub
    m_Weight: 0
  - m_Path: rigi Pelvis/rigi R Thigh
    m_Weight: 0
  - m_Path: rigi Pelvis/rigi R Thigh/rigi R Calf
    m_Weight: 0
  - m_Path: rigi Pelvis/rigi R Thigh/rigi R Calf/rigi R Foot
    m_Weight: 0
  - m_Path: rigi Pelvis/rigi R Thigh/rigi R Calf/rigi R Foot/rigi R Toe0
    m_Weight: 0
  - m_Path: rigi Pelvis/rigi R Thigh/rigi R Calf/rigi R Foot/rigi R Toe0/rigi R Toe01
    m_Weight: 0
  - m_Path: rigi Pelvis/rigi R Thigh/rigi R Calf/rigi R Foot/rigi R Toe0/rigi R Toe01/rigi
      R Toe02
    m_Weight: 0
  - m_Path: rigi Pelvis/rigi R Thigh/rigi R Calf/rigi R Foot/rigi R Toe0/rigi R Toe01/rigi
      R Toe02/rigi R Toe0Nub
    m_Weight: 0
  - m_Path: rigi Pelvis/rigi Spine
    m_Weight: 0
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1
    m_Weight: 0
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi L Clavicle
    m_Weight: 0
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi L Clavicle/rigi L UpperArm
    m_Weight: 0
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi L Clavicle/rigi L UpperArm/rigi
      L Forearm
    m_Weight: 0
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi L Clavicle/rigi L UpperArm/rigi
      L Forearm/rigi L Hand
    m_Weight: 0
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi L Clavicle/rigi L UpperArm/rigi
      L Forearm/rigi L Hand/rigi L Finger0
    m_Weight: 1
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi L Clavicle/rigi L UpperArm/rigi
      L Forearm/rigi L Hand/rigi L Finger0/rigi L Finger01
    m_Weight: 1
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi L Clavicle/rigi L UpperArm/rigi
      L Forearm/rigi L Hand/rigi L Finger0/rigi L Finger01/rigi L Finger02
    m_Weight: 1
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi L Clavicle/rigi L UpperArm/rigi
      L Forearm/rigi L Hand/rigi L Finger0/rigi L Finger01/rigi L Finger02/rigi L
      Finger0Nub
    m_Weight: 1
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi L Clavicle/rigi L UpperArm/rigi
      L Forearm/rigi L Hand/rigi L Finger1
    m_Weight: 1
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi L Clavicle/rigi L UpperArm/rigi
      L Forearm/rigi L Hand/rigi L Finger1/rigi L Finger11
    m_Weight: 1
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi L Clavicle/rigi L UpperArm/rigi
      L Forearm/rigi L Hand/rigi L Finger1/rigi L Finger11/rigi L Finger12
    m_Weight: 1
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi L Clavicle/rigi L UpperArm/rigi
      L Forearm/rigi L Hand/rigi L Finger1/rigi L Finger11/rigi L Finger12/rigi L
      Finger1Nub
    m_Weight: 1
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi L Clavicle/rigi L UpperArm/rigi
      L Forearm/rigi L Hand/rigi L Finger2
    m_Weight: 1
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi L Clavicle/rigi L UpperArm/rigi
      L Forearm/rigi L Hand/rigi L Finger2/rigi L Finger21
    m_Weight: 1
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi L Clavicle/rigi L UpperArm/rigi
      L Forearm/rigi L Hand/rigi L Finger2/rigi L Finger21/rigi L Finger22
    m_Weight: 1
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi L Clavicle/rigi L UpperArm/rigi
      L Forearm/rigi L Hand/rigi L Finger2/rigi L Finger21/rigi L Finger22/rigi L
      Finger2Nub
    m_Weight: 1
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi L Clavicle/rigi L UpperArm/rigi
      L Forearm/rigi L Hand/rigi L Finger3
    m_Weight: 1
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi L Clavicle/rigi L UpperArm/rigi
      L Forearm/rigi L Hand/rigi L Finger3/rigi L Finger31
    m_Weight: 1
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi L Clavicle/rigi L UpperArm/rigi
      L Forearm/rigi L Hand/rigi L Finger3/rigi L Finger31/rigi L Finger32
    m_Weight: 1
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi L Clavicle/rigi L UpperArm/rigi
      L Forearm/rigi L Hand/rigi L Finger3/rigi L Finger31/rigi L Finger32/rigi L
      Finger3Nub
    m_Weight: 1
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi L Clavicle/rigi L UpperArm/rigi
      L Forearm/rigi L Hand/rigi L Finger4
    m_Weight: 1
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi L Clavicle/rigi L UpperArm/rigi
      L Forearm/rigi L Hand/rigi L Finger4/rigi L Finger41
    m_Weight: 1
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi L Clavicle/rigi L UpperArm/rigi
      L Forearm/rigi L Hand/rigi L Finger4/rigi L Finger41/rigi L Finger42
    m_Weight: 1
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi L Clavicle/rigi L UpperArm/rigi
      L Forearm/rigi L Hand/rigi L Finger4/rigi L Finger41/rigi L Finger42/rigi L
      Finger4Nub
    m_Weight: 1
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi Neck
    m_Weight: 0
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi Neck/rigi Head
    m_Weight: 0
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi Neck/rigi Head/rigi HeadNub
    m_Weight: 0
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi R Clavicle
    m_Weight: 0
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi R Clavicle/rigi R UpperArm
    m_Weight: 0
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi R Clavicle/rigi R UpperArm/rigi
      R Forearm
    m_Weight: 0
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi R Clavicle/rigi R UpperArm/rigi
      R Forearm/rigi R Hand
    m_Weight: 0
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi R Clavicle/rigi R UpperArm/rigi
      R Forearm/rigi R Hand/rigi R Finger0
    m_Weight: 1
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi R Clavicle/rigi R UpperArm/rigi
      R Forearm/rigi R Hand/rigi R Finger0/rigi R Finger01
    m_Weight: 1
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi R Clavicle/rigi R UpperArm/rigi
      R Forearm/rigi R Hand/rigi R Finger0/rigi R Finger01/rigi R Finger02
    m_Weight: 1
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi R Clavicle/rigi R UpperArm/rigi
      R Forearm/rigi R Hand/rigi R Finger0/rigi R Finger01/rigi R Finger02/rigi R
      Finger0Nub
    m_Weight: 1
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi R Clavicle/rigi R UpperArm/rigi
      R Forearm/rigi R Hand/rigi R Finger1
    m_Weight: 1
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi R Clavicle/rigi R UpperArm/rigi
      R Forearm/rigi R Hand/rigi R Finger1/rigi R Finger11
    m_Weight: 1
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi R Clavicle/rigi R UpperArm/rigi
      R Forearm/rigi R Hand/rigi R Finger1/rigi R Finger11/rigi R Finger12
    m_Weight: 1
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi R Clavicle/rigi R UpperArm/rigi
      R Forearm/rigi R Hand/rigi R Finger1/rigi R Finger11/rigi R Finger12/rigi R
      Finger1Nub
    m_Weight: 1
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi R Clavicle/rigi R UpperArm/rigi
      R Forearm/rigi R Hand/rigi R Finger2
    m_Weight: 1
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi R Clavicle/rigi R UpperArm/rigi
      R Forearm/rigi R Hand/rigi R Finger2/rigi R Finger21
    m_Weight: 1
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi R Clavicle/rigi R UpperArm/rigi
      R Forearm/rigi R Hand/rigi R Finger2/rigi R Finger21/rigi R Finger22
    m_Weight: 1
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi R Clavicle/rigi R UpperArm/rigi
      R Forearm/rigi R Hand/rigi R Finger2/rigi R Finger21/rigi R Finger22/rigi R
      Finger2Nub
    m_Weight: 1
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi R Clavicle/rigi R UpperArm/rigi
      R Forearm/rigi R Hand/rigi R Finger3
    m_Weight: 1
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi R Clavicle/rigi R UpperArm/rigi
      R Forearm/rigi R Hand/rigi R Finger3/rigi R Finger31
    m_Weight: 1
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi R Clavicle/rigi R UpperArm/rigi
      R Forearm/rigi R Hand/rigi R Finger3/rigi R Finger31/rigi R Finger32
    m_Weight: 1
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi R Clavicle/rigi R UpperArm/rigi
      R Forearm/rigi R Hand/rigi R Finger3/rigi R Finger31/rigi R Finger32/rigi R
      Finger3Nub
    m_Weight: 1
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi R Clavicle/rigi R UpperArm/rigi
      R Forearm/rigi R Hand/rigi R Finger4
    m_Weight: 1
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi R Clavicle/rigi R UpperArm/rigi
      R Forearm/rigi R Hand/rigi R Finger4/rigi R Finger41
    m_Weight: 1
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi R Clavicle/rigi R UpperArm/rigi
      R Forearm/rigi R Hand/rigi R Finger4/rigi R Finger41/rigi R Finger42
    m_Weight: 1
  - m_Path: rigi Pelvis/rigi Spine/rigi Spine1/rigi R Clavicle/rigi R UpperArm/rigi
      R Forearm/rigi R Hand/rigi R Finger4/rigi R Finger41/rigi R Finger42/rigi R
      Finger4Nub
    m_Weight: 1
