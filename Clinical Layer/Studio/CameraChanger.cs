﻿using UnityEngine;
using System.Collections;

public enum StudioCameraPosition {Default, OneChar, TwoChars, JointPicking};

public class CameraChanger : MonoBehaviour {

	public static CameraChanger instance;

	public Camera theCamera;

	public Transform[] cameraPositions;

	void Awake() {
		instance = this;
	}

	// Use this for initialization
	void Start () {
		foreach(Transform camPos in cameraPositions) {
			camPos.GetComponent<Camera>().enabled = false;
		}

		MoveCameraTo (StudioCameraPosition.Default,time:0f);
	}

	public void MoveCameraTo(StudioCameraPosition toPos, float time = 1f) {
		StartCoroutine (MoveToTransformAndFollow(theCamera.transform,cameraPositions[(int)toPos],time:time));
	}

	public IEnumerator MoveCameraToAndReturn(StudioCameraPosition toPos) {
		yield return StartCoroutine (MoveToTransformAndFollow(theCamera.transform,cameraPositions[(int)toPos]));
	}

	//TODO: make this take in to account actively moving / rotating targets... ATM it fixates to the point where the target was when the transition was started
	//Maybe no proper way to do this while still using EaseTypes... meh
	IEnumerator MoveToTransformAndFollow(Transform toMove, Transform target, float time = 1f) {
		//TODO: move toMove to target's position and when there, lock to it
		//iTween.EaseType chosenEaseType = iTween.EaseType.easeInOutCubic;
		
		/*iTween.MoveTo(toMove.gameObject,iTween.Hash("position",target.position,"time",time,"easetype",chosenEaseType)); //TODO: fix lockin'
		iTween.RotateTo(toMove.gameObject,iTween.Hash("rotation",target.eulerAngles,"time",time,"easetype",chosenEaseType));
		iTween.ValueTo(gameObject, iTween.Hash("time", time,"easetype",chosenEaseType, "from", theCamera.fieldOfView, "to", target.GetComponent<Camera>().fieldOfView, "onupdate", "FOVUpdate"));		
		yield return new WaitForSeconds(time);*/

		toMove.parent = null;

		float fromFov = theCamera.fieldOfView;
		float timer = 0;
		while(/*timer < time*/true) {
			iTween.MoveUpdate(toMove.gameObject,target.position,time); //actually have to use the total time, not the current time... odd
			iTween.RotateUpdate(toMove.gameObject,target.eulerAngles,time);
			theCamera.fieldOfView = Mathf.Lerp(fromFov,target.GetComponent<Camera>().fieldOfView,timer);
			timer+=Time.deltaTime;

			bool tooMuchTimeSpent = timer > time * 4f;
			bool close = (Vector3.Distance(toMove.position,target.position) < 0.05f);
			bool rotationClose = (Quaternion.Angle(toMove.rotation,target.rotation) < 1f);

			if(close && rotationClose || tooMuchTimeSpent)break;

			yield return null;
		}

		toMove.parent = target;
		toMove.transform.localRotation = Quaternion.identity;
		toMove.transform.localPosition = Vector3.zero;
		theCamera.fieldOfView = target.GetComponent<Camera> ().fieldOfView;
	}
	
	void FOVUpdate(float fov) {
		theCamera.fieldOfView = fov;
	}
}
