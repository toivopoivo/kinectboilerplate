﻿using UnityEngine;
using System.Collections;

public enum ShowHideStartAction {Show, Hide, Nothing};

public class ShowHide : MonoBehaviour {

	public Transform movingPart;
	public Transform visiblePositionAnchor;
	public Transform hiddenPositionAnchor;

	public ShowHideStartAction startAction = ShowHideStartAction.Nothing;
    public bool tweenStartAction = false; //ONLY SUPPORTS SHOW

	public bool doToggleMovedGOState = false;

	private bool debug = false;

	private bool visible;

	void Start() { //TODO: maybe revert to use Start, (OnEnabled not necessarily wanted behaviour always)
        if (startAction == ShowHideStartAction.Show) {
            if (!tweenStartAction) SnapShow();
            else {
                SnapHide();
                Show();
            }
        }
        else if (startAction == ShowHideStartAction.Hide) {
			if (!tweenStartAction)SnapHide();
			else {
				SnapShow();
				Hide();
			}
		}
	}


	public void Hide() {
		if(debug)Debug.LogWarning("Tween hiding "+name);
		iTween.EaseType chosenEaseType = iTween.EaseType.easeInOutCubic;		
		iTween.MoveTo(movingPart.gameObject,iTween.Hash("position",hiddenPositionAnchor.position,"time",0.5f,"easetype",chosenEaseType));
		visible = false;
		Invoke("ToggleMovingPartIfAsked",0.5f);
	}


	public void Show() {
		if(debug)Debug.LogWarning("Tween showing "+name);
		iTween.EaseType chosenEaseType = iTween.EaseType.easeInOutCubic;		
		iTween.MoveTo(movingPart.gameObject,iTween.Hash("position",visiblePositionAnchor.position,"time",0.5f,"easetype",chosenEaseType));
		visible = true;
		Invoke("ToggleMovingPartIfAsked",0.5f);
	}

	[ContextMenu("Hide")]
	void SnapHide() {
		if(debug)Debug.LogWarning("Snap hiding "+name);
		movingPart.position = hiddenPositionAnchor.position;
		visible = false;
		ToggleMovingPartIfAsked();
	}

	[ContextMenu("Show")]
	void SnapShow() {
		if(debug)Debug.LogWarning("Snap showing "+name);
		movingPart.position = visiblePositionAnchor.position;
		visible = true;
		ToggleMovingPartIfAsked();
	}

	void ToggleMovingPartIfAsked() {
		if(doToggleMovedGOState) {
			movingPart.gameObject.SetActive(visible);
		}
	}
}
