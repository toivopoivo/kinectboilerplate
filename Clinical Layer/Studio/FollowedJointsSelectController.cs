using UnityEngine;
using System.Collections;
using System.Text;
using Kinect = Windows.Kinect;
using UnityEngine.UI;
using ClinicalLayer;

public class FollowedJointsSelectController : MonoBehaviour {

	public bool selectingInProgress = false;
	public bool[] ignoredStatuses = new bool[25];

	public Text followedText;
	public Text ignoredText;

	public Text autoDegreesText;

	public Toggle autoToggle;
	public Slider autoSlider;

	private ModelControllerFollow currJointController;
	private FollowMode prevFollowMode;
	private ClinicalMove currMove;

	// Update is called once per frame
	void Update () {
		if(selectingInProgress)SelectingUpdate();
	}

	void SelectingUpdate () {
		if(Input.GetMouseButtonUp(0)) {
			
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hitInfo;
			
			if(Physics.Raycast(ray, out hitInfo, 1000)) {
				TrackedJoint hitJoint = hitInfo.collider.GetComponentInParent<TrackedJoint>();
				if(hitJoint != null) {
					print(hitJoint.thisType);
					hitJoint.Clicked();
					//ignoredStatuses[(int)hitJoint.thisType] = hitJoint.ignored;
					UpdateIgnoredStatuses ();
					autoToggle.isOn = false; //disable automatic control to avoid confusion (needs to be toggled on again to get automatic values, overriding these)
					ClinicalMovePropertiesControl.instance.dirtyState = true;

					currJointController.SetIgnoredJoints (ignoredStatuses,true); //Run just to get JointHiglihteMeshManager updated (otherwise redundant)
				}
			}
		}
	}

	public IEnumerator SelectAndReturn(ModelControllerFollow jointsController, ClinicalMove currentMove) {
		selectingInProgress = true;
		currJointController = jointsController;
		currMove = currentMove;
		prevFollowMode = currJointController.currentFollowMode;
		currJointController.currentFollowMode = FollowMode.ForceTPose;
		autoToggle.isOn = false;
		UpdateIgnoredStatuses ();
		currJointController.SetIgnoredJoints (ignoredStatuses,true);
		while(selectingInProgress) {
			yield return null;
		}
		//exit when selectinginprogress is turned false from button
		currMove.SetIgnoredJointsForAllFrames(jointsController.GetIgnoredJoints());
		jointsController.currentFollowMode = prevFollowMode;
	}

	public void PickingDone() {
		selectingInProgress = false;
	}

	public void AutomaticCheckBoxAltered(bool on) {
		//Toggled on: trigger slider's onvaluechanged
		//Will be also called manually to off if joint ignored states are manually altered
		if(on) {
			//wat
			float asd = autoSlider.value;
			autoSlider.value = 100;
			autoSlider.value = asd;
		}
		autoSlider.interactable = on;
	}

	public void AutomaticSliderChanged(float draggedTreshold) {
		//autoDegreesText.text = Mathf.Round (draggedTreshold);
		autoDegreesText.text = draggedTreshold.ToString ();
		ignoredStatuses = currMove.CalculateMovedJoints(draggedTreshold);
		currJointController.SetIgnoredJoints (ignoredStatuses,true);
		UpdateIgnoredStatuses();
		ClinicalMovePropertiesControl.instance.dirtyState = true;
	}

	void UpdateIgnoredStatuses() {
		ignoredStatuses = currJointController.GetIgnoredJoints ();

		StringBuilder sbIgnored = new StringBuilder ();
		StringBuilder sbFollowed = new StringBuilder ();
		for (int i = 0; i < 25; i++) {
			Kinect.JointType t = (Kinect.JointType)i;
			if(Utils.JointTypeIgnored(t))continue;
			if(ignoredStatuses[i] == true)sbIgnored.AppendLine(t.ToString());
			else sbFollowed.AppendLine(t.ToString());
		}
		ignoredText.text = sbIgnored.ToString ();
		followedText.text = sbFollowed.ToString ();
	}
}