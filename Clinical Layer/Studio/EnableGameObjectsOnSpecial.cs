﻿using UnityEngine;
using System.Collections;

public class EnableGameObjectsOnSpecial : MonoBehaviour {

	public GameObject[] bjects;

	public bool toggleAtStart = false;

	void Start() {
		if(toggleAtStart)Toggle();
	}

	// Update is called once per frame
	void Update () {
		if(Input.touchCount>1) {
			if(Input.touches[1].phase == TouchPhase.Began) {
				Toggle();
			}
		}
		if(Input.GetKeyUp(KeyCode.T)) Toggle();
	}

	void Toggle() {
		foreach (GameObject bject in bjects) {
			bject.SetActive(!bject.activeSelf);
		}
	}
}
