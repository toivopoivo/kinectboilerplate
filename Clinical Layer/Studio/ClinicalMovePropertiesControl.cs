using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;
using ClinicalLayer;

public class ClinicalMovePropertiesControl : MonoBehaviour {

	public static ClinicalMovePropertiesControl instance;

	public CuttingTool cuttingTool;
	public FollowedJointsSelectController followedJointsSelectController;

	public InputField moveNameField;
	public InputField moveNotesField;
	public InputField frameRateField;
	public Text lenghtInSecondsText;
	public Text frameCountText;
	public Text allowedErrorText;
	public Slider allowedErrorSlider;
	public ComboBox qualityDropDown;

	public MoveRepeater playBackCharacter;

	public ModelControllerFollow jointsController;

	public ClinicalMove currentlyOpenedMove;

	private Vector3 posOffset;

	public Text playbackModeText;

	//Is saving to disk needed to not lose changes? true if is
	public bool dirtyState = false;

	public string pathOfOpenFile;

    public bool lastOpeningSuccessful;

	void Awake() {
		instance = this;
	}

	void Start() {
		qualityDropDown.OnSelectionChanged += QualityDropDownChanged;
		qualityDropDown.AddItems ("Low","Medium","High");
		//toggle forth and back to propely set it
		TogglePlaybackMode();
		TogglePlaybackMode();
	}

	public void SetMove(ClinicalMove inMove, bool unSaved = false) {
		currentlyOpenedMove = inMove;
		SetControls ();

		dirtyState = unSaved;
		if(unSaved)pathOfOpenFile = ""; //Not saved as a file yet
	}

	public IEnumerator OpenFile(string path) {
        lastOpeningSuccessful = false; //default false
		ClinicalMove created = null;
		try {
			created = ClinicalMove.CreateFromJson (File.ReadAllText (path));
		}
		/*catch(LitJson.JsonException e) {
			Debug.LogWarning("Exception while parsing JSON:"+path+"\n\nError:"+e.Message+"\n"+e.StackTrace);
            yield break;
		}*/
		catch(System.Exception e) {
			Debug.LogWarning("Parsing failed, attempting old format deserialization for: "+path+"\n\nError:"+e.Message+"\n"+e.StackTrace);
			try {
				created = new ClinicalMove(File.ReadAllBytes(path));
				AlertBox.Show("Opened move is using old format. Saving will use new format.");
			}
			catch(System.Exception er) {
				AlertBox.Show("Failed to open\n"+path+"\n\nError:"+er.Message+"\n"+er.StackTrace);
                yield break;
			}
		}

        //Prompt before opening if file version is newer
        if (created.fileFormatVersionMajor > ClinicalMove.GetGeneratorFileFormatVersionMajor() || created.fileFormatVersionMinor > ClinicalMove.GetGeneratorFileFormatVersionMinor()) {
            string msg = "File was saved in a newer version of the application! It is recommended to update the application. Do you still want to try to open the file? This might cause loss of data.";
            msg += "\nFile version: " + created.GetFileFormatVersionVerbose() + " Application version: " + ClinicalMove.GetgeneratorVersionVerbose();
            string[] options = {"Cancel","Try opening the file"};
            yield return StartCoroutine(AlertBox.instance.ShowAndWait(msg,options));
            if (AlertBox.instance.lastResponse == options[0]) yield break;
        }

		try {
			SetMove (created);
		}
		catch(System.Exception e) {
			AlertBox.Show("Failed to open\n"+path+"\n\nError:"+e.Message+"\n"+e.StackTrace);
            yield break;
		}
		pathOfOpenFile = path;

		lastOpeningSuccessful = true;
	}

	public void SaveCurrentMove(string saveToFolder) {
		//actual crop
		CropToCutterArea ();

		string jsonized = currentlyOpenedMove.ToJSON ();
		File.WriteAllText (saveToFolder,jsonized);
		print ("SAVED TO "+saveToFolder);
		dirtyState = false;
	}

	void SetControls() {
		UpdateFields ();

		cuttingTool.Init (currentlyOpenedMove.lenghtInFrames);
		playBackCharacter.SetMoveToPlay (currentlyOpenedMove);
		playBackCharacter.currentPlayBackMode = PlayBackMode.EditPreview;
		playBackCharacter.Play ();

		jointsController.SetIgnoredJoints (currentlyOpenedMove.GetFrames()[0].ignoredJoints,false); //TODO:actually update the ignored statuses all the time, if frame-specific ignore values are needed
	}

	void UpdateFields () {
		moveNameField.text = currentlyOpenedMove.moveName;
		moveNotesField.text = currentlyOpenedMove.moveNotes;
		frameRateField.text = currentlyOpenedMove.frameRate.ToString ();
		lenghtInSecondsText.text = Utils.GetTimeStampSeconds (currentlyOpenedMove.timeLenght);
		frameCountText.text = currentlyOpenedMove.lenghtInFrames.ToString ();
		allowedErrorSlider.value = currentlyOpenedMove.angleComparingStrictness;
		//qualityDropDown.OnItemClicked ((int)currentlyOpenedMove.clipQuality);
		qualityDropDown.SelectedIndex = (int)currentlyOpenedMove.clipQuality;
	}

	//NOTE move needs to be reloaded or atleast cutter needs to be re-initialized after calling this.
	public void CropToCutterArea() {
		int editedLenght = (Mathf.RoundToInt(cuttingTool.activeEndFrame) - Mathf.RoundToInt(cuttingTool.activeStartFrame)) + 1;
		BodyOrientationFrame[] editedFrames = new BodyOrientationFrame[editedLenght];
		System.Array.Copy(currentlyOpenedMove.GetFrames(),Mathf.RoundToInt(cuttingTool.activeStartFrame),editedFrames,0,editedLenght);
		currentlyOpenedMove.SetFrames(editedFrames);
	}

	public void Close() {
		SendMessage("Hide");
	}

	public void NameFieldAltered(string inString) {
		currentlyOpenedMove.moveName = inString;
		dirtyState = true;
	}
	public void NotesFieldAltered(string inString) {
		currentlyOpenedMove.moveNotes = inString;
		dirtyState = true;
	}
	public void FPSFieldAltered(string inString) {
		if(float.TryParse (inString, out currentlyOpenedMove.frameRate)) {
			UpdateFields();
			playBackCharacter.frameRate = currentlyOpenedMove.frameRate;
			dirtyState = true;
		}
	}

	public void AngleStrictnessSliderAltered(float val) {
		currentlyOpenedMove.angleComparingStrictness = val;
		allowedErrorText.text = currentlyOpenedMove.angleComparingStrictness.ToString ();
	}

	public void PickIgnoredJointsButtonPressed() {
		StartCoroutine (PickingIgnoredJoints());
		//StartCoroutine (followedJointsSelectController.SelectAndReturn ());
	}
	
	IEnumerator PickingIgnoredJoints () {
		SendMessage ("Hide");
		followedJointsSelectController.SendMessage ("Show");
		//jointsController.visibilityController.Disappear ();
		yield return StartCoroutine (CameraChanger.instance.MoveCameraToAndReturn(StudioCameraPosition.JointPicking));
		StartCoroutine(jointsController.visibilityController.VisibilityTo (0.4f, 0.5f, PartialVisibilityMode.Transparent));
		StartCoroutine (followedJointsSelectController.SelectAndReturn (jointsController,currentlyOpenedMove));
		while(followedJointsSelectController.selectingInProgress) {
			yield return null;
		}
		followedJointsSelectController.SendMessage ("Hide");
		yield return StartCoroutine(jointsController.visibilityController.VisibilityTo (1, 0.5f, PartialVisibilityMode.Transparent));
		yield return StartCoroutine (CameraChanger.instance.MoveCameraToAndReturn(StudioCameraPosition.OneChar));
		SendMessage ("Show");
	}

	public void SetPosOffset(Vector3 inOffset) {
		//print ("Setting offset to "+inOffset);

		Vector3 delta = inOffset - posOffset ;

		//print (delta);

		BodyOrientationFrame[] frames = currentlyOpenedMove.GetFrames ();
		for (int i = 0; i < currentlyOpenedMove.lenghtInFrames; i++) {
			frames[i].rootPosition += delta;
		}
		currentlyOpenedMove.SetFrames (frames);

		posOffset = inOffset;

		playBackCharacter.NotifySubscribersAboutNewFrame ();

		dirtyState = true;
	}

	public void TogglePlaybackMode() {
		if(playBackCharacter.currentPlayBackMode == PlayBackMode.Manual) {
			playBackCharacter.currentPlayBackMode = PlayBackMode.EditPreview;
			playbackModeText.text = "Stop preview ❚❚";
		}
		else {
			playBackCharacter.currentPlayBackMode = PlayBackMode.Manual;
			playbackModeText.text = "Preview move ▶";
		}
	}

	void QualityDropDownChanged(int index) {
		currentlyOpenedMove.clipQuality = (BodyOrientationFrameQuality)index;
	}
}