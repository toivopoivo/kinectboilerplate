﻿using UnityEngine;
using System.Collections;
using ClinicalLayer;

public class VisibilityController : MonoBehaviour {
	
	private bool visible = true;
	private bool targetVisible = true;
	private float alpha = 1;
	private PartialVisibilityMode currentPartialVisibilityMode = PartialVisibilityMode.Transparent;
	
	private Renderer[] renderers;
	private Material[] origMats = null;

	public bool transparentAtStart = false;

	// Use this for initialization
	void Start () {
		//TODO: execute after other starts!
		renderers = GetComponentsInChildren<Renderer> ();
		
		SaveOriginalMats();

		//TODO: maybe should be in onenable?
		if(transparentAtStart) {
			StartCoroutine(VisibilityTo(0.1f,0.1f,PartialVisibilityMode.Transparent));
		}
	}

	void OnEnable() {

	}

	public IEnumerator VisibilityTo(float visTarget,float fadeTime,PartialVisibilityMode visMode) {
		MakeVisible (true);
		MakePartiallyVisible (visMode);
		//if(alpha > 0.99f)SaveOriginalMats();
		
		float startFrom = alpha;
		
		//Debug.LogError("FADING: start from:"+startFrom+" fade to: "+visTarget);
		
		float timer = 0;
		
		while(timer<fadeTime) {
			timer+=Time.deltaTime;
			SetAlpha(Mathf.Lerp(startFrom,visTarget,timer/fadeTime));
			yield return null;
		}
		SetAlpha(visTarget);
		
		if(alpha>0.95f && origMats.Length > 0)MakeOpaque();
		else if (visTarget < 0.01f)MakeVisible(false);
	}

	void SetAlpha(float a) {
		if(currentPartialVisibilityMode == PartialVisibilityMode.Transparent) {
			foreach(Renderer r in renderers) {
				Color tempColor = r.material.color;
				tempColor.a = a;
				//r.material.color = tempColor;
				r.material.SetColor("_Color",tempColor);

				/*r.enabled = false;
				r.enabled = true;*/
			}
		}
		else if (currentPartialVisibilityMode == PartialVisibilityMode.Clip) {
			foreach(Renderer r in renderers) {
				r.material.SetFloat("_clipDistance",1-a);
			}
		}
		alpha = a;
		//print (a);
	}
	
	void SaveOriginalMats() {
		origMats = new Material[renderers.Length];
		
		for (int i = 0; i < renderers.Length; i++) {
			origMats[i] = renderers[i].sharedMaterial;			
		}
	}
	
	void MakePartiallyVisible(PartialVisibilityMode partMod) {
		currentPartialVisibilityMode = partMod;
		
		foreach (Renderer r in renderers) {
			r.material = new Material(r.material); //instantiate new material, instead of changing original
			
			if(partMod == PartialVisibilityMode.Transparent) {
				//r.material.shader = Shader.Find("Transparent/Bumped Diffuse with ZWrite"/*"Transparent/VertexLit with Z"*/);

				//I HERD U LIKE MATERIAL PROPERTIES (Only way to properly change to fade mode? wtf?)
				r.material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
				r.material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
				r.material.SetInt("_ZWrite", 0);
				r.material.DisableKeyword("_ALPHATEST_ON");
				r.material.EnableKeyword("_ALPHABLEND_ON");
				r.material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
				r.material.renderQueue = 3000;

				r.material.SetFloat("_Mode", 2f);
			}
			else if (partMod == PartialVisibilityMode.Clip) {
				r.material.shader = Shader.Find("SecondChance/ClipShader(NoRim)");		
				r.material.SetTexture("_clipMask",(Texture2D)Resources.Load("noise"));
			}
		}
	}
	
	//Reverts back to original shader, from partial visibility shader
	void MakeOpaque() {
		if(!visible)MakeVisible (true);
		
		int count = 0;
		foreach (Renderer r in renderers) {
			r.material = origMats[count];
			count++;
		}
		alpha = 1;
		visible = true;
	}

	//Enable/disable renderers
	void MakeVisible(bool vis) {
		foreach (Renderer r in renderers) {
			r.enabled = vis;
		}
		visible = vis;
		if(!vis)alpha = 0;
	}
	
	public void Appear() {
		if(targetVisible)return;
		print (name+" appearing");
		if(renderers.Length < 0)return;
		StopAllCoroutines ();
		StartCoroutine (VisibilityTo (1f,1f,PartialVisibilityMode.Clip));
		targetVisible = true;
	}
	
	public void Disappear() {
		if(!targetVisible)return;
		print (name+" disappearing");
		if(renderers.Length < 0)return;
		StopAllCoroutines ();
		StartCoroutine (VisibilityTo (0f,1f,PartialVisibilityMode.Clip));
		targetVisible = false;
	}
}