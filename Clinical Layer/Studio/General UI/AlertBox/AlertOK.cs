﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AlertOK : MonoBehaviour {

	public AlertBox parentAlertBox;
	public string response;
	public Text buttonText;

	public void ThisOptionSelected() {
		AlertBox.instance.HideInstance (response);
	}

	public void Set(string textToSet, string responseToSet) {
		buttonText.text = textToSet;
		response = responseToSet;
	}

	//Overload, maybe the only one used
	public void Set(string textToSet) {
		Set(textToSet,textToSet);
	}
}
