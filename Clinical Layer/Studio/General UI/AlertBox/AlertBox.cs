﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

//This enum not in use ATM
public enum AlerBoxOptionSet{
	OK, //Simple "OK" with no choices
	NoOrYes, //Ei, Kyllä
	CreateNewOrEnterCode, //Luo uusi tunnus, käytä olemassaolevaa
	NoThanksOrShowMe, //Ei kiitos, Näytä minulle (multi device)
	AreYouSureMulti //Olen varma, En haluakkaan (denying multi device confirm)
};

public class AlertBox : MonoBehaviour {

	public static AlertBox instance;

	public Transform movingPart;
	public Text alertText;

	public bool showing = false;
	public string lastResponse;

	//public GameObject[] optionSetRoots;

	public float maxSpaceForButtons;

	public AlertOK[] buttons;

	void Awake() {
		instance = this;
	}

	void Start () {
		movingPart.gameObject.SetActive (false);
	}

	static public void Show(string message) {
		//instance.ShowInstance (message); //NEJ

		instance.StartCoroutine (instance.ShowAndWait (message, "OK"));
	}

	public IEnumerator ShowAndWait(string message, string[] responses, bool compilerSatisfyingStupidModifier) {
		SetButtons(responses.Length,responses);
		
		ShowInstance (message);
		while(showing) {
			//SetButtons(responses.Length,responses);
			yield return null;
		}
	}

	public IEnumerator ShowAndWait(string message, params string[] responsesParams) {
		yield return StartCoroutine(ShowAndWait(message,responsesParams,true));
	}

	public IEnumerator ShowAndWait(string message, AlerBoxOptionSet optionSet) {
		string[] responses;
		switch(optionSet) {
			case AlerBoxOptionSet.OK:
				responses = new string[] {"OK"};
				break;
			case AlerBoxOptionSet.NoOrYes:
				responses = new string[] {"No","Yes"};
				break;
			default:
				Debug.LogError("Not in use! use literal instead");
				responses = new string[] {"Fail!"};
				break;
		}

		yield return StartCoroutine(ShowAndWait(message, responses, true));
	}

	void SetButtons(int activeButtonsNumber, string[] responses) {
		for (int i = 0; i < buttons.Length; i++) {
			if(i < activeButtonsNumber) {
				buttons[i].gameObject.SetActive(true);
				//print("i:"+i);
				//print("len:"+buttons.Length);
				buttons[i].Set(responses[i]);
			}
			else buttons[i].gameObject.SetActive(false);
		}
	}

	void ShowInstance(string message) {
		movingPart.gameObject.SetActive (true);
		print ("Alerting:" + message);
		//iTween.MoveTo (movingPart.gameObject, iTween.Hash ("position", visibleAnchor.position, "time", 0.5f, "easetype", iTween.EaseType.easeOutCubic));
		movingPart.SendMessage ("Show");
		alertText.text = message;
		showing = true;
	}

	public void HideInstance() {
		HideInstance ("");
	}

	public void HideInstance(string response) {
		lastResponse = response;
		print ("User responded to alert box: "+response);
		//iTween.MoveTo (movingPart.gameObject, iTween.Hash ("position", hiddenAnchor, "time", 0.5f, "easetype", iTween.EaseType.easeInCubic, "oncomplete","DisableCamDelayed","oncompletetarget",gameObject));
		showing = false;
		movingPart.SendMessage ("Hide");
	}

	/*public void DisableCamDelayed() {
		//print ("prää");
		showing = false;
	}*/
}
