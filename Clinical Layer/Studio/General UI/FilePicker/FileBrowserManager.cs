﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;

public class FileBrowserManager : MonoBehaviour {

	public static FileBrowserManager instance;

	public SimpleFileBrowser browser;

	public RectTransform browserRoot;

	public FileSystemInfo lastSelection;
	public bool lastCancelled;

	void Awake() {
		instance = this;
	}

	public IEnumerator PromptForEntry(string[] acceptedExtensions, bool creatingNew, string defaultGivenFileName = "newfile", string startPath = "") {
		if(defaultGivenFileName == "newfile")defaultGivenFileName+=acceptedExtensions[0];

		print("Opening file panel, start path:"+startPath);

		browser.selectionConfirmed = false;
		browser.selectingCancelled = false;
		browser.acceptedExtensions = acceptedExtensions;
		browser.creatingNew = creatingNew;
		browser.selection = null;

		if(creatingNew)browser.fileNameField.text = defaultGivenFileName;
		browser.FileNameFieldAltered ();

		SendMessage("Show");
		//browser.Init ();
		if(startPath == "")browser.currentPath = ClinicalLayer.Utils.appFilesPath;
		else browser.currentPath = startPath;
		browser.Refresh (false);

		//Wait for user to select something
		while(!browser.selectionConfirmed && !browser.selectingCancelled) {
			yield return null;
		}

		if(browser.selection != null)lastSelection = browser.selection.thisInfo;
		lastCancelled = browser.selectingCancelled;
		SendMessage("Hide");
	}

	public void CancelAndClose() {
		StopCoroutine("PromptForEntry");
		SendMessage("Hide");
	}

	//Overload, for selecting folder //TODO: make this work, this isn't used so it doesn't work further down the line
	public IEnumerator PromptForFolder() {
		yield return StartCoroutine (PromptForEntry(new string[]{"FOLDER"},false));
	}

	/*//Overload with no default name
	public IEnumerator PromptForFile(string[] acceptedExtensions, bool creatingNew) {
		yield return StartCoroutine(PromptForEntry(new string[]{acceptedExtension},creatingNew, defaultGivenFileName:defaultGivenFileName)); //errgh but no compiler error ....
	}*/

	//Overload with only one extension
	public IEnumerator PromptForFile(string acceptedExtension, bool creatingNew, string defaultGivenFileName = "newfile", string startPath = "") {
		yield return StartCoroutine(PromptForEntry(new string[]{acceptedExtension},creatingNew, defaultGivenFileName:defaultGivenFileName, startPath:startPath)); //errgh but no compiler error ....
	}
}