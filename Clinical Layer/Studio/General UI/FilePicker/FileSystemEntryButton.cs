﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;

public class FileSystemEntryButton : MonoBehaviour {

	public Text filenameText;
	public UnityEngine.UI.Image icon;

	public FileSystemInfo thisInfo;
	private SimpleFileBrowser browser;

	public bool isFolder = false;

	public Sprite folderSprite;
	public Sprite bvhSprite;
	public Sprite clinicalSprite;
	public Sprite otherSprite;

	public MovePreviewPresenter attachedPreviewer { get; private set; }

	private ScrollRect parentScrollRect {
		get {
			if(m_parentScrollRect == null) {
				m_parentScrollRect = GetComponentInParent<ScrollRect>();
			}
			return m_parentScrollRect;
		}
	}
	private ScrollRect m_parentScrollRect;

	public void Set(FileSystemInfo inInfo, SimpleFileBrowser parentBrowser) {
		thisInfo = inInfo;
		browser = parentBrowser;

		filenameText.text = thisInfo.Name;

		if ((thisInfo.Attributes & FileAttributes.Directory) == FileAttributes.Directory) {
			isFolder = true;
			icon.sprite = folderSprite;
		}
		else {
			isFolder = false;
			if(thisInfo.Extension == ".bvh") {
				icon.sprite = bvhSprite;
			}
			else if(thisInfo.Extension == ".clinicalMove") {
				icon.sprite = clinicalSprite;
				CreateAnimatedThumb();
			}
			else {
				icon.sprite = otherSprite;
			}
		}
	}

	void Update() {
		if(attachedPreviewer == null || attachedPreviewer.loadFailed)return;
		attachedPreviewer.updating = CheckIfVisible();
	}

	bool CheckIfVisible() {
		Canvas canvas = GetComponent<Graphic>().canvas;
		return RectTransformUtility.RectangleContainsScreenPoint((parentScrollRect.transform as RectTransform),transform.position,canvas.worldCamera);
	}
		
	public void Selected() {
		browser.EntrySelected (this);
	}

	void OnDestroy() {
		if(attachedPreviewer)attachedPreviewer.Destroy();
	}

	void CreateAnimatedThumb ()
	{
		attachedPreviewer = MovePreviewPresenter.Create(thisInfo.FullName);
		//if(attachedPreviewer.loadFailed)return;

		GameObject iconGO = icon.gameObject;
		DestroyImmediate(icon);
		RawImage rawImage = iconGO.AddComponent<RawImage>();
		rawImage.texture = attachedPreviewer.renderTex;

//		icon.material = new Material(Shader.Find("Standard"));
//		icon.materialForRendering.mainTexture = attachedPreviewer.renderTex;
	}
}
