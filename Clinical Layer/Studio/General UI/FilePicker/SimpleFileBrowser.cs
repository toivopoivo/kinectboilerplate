﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

public class SimpleFileBrowser : MonoBehaviour {

	public string[] acceptedExtensions;

	public string initialPath;
	public string currentPath {
		set {
			m_currentPath = value;
			currentPathText.text = value;
		}
		get {
			return m_currentPath;
		}
	}
	private string m_currentPath;

	public Button confirmSelectionButton;

	public Text currentPathText;
	public FileSystemEntryButton entryPrefab;
	public Transform entrybuttonsParent;
	public List<FileSystemEntryButton> entryButtons = new List<FileSystemEntryButton>();

	public InputField fileNameField;

	public Text confirmButtonText;

	public RawImage previewImagePresenter;

	public FileSystemEntryButton selection {
		get {return m_selection;}
		set {
			if(m_selection)Destroy(m_selection.gameObject.GetComponent<Outline>());
			m_selection = value;
			if(value != null) {
				m_selection.gameObject.AddComponent<Outline>();
				fileNameField.text = selection.thisInfo.Name;
				confirmSelectionButton.interactable = true;
			}
			else if(!creatingNew) {
				fileNameField.text = "Please select a file of type "+AcceptedExtensionsVerbose();
				confirmSelectionButton.interactable = false;
			}
		}
	}
	private FileSystemEntryButton m_selection;

	public bool creatingNew {
		get { return m_creatingNew; }
		set {
			m_creatingNew = value;
			fileNameField.interactable = m_creatingNew;
			if(creatingNew)confirmButtonText.text = "Save";
			else confirmButtonText.text = "Open selection";
		}
	}
	private bool m_creatingNew = false;

	public string givenPath {
		get {
			return Path.Combine(currentPath,fileNameField.text);
		}
	}

	public bool selectionConfirmed = false;
	public bool selectingCancelled = false;

	//public selectedButton

	//Debug
	void Update() {
		if(Input.GetKeyDown(KeyCode.G)) {
			//currentPath = @"H:\Omat tiedostot\Google Drive\Hiihtopeli\Kinect";
			currentPath = @"C:\Users\Max\Google Drive\Hiihtopeli\Kinect";
			Refresh(false);
		}
	}

	// Use this for initialization
	void Start () {
		Init ();
	}

	public void Init() {
		print("PERSISTENT DATA PATH:"+Application.persistentDataPath);

		//First opened: destroy placeholders
		//entrybuttonsParent.DestroyChildren ();
		if(entryButtons.Count == 0) {
			while(entrybuttonsParent.childCount>0) {
				DestroyImmediate(entrybuttonsParent.GetChild(0).gameObject);
			}
		}
		
		//currentPath = initialPath;
		//if(Application.isEditor)currentPath = Utils.defaultAssetsRootPath;
		//else currentPath = Application.persistentDataPath;//comment

		//currentPath = Utils.appFilesPath;
		//currentPath = Utils.appFilesPath+"/TESTABLE MOVES/Final/Visual";
		
		Refresh (false);

		if(previewImagePresenter)previewImagePresenter.gameObject.SetActive(false);
	}

	public void Refresh (bool showDrivesInstead) {
		Clear ();
		DisablePreview();

		FileSystemInfo[] infos;

		if(!showDrivesInstead) {
			infos = new DirectoryInfo (currentPath).GetFileSystemInfos ();
		}
		else {
			//DriveInfo[] driveInfos = DriveInfo.GetDrives();
			//infos = new FileSystemInfo[driveInfos.Length];

			string[] drives = Directory.GetLogicalDrives();
			infos = new FileSystemInfo[drives.Length];

			for (int i = 0; i < infos.Length; i++) {
				//infos[i] = new DirectoryInfo(driveInfos[i].Name);
				infos[i] = new DirectoryInfo(drives[i]);
			}
		}

		foreach(FileSystemInfo info in infos) {
			if(info.Extension == ".meta")continue;
			FileSystemEntryButton entryButton = Instantiate(entryPrefab) as FileSystemEntryButton;
			entryButton.transform.SetParent(entrybuttonsParent,false);
			entryButtons.Add(entryButton);
			entryButton.Set(info,this);
			//entryButton.transform.parent = entrybuttonsParent;
		}
	}

	void Clear() {
		while(entryButtons.Count > 0) {
			DestroyImmediate(entryButtons[0].gameObject);
			entryButtons.RemoveAt(0);
		}
		selection = null;
	}

	public void UpAFolder() {
		DirectoryInfo currentPathDirectoryInfo = new DirectoryInfo (currentPath);
		if(currentPathDirectoryInfo.Parent == null) {
			print("root!");
			Refresh(true);
		}
		else {
			currentPath = currentPathDirectoryInfo.Parent.FullName;
			Refresh (false);
		}
	}

	public void EntrySelected (FileSystemEntryButton callingEntry)	{
		if(callingEntry.isFolder) {
			currentPath = callingEntry.thisInfo.FullName;
			Refresh(false);

			if(acceptedExtensions.Contains("FOLDER")) {
				selection = callingEntry;
				confirmSelectionButton.interactable = true;

				print("Selected folder:"+callingEntry.thisInfo.FullName);
			}

		}
		else {
			if(acceptedExtensions.Contains(callingEntry.thisInfo.Extension)) {
				//Selected
				selection = callingEntry;
				print("Selected:"+callingEntry.thisInfo.FullName);

				PreviewSelected();
			}
			else {
				//Invalid selection
				selection = null;
				print("Not the right kind of file");
				AlertBox.Show(callingEntry.thisInfo.Name+" is not the right kind of file. Only files with the extension(s) "+AcceptedExtensionsVerbose()+ " are accepted.");
			}
		}
	}

	void DisablePreview() {
		previewImagePresenter.gameObject.SetActive(false);
	}

	//TODO: set old preview render textures back to smaller size maybe
	void PreviewSelected ()
	{
		if(!previewImagePresenter)return;

		if(selection.attachedPreviewer != null) {
			previewImagePresenter.gameObject.SetActive(true);

			//TODO get out of here's dolan (this code shouldn't really be here)
			selection.attachedPreviewer.renderTex.Release();
			selection.attachedPreviewer.renderTex = new RenderTexture(512,512,16);
			selection.attachedPreviewer.cam.targetTexture = selection.attachedPreviewer.renderTex;
			selection.GetComponentInChildren<RawImage>().texture = selection.attachedPreviewer.renderTex;

			previewImagePresenter.texture = selection.attachedPreviewer.renderTex;
		}
		else {
			DisablePreview();
		}
	}

	string AcceptedExtensionsVerbose() {
		StringBuilder sb = new StringBuilder (" ");
		for (int i = 0; i < acceptedExtensions.Length; i++) {
			sb.Append(acceptedExtensions[i]);
			if(i != acceptedExtensions.Length - 1)sb.Append(", ");
		}		
		return sb.ToString ();
	}

	public void FileNameFieldAltered() {
		FileInfo proposedFile;
		try {
			proposedFile = new FileInfo (fileNameField.text);
		}
		catch {
			confirmSelectionButton.interactable = false;
			print("sdiofjs");
			return;
		}
		if(acceptedExtensions.Contains(proposedFile.Extension)) {
			confirmSelectionButton.interactable = true;
		}
		else {
			//confirmSelectionButton.interactable = false;
			fileNameField.text += acceptedExtensions[0];
		}
	}

	//Called from open/save button
	public void ConfirmSelection() {
		if(creatingNew) {
			if(!FileNameIsValid(fileNameField.text)) {
				AlertBox.Show("Filename contains illegal characters! Not saving.\nAll illegal characters:\n"+new string(System.IO.Path.GetInvalidFileNameChars()));
				return;
			}
			else if(File.Exists(givenPath)) {
				StartCoroutine(ConfirmOverwrite());
			}
			else selectionConfirmed = true;
		}
		else selectionConfirmed = true;
	}

	IEnumerator ConfirmOverwrite ()	{
		yield return StartCoroutine(AlertBox.instance.ShowAndWait("\""+fileNameField.text+"\" already exists. Overwrite?","Yes","Cancel"));
		if(AlertBox.instance.lastResponse == "Yes")selectionConfirmed = true;
	}

	public void CancelSelecting() {
		selectingCancelled = true;
	}


	public static string SanitizeFileName (string filename) {
		//filename = filename.Replace (" ", "_");
		foreach( char invalidchar in System.IO.Path.GetInvalidFileNameChars()) {
			//Debug.Log(invalidchar.ToString());
			filename = filename.Replace(invalidchar.ToString(), string.Empty);
		}
		return filename;
	}
	
	public static bool FileNameIsValid (string filename) {
		foreach( char invalidchar in System.IO.Path.GetInvalidFileNameChars()) {
			if(filename.Contains(invalidchar.ToString())) return false;
		}
		return true;
	}
}