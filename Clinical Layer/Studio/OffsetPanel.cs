﻿using UnityEngine;
using System.Collections;

public class OffsetPanel : MonoBehaviour {

	public Vector3 offset = Vector3.zero;

	public void XSet(float setting) {
		offset.x = setting;
		ClinicalMovePropertiesControl.instance.SetPosOffset (offset);
	}

	public void YSet(float setting) {
		offset.y = setting;
		ClinicalMovePropertiesControl.instance.SetPosOffset (offset);
	}

	public void ZSet(float setting) {
		offset.z = setting;
		ClinicalMovePropertiesControl.instance.SetPosOffset (offset);
	}
}