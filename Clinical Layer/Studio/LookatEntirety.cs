﻿using UnityEngine;
using System.Collections;

public class LookatEntirety : MonoBehaviour {

	public Transform targetParent;
	public float damping = 3f;

	public bool changeFov = true;
	public float fovDamping = 1;
	public float fovFactor = 1;

	// Use this for initialization
	void Start () {
		//Snap();
	}

	void Snap() {
		Vector3 centerPoint = GetCombinedRendererBounds (targetParent).center;
		Quaternion rotation = Quaternion.LookRotation(centerPoint - transform.position);
		transform.rotation = rotation;
	}
	
	// Update is called once per frame
	void Update () {
		Camera cam = GetComponent<Camera>();
		if(cam == null || cam.enabled == false) {
			if(CameraChanger.instance.theCamera.transform.parent == transform) {
				cam = Camera.main;
			}
			else return;
		}

		Bounds combinedBounds = GetCombinedRendererBounds (targetParent);
		Vector3 centerPoint = combinedBounds.center;
		Quaternion rotation = Quaternion.LookRotation(centerPoint - transform.position);
		transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * damping);

		if(changeFov) {
			float fovMod = combinedBounds.size.magnitude / Vector3.Distance(transform.position,centerPoint);
			//print(fovMod);
			if(combinedBounds.size.magnitude < 0.001f)fovMod = 1;

			cam.fieldOfView = Mathf.Lerp(cam.fieldOfView,fovMod * 40 * fovFactor,Time.deltaTime * fovDamping);
			//camera.fieldOfView = Mathf.Lerp(15,90,fovMod);
		}

		//var fovMod = Mathf.InverseLerp(17,5,Vector3.Distance(transform.position,centerPoint));		
		//print(fovMod);		
		//if(changeFov)camera.fieldOfView = Mathf.Lerp(15,90,fovMod);
	}

	void OnDrawGizmosSelected() {
		Bounds bounds = GetCombinedRendererBounds(targetParent);
		Gizmos.DrawWireCube(bounds.center, bounds.size);
	}

	public static Bounds GetCombinedRendererBounds(Transform parent) {
		Bounds combinedBounds = new Bounds();
		//combinedBounds.center = parent.position;
		Renderer[] childRenderers = parent.GetComponentsInChildren<Renderer>();

		Vector3 temp = Vector3.zero;
		foreach (Renderer childRenderer in childRenderers) {
			temp += childRenderer.transform.position;
		}
		combinedBounds.center = temp * ((1 / (float)childRenderers.Length));

		foreach (Renderer childRenderer in childRenderers) {
		/* toivo 26.1.2019 no such thing as particle renderer
         * if(childRenderer.GetType() == typeof(ParticleRenderer))continue; //skip particle renderers as they're random*/
			if(!childRenderer.enabled)continue;
			//if(childRenderer.GetType() != typeof(SkinnedMeshRenderer))continue; //HACK HACK TODO remember!
			combinedBounds.Encapsulate(childRenderer.bounds);
		}

		//print (combinedBounds.center);

		return combinedBounds;
	}
}
