﻿using UnityEngine;
using System.Collections;
using ClinicalLayer;
using System.Threading;

public class MovePreviewPresenter : MonoBehaviour {

	public MoveRepeater repeater;
	public Camera cam;

	[HideInInspector]
	public RenderTexture renderTex;

	[HideInInspector]
	public bool loadFailed;

	private string pathTomove;
	private ClinicalMove loadedMove = null;
	private bool moveIsLoaded = false;

	public bool updating {
		set {
			if(m_updating == value)return;
			m_updating = value;
			if(repeater == null || cam == null)return;
			repeater.gameObject.SetActive(value);
			cam.enabled = value;
		}
		get { return m_updating; }
	}
	private bool m_updating = true;

	private static int instancesCreated = 0;
	private static bool dontLoadNewThisFrame = false;


	public static MovePreviewPresenter Create(string pathToMove) {
		GameObject root = new GameObject("Movepreviewpresenterrroot");
		root.transform.Translate(500,10f * (float)instancesCreated,0);
		instancesCreated++;
		MovePreviewPresenter presenter = root.AddComponent<MovePreviewPresenter>();
		presenter.StartCoroutine(presenter.LazyInit(pathToMove));

		return presenter;
	}

	IEnumerator LazyInit(string inPath) {
		pathTomove = inPath;
		renderTex = new RenderTexture(256,256,16);

		//wait until we actually want to load this
		while(dontLoadNewThisFrame || !updating) {
			yield return null;
		}
		dontLoadNewThisFrame = true;

		//Instantiate rest of stuff
		GameObject rest = Instantiate(Resources.Load("movePreviewPresenter")) as GameObject;
		rest.transform.parent = transform;
		rest.transform.localPosition = Vector3.zero;

		//Assign new stuff
		cam = GetComponentInChildren<Camera>();
		repeater = GetComponentInChildren<MoveRepeater>();

		cam.targetTexture = renderTex;

		//Start loading the ClinicalMove in another thread
		Thread loadingThread = new Thread(MoveLoader);
		loadingThread.Start();

		//wait while loading thread runs
		while(!moveIsLoaded && !loadFailed) {
			yield return null;
		}
		if (moveIsLoaded) {
			repeater.SetMoveToPlay (loadedMove);
			repeater.Play ();
		}

		//Let the application run freely for a small while before loading another preview is allowed
		yield return new WaitForSeconds(0.2f);
		dontLoadNewThisFrame = false;
	}

	void MoveLoader() {
		try {
			loadedMove = ClinicalMove.CreateFromJson(System.IO.File.ReadAllText(pathTomove));
			moveIsLoaded = true;
		}
		catch {
			loadFailed = true;
		}
		//repeater.SetMoveToPlay(ClinicalMove.CreateFromJson(System.IO.File.ReadAllText(pathToMove)));
	}

	public void Destroy() {
		Destroy(renderTex);
		Destroy(gameObject);
		dontLoadNewThisFrame = false;
	}
}
