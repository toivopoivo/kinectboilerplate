using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;
using ClinicalLayer;

public class StudioControl : MonoBehaviour {

	public MoveRecorder recorder;
	public MoveRepeatTracker moveTracker;

	public ModelControllerFollow loneChar;
	public ModelControllerFollow pairCharOne;
	public ModelControllerFollow pairCharTwo;

	public Text recorderNotifText;

	public GameObject mainMenuUI;
	public GameObject newAnimationUI;
	public GameObject recordingUI;
	public GameObject propertiesUI; //TODO: maybe use singleton instance.gameObject instead?
	public GameObject moveTrackingUI;

	public GameObject qualityPanel;

	private bool recordingStopRequested = false;

	void Start() {
		//Utils.GoToCopyFilesIfNeeded();

		FileInfo test = new FileInfo (@"C:/Directory/file with spaces in name.txt");
		print (test.Name);
		print (test.DirectoryName);

		/*print (System.IO.Path.GetInvalidFileNameChars ().Length);
		print (System.IO.Path.GetInvalidFileNameChars ()[3].ToString());
		print(new string (System.IO.Path.GetInvalidFileNameChars (),0,System.IO.Path.GetInvalidFileNameChars ().Length));*/

		/* //TODO: uncomment at some point, useful on windows maybe?
		string[] args = System.Environment.GetCommandLineArgs ();
		foreach (string arg in args) {
			print(arg);
		}

		//Open externally pointed file
		if(args.Length > 1) {
			FileInfo info = new FileInfo(args[1]);
			switch(info.Extension) {
			case ".clinicalMove":
			case ".move":
				mainMenuUI.SendMessage ("Hide");
				StartCoroutine(OpenPointed(info.FullName));
				break;
			case ".bvh":
			case ".BVH":
				mainMenuUI.SendMessage ("Hide");
				StartCoroutine(ImportPointed(info.FullName));
				break;
			default:
				AlertBox.Show("Cannot open unsupported file:\n"+info.FullName);
				break;
			}
		}
		*/
	}

	public void ImportButtonPressed() {
		newAnimationUI.SendMessage ("Hide");
		StartCoroutine (SelectBVHAndImport ());
	}

	public void RecordWithKinectButtonPressed() {
		newAnimationUI.SendMessage ("Hide");
		StartCoroutine (RecordAndSave());
	}

	public void StopRecordingButtonPressed() {
		recordingStopRequested = true;
	}

	public void SaveButtonPressed() {
		StartCoroutine (SaveMoveWithPrompt ());
	}

	public void OpenExistingButtonPressed() {
		StartCoroutine (PromptAndOpenMove());
	}

	public void BackFromPropertiesPressed() {
		//pressed back while in properties screen (possibly show alert, unsaved changes?)
		StartCoroutine(VerifyAndGoBackToMain());
	}

	public void CancelRecordingButtonPressed() {
		recorder.AbandonRecording ();
		recordingUI.SendMessage ("Hide");
		mainMenuUI.SendMessage ("Show");
		CameraChanger.instance.MoveCameraTo (StudioCameraPosition.Default);
	}

	public void TestMoveButtonPressed() {
		propertiesUI.SendMessage ("Hide");
		moveTrackingUI.SendMessage ("Show");

		pairCharOne.driving = true;
		pairCharTwo.driving = true;
		pairCharOne.currentFollowMode = FollowMode.Live;
		pairCharTwo.currentFollowMode = FollowMode.Recording;
		pairCharTwo.GetComponent<MoveRepeater> ().SetMoveToPlay (ClinicalMovePropertiesControl.instance.currentlyOpenedMove);
		pairCharTwo.GetComponent<MoveRepeater> ().Play ();
		
		moveTracker.StartTrackingMove(ClinicalMovePropertiesControl.instance.currentlyOpenedMove);
		CameraChanger.instance.MoveCameraTo (StudioCameraPosition.TwoChars);
	}

	public void BackToPropertiesFromTesting() {
		pairCharOne.driving = false;
		pairCharTwo.driving = false;
		moveTracker.StopTracking ();
		pairCharTwo.GetComponent<MoveRepeater> ().Stop ();

		moveTrackingUI.SendMessage ("Hide");
		propertiesUI.SendMessage ("Show");
		CameraChanger.instance.MoveCameraTo (StudioCameraPosition.OneChar);
		moveTracker.StopTracking ();
	}


	IEnumerator VerifyAndGoBackToMain() {
		if(ClinicalMovePropertiesControl.instance.dirtyState) {
			string[] responses = {"Cancel","Go to main menu"};
			yield return StartCoroutine(AlertBox.instance.ShowAndWait("Go back to main menu? Unsaved work will be lost.",responses,false));
			if(AlertBox.instance.lastResponse == responses[0]) yield break;
		}

		propertiesUI.SendMessage("Hide");
		mainMenuUI.SendMessage("Show");
		CameraChanger.instance.MoveCameraTo (StudioCameraPosition.Default);
		yield return null;
	}

	IEnumerator SaveMoveWithPrompt () {
		if(ClinicalMovePropertiesControl.instance.currentlyOpenedMove.moveName == "") {
			AlertBox.Show("Move name cannot be blank!");
			yield break;
		}
		//TODO NEXT:Prompt for folder to save to, do interrupts if problems
		FileInfo proposedFileInfo;
		if(ClinicalMovePropertiesControl.instance.pathOfOpenFile == "") {
			//If file is new, propose root folder for saved moves and move name as filename
			string sanitizedFileName = SimpleFileBrowser.SanitizeFileName(ClinicalMovePropertiesControl.instance.currentlyOpenedMove.moveName+".clinicalMove");
			proposedFileInfo = new FileInfo(Utils.savedMovesFolder+"/"+sanitizedFileName);
			print(proposedFileInfo.FullName);
			print(proposedFileInfo.DirectoryName);
			print(proposedFileInfo.Name);
		}
		else {
			//If file is not new, propose old path and file name
			//If the opened file uses the old binary-only format, replace the extension with the new extension
			string fileName = ClinicalMovePropertiesControl.instance.pathOfOpenFile;
			if(fileName.EndsWith(".move")) {
				fileName = fileName.Replace(".move",".clinicalMove");
				print("sneakily changed file extension from old to new while keeping rest of filename intact");
			}
			print(fileName);
			proposedFileInfo = new FileInfo(fileName);
		}
		qualityPanel.SendMessage("Show");

		yield return StartCoroutine (FileBrowserManager.instance.PromptForFile(".clinicalMove",true, defaultGivenFileName:proposedFileInfo.Name, startPath:proposedFileInfo.DirectoryName));
		if(FileBrowserManager.instance.lastCancelled) {
			qualityPanel.SendMessage("Hide");
			yield break;
		}
		string saveToFolder = FileBrowserManager.instance.browser.givenPath;
		ClinicalMovePropertiesControl.instance.SaveCurrentMove (saveToFolder);
		AlertBox.Show ("Move saved successfully.");
		qualityPanel.SendMessage("Hide");

		StartCoroutine(OpenPointed(FileBrowserManager.instance.browser.givenPath));
	}

	IEnumerator PromptAndOpenMove () {
		mainMenuUI.SendMessage ("Hide");
		yield return StartCoroutine(FileBrowserManager.instance.PromptForEntry((new string[] {".clinicalMove",".move"}),false));

		bool cancelled = FileBrowserManager.instance.lastCancelled;
		if(cancelled) {
			mainMenuUI.SendMessage("Show");
			yield break;
		}
		yield return StartCoroutine (OpenPointed (FileBrowserManager.instance.lastSelection.FullName));
	}

	IEnumerator OpenPointed(string path) {
		mainMenuUI.SendMessage ("Hide");
        yield return StartCoroutine(ClinicalMovePropertiesControl.instance.OpenFile (path));

        if (ClinicalMovePropertiesControl.instance.lastOpeningSuccessful == false) {
			mainMenuUI.SendMessage("Show");
			yield break;
		}
		loneChar.currentFollowMode = FollowMode.Recording;		
		yield return StartCoroutine (CameraChanger.instance.MoveCameraToAndReturn (StudioCameraPosition.OneChar));		
		propertiesUI.SendMessage ("Show");
	}

	IEnumerator SelectBVHAndImport () {
		yield return StartCoroutine(FileBrowserManager.instance.PromptForFile(".bvh",false));
		if(FileBrowserManager.instance.lastCancelled) {
			mainMenuUI.SendMessage("Show");
			yield break;
		}		
		StartCoroutine(ImportPointed(FileBrowserManager.instance.lastSelection.FullName));
	}

	IEnumerator ImportPointed(string path) {
		mainMenuUI.SendMessage ("Hide");
		try {
			ClinicalMove importedMove = BVHImporter.ImportAndConvertToMove (path);
			ClinicalMovePropertiesControl.instance.SetMove (importedMove, unSaved:true);
		}
		catch(System.Exception e) {
			AlertBox.Show("Failed to import BVH file \n"+path+"\n\nError:"+e.Message+"\n"+e.StackTrace);
			mainMenuUI.SendMessage("Show");
			yield break;
		}
		loneChar.currentFollowMode = FollowMode.Recording;

		BVHComparisonController.instance.JustImported ();

		yield return StartCoroutine (CameraChanger.instance.MoveCameraToAndReturn (StudioCameraPosition.OneChar));
		propertiesUI.SendMessage ("Show");
	}

	IEnumerator RecordAndSave () {
		yield return StartCoroutine(CameraChanger.instance.MoveCameraToAndReturn(StudioCameraPosition.OneChar));

		recordingUI.SendMessage ("Show");

		recorder.currentRecordingSourceMode = RecordingSourceMode.FromLocal;
		loneChar.currentFollowMode = FollowMode.Live;

		loneChar.isTracking = false;
		loneChar.currentBodyOrientationFrame.isTracking = false; //a bit chewgum-y, do some changes to make this unnecesssary?

		while(!loneChar.isTracking) {
			recorderNotifText.text = "Go in front of the kinect to start recording.";
			yield return null;
		}

		float recorderDelay = 3;
		while(recorderDelay > 0) {
			recorderDelay -= Time.deltaTime;
			recorderNotifText.text = "Recording will start in "+System.Math.Round(recorderDelay,2).ToString("F2")+" seconds!";
			yield return null;
		}

		recorder.StartRecording ("Recorded move");
		recorderNotifText.text = "RECORDING";

		while(!recordingStopRequested) {
			//Recording ongoing, continue until stop button pressed
			yield return null;
		}
		recordingStopRequested = false;
		recorder.StopRecording ();
		recordingUI.SendMessage ("Hide");

		ClinicalMovePropertiesControl.instance.SetMove (recorder.currentlyRecording,unSaved:true);
		propertiesUI.SendMessage ("Show");

		loneChar.currentFollowMode = FollowMode.Recording;
	}
}