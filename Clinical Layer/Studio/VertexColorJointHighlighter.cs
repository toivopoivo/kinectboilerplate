﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class WeightJoint {
	public List<AffectedVert> affectedVerts = new List<AffectedVert>();
	public AffectedVert[] affectedVertsArr;
}

public struct AffectedVert {
	public SkinnedMeshRenderer belongsToRenderer;
	public int vertexIndex;
	public float weight;
}

public class VertexColorJointHighlighter : MonoBehaviour {

	//drag-dropped manually, actual rig bones
	public Transform[] rigBonesPerKinect;

	private WeightJoint[] weightJoints = new WeightJoint[25];

	private SkinnedMeshRenderer[] rendas;

	public Toggle onOffCheckbox;

	void Start() {
		rendas = GetComponentsInChildren<SkinnedMeshRenderer> ();

		int[] rigBoneIndexes = new int[25];


		for (int i = 0; i < weightJoints.Length; i++) {
			weightJoints[i] = new WeightJoint();
			foreach (SkinnedMeshRenderer r in rendas) {
				Mesh usedMesh = r.sharedMesh;
				BoneWeight[] weights = usedMesh.boneWeights;
				Transform[] bonesUsedByThisMesh = r.bones;
				//print(weights.Length);
				for (int j = 0; j < weights.Length; j++) {
					if(bonesUsedByThisMesh[weights[j].boneIndex0] == rigBonesPerKinect[i]) {
						AffectedVert afVert = new AffectedVert();
						afVert.belongsToRenderer = r;
						afVert.vertexIndex = j;
						afVert.weight = weights[j].weight0; //kind of random
						weightJoints[i].affectedVerts.Add(afVert);
					}
				}				
			}
			weightJoints[i].affectedVertsArr = weightJoints[i].affectedVerts.ToArray();
		}
	}

	void Update() {
		/*float[] testArr = new float[25];
		for (int i = 0; i < 25; i++) {
			testArr[i] = 0f;
			if(i != (int)Time.time / 2)continue;
			print((Windows.Kinect.JointType)i);
			//testArr[i] = Mathf.Sin(Time.time*15);
			testArr[i] = 1f;
		}

		DisplayJointCompliance (testArr);*/
	}	

	public void DisplayJointCompliance(float[] jointComplyValuesPerJointType) {
		if(!onOffCheckbox.isOn)return;
		//(NO)find all joints that affect vertex x, then find the middle color between what the joints want	
		//Go through joints, add the compliance times weight color to their vertex color

		/*for (int i = 0; i < 25; i++) {
			//print(weightJoints[i].affectedVerts.Count);
			foreach(AffectedVert vert in weightJoints[i].affectedVerts) {
				Color[] colors = vert.belongsToRenderer.sharedMesh.colors;
				colors[vert.vertexIndex] = Color.Lerp(Color.white,Color.red,Random.value);
				vert.belongsToRenderer.sharedMesh.colors = colors;
				//vert.belongsToRenderer.sharedMesh.colors[vert.vertexIndex] = Color.Lerp(Color.white,Color.red,Random.value);
			}
		}*/
		//int foundCount = 0;
		//print (jointComplyValuesPerJointType.Length);
		foreach (SkinnedMeshRenderer rend in rendas) {
			Color[] colors = rend.sharedMesh.colors;
			if(colors.Length == 0)colors = new Color[rend.sharedMesh.vertexCount];

			for (int i = 0; i < 25; i++) {			
			//print(weightJoints[i].affectedVerts.Count);
				foreach(AffectedVert vert in weightJoints[i].affectedVertsArr) {
					if(vert.belongsToRenderer == rend) {
						//colors[vert.vertexIndex] = Color.Lerp(Color.white,Color.green,(int)Time.time % 2);
						//foundCount++;
						if(jointComplyValuesPerJointType[i] < -500f)colors[vert.vertexIndex] = Color.grey;
						else colors[vert.vertexIndex] = Color.Lerp(Color.black,Color.green,jointComplyValuesPerJointType[i]);
					}
				}
			}

			rend.sharedMesh.colors = colors;
		}
		//this.enabled = false;
		//print (foundCount);
	}
}
