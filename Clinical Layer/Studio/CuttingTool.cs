﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using ClinicalLayer;

public class CuttingTool : MonoBehaviour {

	public MoveRepeater movePlayer {
		get {
			return ClinicalMovePropertiesControl.instance.playBackCharacter;
		}
	}

	public RectTransform visibleTimeLine;
	public RectTransform selectedAreaHighlighter;

	public Slider startPointSlider;
	public Slider endPointSlider;
	public Slider currentPointSlider;

	//public Image checkerSprite;
	public RawImage checkerSprite;

	public float minFrame;
	public float maxFrame;

	public float frameCount;

	public float activeStartFrame;
	public float activeEndFrame;

	public Text startFrameNumberText;
	public Text endFrameNumberText;
	public Text selelctedAreaLenghtText;

	private float framePixelWidth = 8;

	public RectTransform scalableRoot;
	public RectTransform scrollAreaContainer;

	public Transform[] preserveGlobalScaleObjects;
	private Vector3[] origScales;

	private float zoom = 1;
	private float lastZoom = 1;

	public float scrollCounterMod = 0;

	public float selectedAreaLenght {
		get {
			return activeEndFrame - activeStartFrame;
		}
	}

	void Start() {
		if(Time.time<0.2f) Init(80); //debug

		origScales = new Vector3[preserveGlobalScaleObjects.Length];
		for (int i = 0; i < origScales.Length; i++) {
			origScales[i] = preserveGlobalScaleObjects[i].localScale;
		}
	}

	void Update() {
		//Zooming
		float zoomMod = 0f;
		zoomMod += Input.GetAxis ("Mouse ScrollWheel") * 3f;
		zoomMod += Input.GetAxis ("Vertical") * 0.1f;

		zoom = zoom * (zoomMod / 10 + 1);
		
		float mouseX = Input.mousePosition.x;
		//float lineStart = (float)scalableRoot.rect.x;
		//float lineEnd = lineStart + (float)scalableRoot.rect.width;
		Vector3[] corners = new Vector3[4];
		scalableRoot.GetWorldCorners(corners);
		float lineStart = corners[0].x;
		float lineEnd = corners[3].x;
		
		//print ("START"+lineStart);
		//print ("END"+lineEnd);
		
		float prog = Mathf.InverseLerp (lineStart, lineEnd, mouseX);
		//print (prog);
		
		//scrollAreaContainer.GetComponent<ScrollRect> ().horizontalNormalizedPosition += zoomMod * ((1 / scalableRoot.localScale.x) * (prog - 0.5f));
		//if(Mathf.Abs(zoomMod) > 0.01f)scrollAreaContainer.GetComponent<ScrollRect> ().horizontalNormalizedPosition += (1 / zoom) * (prog - 0.5f);
		
		//Amount to move if going from 0.5x to 1x and mouse in right corner and 2x view size scroll area:
		//Center was at 0.5, now needs to be at 0.75
		//Move 0.25 right
		
		//Amount to move if going from 4x to 2x and mouse in right corner and 1x view size scroll area:
		//Center was at 0.875, now needs to be at 0.75
		//Move 0.125 left
		
		//Calculation: change / 2 ? nope
		
		//Amount to move when zooming in (scale rising): the width of the part that would otherwise be now hidden if zooming from center
		//1000px total width, container 500px
		//0.5 to 1: on both sides, 250px gets hidden
		//Need to move right 250px
		
		//4000px total width, container 1000px
		//1 to 0.25: if staying on right side, can see 3000px more
		//Moved from 0.85 to 0.5 = 0,375 = 1500
		//1500 = 0,375 * 4000
		//left edge: 0.75 to 0
		
		//scale inverse 4 to 1
		//Calculation: 
		/*float zoomDelta = (1 / lastZoom) - (1 / zoom); //1 to 4, 3
		float shownWidthDelta = zoomDelta * scalableRoot.rect.width; //3 * (0.25 * totalwidth) = 3000
		float pixelsToMove = shownWidthDelta * (prog - 0.5f) * 1f; //1500*/

		/*float totalWidth = scalableRoot.rect.width;
		float visibleWidth = totalWidth * (1 / zoom); //1000 * (0.5 / 1) = 500
		float visibleWidthLast = totalWidth * (1 / lastZoom); //1000 * (1 / 1) = 1000
		float visibleWidthDelta = visibleWidth - visibleWidthLast; // = 1000

		float pixelsToMove = visibleWidthDelta * 0.5f;*/

		/*float totalWidth = scalableRoot.rect.width;
		float halfWidth = scalableRoot.rect.width / 2;
		float posOnTrack = prog - 0.5f;
		float zoomDelta = (1 / zoom) - (1 / lastZoom);
		float pixelsToMove = -posOnTrack * zoomDelta * totalWidth;
		if(Mathf.Abs(pixelsToMove)>0.01f)print (pixelsToMove);		

		pixelsToMove += Input.GetAxis ("Mouse ScrollWheel") * scrollCounterMod; //avoid scrolling the timeline normally TODO:fix this, it isn't pixel based
		//scrollAreaContainer.GetComponent<ScrollRect> ().horizontalNormalizedPosition += pixelsToMove / scalableRoot.rect.width;*/

		scalableRoot.localScale = new Vector2 (zoom,scalableRoot.localScale.y);
		//scalableRoot.SetSizeWithCurrentAnchors (RectTransform.Axis.Horizontal ,scrollAreaContainer.rect.width * zoom);

		//Vector3 sgale = scalableRoot.localScale;
		//scalableRoot.localScale = Vector3.one;
		if(Mathf.Abs(zoomMod) > 0.05f) {
			Vector2 prevPivot = scalableRoot.pivot;

			//float pivotDelta = scalableRoot.pivot.x - prevPivot.x;
			scalableRoot.localPosition = new Vector2 (scalableRoot.rect.width * (prog - 0.5f),scalableRoot.localPosition.y);
			//print(scalableRoot.localPosition.x);
			scalableRoot.pivot = new Vector2 (prog, scalableRoot.pivot.y);
			//scalableRoot.Translate (scalableRoot.rect.width * pivotDelta, 0, 0, Space.Self);
		}
		//scalableRoot.localScale = sgale;


		if(origScales == null)Start();
		for (int i = 0; i < origScales.Length; i++) {
			preserveGlobalScaleObjects[i].localScale = new Vector2(origScales[i].x / scalableRoot.lossyScale.x, origScales[i].y / scalableRoot.lossyScale.y);
		}

		lastZoom = zoom;
	}

	public void Init (int length) {
		print ("Cutting tool: Initializing, frame count:"+length);

		minFrame = 0;
		maxFrame = length;
		frameCount = length;

		/*SetEndFrame(length);
		SetStartFrame(0);
		UpdateMiddlePartVizualization();*/
		
		//set checker
		//TODO: USE TILED IMAGE INTEAD FOR CHECKER //TODO: NO DONT USE USE RAW IMAGE BECAUSE TILED SPRITE IS FUCKING BROKEN
		visibleTimeLine.SetSizeWithCurrentAnchors (RectTransform.Axis.Horizontal, ((length) * framePixelWidth)-1);
		checkerSprite.uvRect = new Rect(0,0,frameCount / 2,1);

		startPointSlider.maxValue = length-1;
		endPointSlider.maxValue = length-1;
		currentPointSlider.maxValue = length-1;
		//startPointSlider.rectTransform.SetSizeWithCurrentAnchors (RectTransform.Axis.Horizontal, length * framePixelWidth);

		startPointSlider.value = minFrame;
		endPointSlider.value = maxFrame;
		currentPointSlider.value = minFrame;

		//CurrentMarkerMoved (0.1f);
		//CurrentMarkerMoved (0);
		StartMarkerMoved (0);
		StartMarkerMoved (0.1f);
	}

	//CLAMP so that markers can't be at the very edge positions, always atleast 1 frame away

	public void StartMarkerMoved(float sliderVal) {
		print ("start to:"+sliderVal);

		activeStartFrame = sliderVal;
		
		float maxAllowed = activeEndFrame - 1;
		if(activeStartFrame > maxAllowed)startPointSlider.value = maxAllowed;

		UpdateSelectedAreaHighligter();
		//TODO: snap mover
		SnapPlaybackIfNotLooping (sliderVal);
		ClinicalMovePropertiesControl.instance.dirtyState = true;
	}
	
	public void EndMarkerMoved(float sliderVal) {
		print ("end to:"+sliderVal);

		activeEndFrame = sliderVal;
		
		float minAllowed = activeStartFrame + 1;
		if(activeEndFrame < minAllowed)endPointSlider.value = minAllowed;

		UpdateSelectedAreaHighligter();
		SnapPlaybackIfNotLooping (sliderVal);
		ClinicalMovePropertiesControl.instance.dirtyState = true;
	}

	public void CurrentMarkerMoved(float sliderVal) {
		movePlayer.currentPlayBackMode = PlayBackMode.Manual;
		movePlayer.SetFrame((int)sliderVal);
	}

	public void PlayingTick (float normalizedTime) {
		//HIRVEETÄ PURKKAA MITÄ HELVETTIÄ TÄÄLLÄ OIKEEN TAPAHTUU //HACK HACK TODO TODO FIXME FIXME
		currentPointSlider.onValueChanged.RemoveListener(CurrentMarkerMoved);
		currentPointSlider.normalizedValue = normalizedTime;
		currentPointSlider.onValueChanged.AddListener(CurrentMarkerMoved);

		//print("playing tick: "+normalizedTime);
	}

	void SnapPlaybackIfNotLooping (float snapToFrame) {
		if(movePlayer.currentPlayBackMode != PlayBackMode.EditPreview) {
			movePlayer.currentPlayBackMode = PlayBackMode.Manual;
			movePlayer.SetFrame((int)snapToFrame);
		}
	}

	void UpdateSelectedAreaHighligter () {
		selectedAreaHighlighter.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal,selectedAreaLenght * framePixelWidth);
		Vector2 pos = selectedAreaHighlighter.position;
		pos.x = startPointSlider.handleRect.position.x + (endPointSlider.handleRect.position.x - startPointSlider.handleRect.position.x) * 0.5f;
		selectedAreaHighlighter.position = pos;
	}
}
